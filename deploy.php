<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'Admin Trainznation');

// Project repository
set('repository', 'git@gitlab.com:trainznation/admin-trainznation.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', []);


// Hosts

host('192.168.0.18')
    ->user('deployer')
    ->identityFile('~/.ssh/deployerkey')
    ->set('deploy_path', '/www/wwwroot/admin.trainznation.tk')
    ->set('writable_mode', 'chmod');

// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

#before('deploy:symlink', 'artisan:migrate');

