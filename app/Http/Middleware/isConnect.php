<?php

namespace App\Http\Middleware;

use Closure;

class isConnect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session()->has('api_token') == true) {
            return $next($request);
        } else {
            return redirect()->route('login');
        }
    }
}
