<?php

namespace App\Http\Controllers\Tutoriel;

use App\Helpers\FileTransfert;
use App\Helpers\Image;
use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class TutorielCategoryController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;
    /**
     * @var FileTransfert|Image
     */
    private $image;


    /**
     * TutorielCategoryController constructor.
     * @param Trainznation $trainznation
     * @param FileTransfert $image
     */
    public function __construct(Trainznation $trainznation, FileTransfert $image)
    {
        $this->trainznation = $trainznation;
        $this->image = $image;
    }

    public function index()
    {
        $page_title = 'Gestion des Tutoriels';
        $page_description = 'Tableau de bord des catégories';

        $categories = $this->trainznation->get('/admin/tutoriel/category/list')->object();

        return view('tutoriel.category.index', compact('page_title', 'page_description'), [
            "categories" => $categories->data
        ]);
    }

    public function list()
    {
        try {
            $categories = $this->trainznation->get('/admin/tutoriel/category/list')->object();
            return response()->json($categories, 200);
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return response()->json("Erreur API", 500);
        }
    }

    public function store(Request $request)
    {
        try {
            $category = $this->trainznation->post('/admin/tutoriel/category', [
                "name" => $request->get('name')
            ])->object();

            try {
                $this->image->uploadFile($request->file('category_img'), 'tutoriel/category', $category->data->id.'.png');
            }catch (FileException $exception) {
                Log::error($exception->getMessage());
                return back()->with("error", "Erreur FILE: ".$exception->getMessage());
            }
        }catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return back()->with("error", "Erreur API: ".$exception->getMessage());
        }

        return back()->with('success', "La catégorie <strong>".$category->data->name."</strong> à été ajouté");
    }

    public function get($category_id)
    {
        try {
            return $this->trainznation->get('/admin/tutoriel/category/' . $category_id)->object();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return response()->json("Erreur API", 500);
        }
    }

    public function delete($category_id)
    {
        try {
            $category = $this->get($category_id);
            try {
                $delete = $this->trainznation->delete('/admin/tutoriel/category/' . $category_id)->object();
                try {
                    $this->deleteImg($category_id);
                }catch (FileException $exception) {
                    Log::error($exception->getMessage());
                    return response()->json("Erreur FILE", 500);
                }
            } catch (\Exception $exception) {
                Log::error($exception->getMessage());
                return response()->json("Erreur API", 500);
            }
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return response()->json("Erreur API", 500);
        }

        return response()->json($delete, 200);
    }

    private function deleteImg($category_id)
    {
        try {
            $this->image->deleteFile('tutoriel/category/', $category_id.'.png');
        }catch (FileException $exception) {
            Log::error($exception->getMessage());
            return response()->json("Erreur FILE", 500);
        }
    }
}
