<?php

namespace App\Http\Controllers\Tutoriel;

use App\Helpers\FileTransfert;
use App\Helpers\Video;
use App\Http\Controllers\Controller;
use App\Jobs\UploadFile;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TutorielController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;
    /**
     * @var FileTransfert
     */
    private $fileTransfert;

    /**
     * TutorielController constructor.
     * @param Trainznation $trainznation
     * @param FileTransfert $fileTransfert
     */
    public function __construct(Trainznation $trainznation, FileTransfert $fileTransfert)
    {
        $this->trainznation = $trainznation;
        $this->fileTransfert = $fileTransfert;
    }

    public function index()
    {
        $page_title = 'Gestion des Tutoriels';
        $page_description = 'Tableau de bord';

        return view('tutoriel.index', compact('page_title', 'page_description'));
    }

    public function list()
    {
        try {
            $tutoriels = $this->trainznation->get('/admin/tutoriel/list')->object();
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
            return response()->json("Erreur Système, consulter les logs", 500);
        }

        return response()->json($tutoriels, 200);
    }

    public function create()
    {
        $page_title = 'Gestion des Tutoriels';
        $page_description = "Création d'un tutoriel";

        return view('tutoriel.create', compact('page_title', 'page_description'));
    }

    public function store(Request $request)
    {
        try {
            $tutoriel = $this->trainznation->post('/admin/tutoriel', [
                "title" => $request->get('title'),
                "tutoriel_category_id" => $request->get('tutoriel_category_id'),
                "short_content" => $request->get('short_content'),
                "content" => $request->get('content'),
                "published" => ($request->has('published')) ? 1 : 0,
                "source" => ($request->has('source')) ? 1 : 0,
                "premium" => ($request->has('premium')) ? 1 : 0,
                "social" => ($request->has('social')) ? 1 : 0,
                "time" => null,
                "published_at" => ($request->has('published') == true || $request->get('published_at') != null) ? $request->get('published_at') : null
            ])->object();
        } catch (Exception $exception) {
            Log::critical($exception);
            return back()->with('error', $exception->getMessage());
        }

        if ($request->has('img_file')) {
            try {
                $this->fileTransfert->uploadFile($request->file('img_file'), 'tutoriel/'.$tutoriel->id.'/', $tutoriel->id.'.png');
            } catch (Exception $exception) {
                Log::critical($exception);
                return back()->with('error', $exception->getMessage());
            }
        }

        if ($request->has('video_file')) {
            try {
                $this->fileTransfert->uploadFile($request->file('video_file'), 'tutoriel/'.$tutoriel->id.'/', 'video.mp4');
                $this->updateDurationTime($tutoriel->data->id, $request->file('video_file'));
            } catch (Exception $exception) {
                Log::critical($exception);
                return back()->with('error', $exception->getMessage());
            }
        }

        if ($request->has('source_file')) {
            try {
                $this->fileTransfert->uploadFile($request->file('source_file'), 'tutoriel/'.$tutoriel->id.'/', 'source.zip');
            }catch (Exception $exception) {
                Log::critical($exception);
                return back()->with('error', $exception->getMessage());
            }
        }

        return back()->with('success', "Le tutoriel <strong>".$tutoriel->data->title."</strong> à été créer");
    }

    public function show($tutoriel_id)
    {
        try {
            $tutoriel = $this->trainznation->get('/admin/tutoriel/' . $tutoriel_id)->object();
        }catch (Exception $exception) {
            abort(500, $exception);
        }

        $page_title = 'Gestion des Tutoriels';
        $page_description = "Tutoriel: <strong>".$tutoriel->data->title."</strong>";

        return view('tutoriel.show', compact('page_title', 'page_description'), [
            "tutoriel" => $tutoriel->data
        ]);
    }

    private function updateDurationTime($tutoriel_id, $file) {
        try {
            $this->trainznation->put('/admin/tutoriel/'.$tutoriel_id, [
                "time" => Video::exctractDuration($file)
            ]);
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        return null;
    }
}
