<?php

namespace App\Http\Controllers\Wiki;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class WikiCategoryController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * WikiCategoryController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        try {
            $categories = $this->trainznation->get('/admin/wiki/category/list')->object()->data;
        } catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        $page_title = 'Gestion du Wiki';
        $page_description = 'Liste des Catégories';

        return view('wiki.category.index', compact('page_title', 'page_description', 'categories'));
    }

    public function search(Request $request)
    {
        try {
            $search = $this->trainznation->post('/admin/wiki/category/search', ["q" => $request->get('q')])->object()->data;
        } catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json($search);
    }

    public function store(Request $request)
    {
        try {
            $category = $this->trainznation->post('/admin/wiki/category', [
                "name" => $request->get('name')
            ])->object()->data;
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json($category);
    }

    public function delete($category_id)
    {
        try {
            $this->trainznation->delete('/admin/wiki/category/' . $category_id);
        } catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json(null, 200);
    }
}
