<?php

namespace App\Http\Controllers\Wiki;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class WikiController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * WikiController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    /**
     * @return Application|Factory|RedirectResponse|View
     */
    public function index()
    {
        try {
            $categories = $this->trainznation->get('/admin/wiki/category/list')->object()->data;
            $sectors = $this->trainznation->get('/admin/wiki/sector/list')->object()->data;
            $articles = $this->trainznation->get('/admin/wiki/list')->object()->data;
        } catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        $page_title = "Gestion du wiki";
        $page_description = "Gestion des articles";

        return view('wiki.index', compact('page_title', 'page_description', 'categories', 'articles', 'sectors'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function search(Request $request)
    {
        try {
            if ($request->has('q')) {
                $search = $this->trainznation->post('/admin/wiki/search', ['q' => $request->get('q')])->object()->data;
            } elseif ($request->has('category')) {
                $search = $this->trainznation->post('/admin/wiki/search', ['category' => $request->get('category')])->object()->data;
            } elseif ($request->has('sector')) {
                $search = $this->trainznation->post('/admin/wiki/search', ['sector' => $request->get('sector')])->object()->data;
            } elseif ($request->has('published')) {
                $search = $this->trainznation->post('/admin/wiki/search', ['published' => $request->get('published')])->object()->data;
            } else {
                $search = $this->trainznation->post('/admin/wiki/search', [])->object()->data;
            }
        } catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json($search);
    }

    public function create()
    {
        try {
            $categories = $this->trainznation->get('/admin/wiki/category/list')->object()->data;
        } catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        $page_title = "Gestion du wiki";
        $page_description = "Création d'un article";

        return view('wiki.create', compact('page_title', 'page_description', 'categories'));
    }

    public function store(Request $request)
    {
        try {
            $article = $this->trainznation->post('/admin/wiki', [
                "title" => $request->get('title'),
                "content" => $request->get('content'),
                "wiki_category_id" => $request->get('wiki_category_id'),
                "wiki_sector_id" => $request->get('wiki_sector_id')
            ])->object()->data;
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json($article);
    }

    public function show($article_id)
    {
        try {
            $article = $this->trainznation->get('/admin/wiki/'.$article_id)->object()->data;
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        $page_title = "Gestion du wiki";
        $page_description = "Fiche d'un article: <strong>".$article->title."</strong>";

        return view('wiki.show', compact('page_title', 'page_description', 'article'));
    }

    public function edit($article_id)
    {
        try {
            $article = $this->trainznation->get('/admin/wiki/'.$article_id)->object()->data;
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        $page_title = "Gestion du wiki";
        $page_description = "Edition d'un article: <strong>".$article->title."</strong>";

        return view('wiki.edit', compact('page_title', 'page_description', 'article'));
    }

    public function update(Request $request, $article_id)
    {
        try {
            $article = $this->trainznation->put('/admin/wiki/'.$article_id, [
                "title" => $request->get('title'),
                "content" => $request->get('content')
            ])->object()->data;
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json($article);
    }

    public function delete($article_id)
    {
        try {
            $this->trainznation->delete('/admin/wiki/'.$article_id);
        }catch (Exception $exception) {
            if(\request()->wantsJson()) {
                return response()->json($exception->getMessage());
            } else {
                return back()->with('error', $exception->getMessage());
            }
        }

        if(\request()->wantsJson()) {
            return response()->json(null, 200);
        } else {
            return back()->with('success', "Un article à été supprimer");
        }
    }

    public function publish($article_id)
    {
        try {
            $this->trainznation->get('/admin/wiki/'.$article_id.'/publish');
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        return back()->with('success', "L'article à été publier");
    }

    public function unpublish($article_id)
    {
        try {
            $this->trainznation->get('/admin/wiki/'.$article_id.'/unpublish');
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        return back()->with('success', "L'article à été publier");
    }
}
