<?php

namespace App\Http\Controllers\Wiki;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class WikiSectorController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * WikiSectorController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        try {
            $sectors = $this->trainznation->get('/admin/wiki/sector/list')->object()->data;
            $categories = $this->trainznation->get('/admin/wiki/category/list')->object()->data;
        } catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        $page_title = "Gestion du wiki";
        $page_description = "Liste des secteurs";

        return view('wiki.sector.index', compact('page_title', 'page_description', 'sectors', 'categories'));
    }

    public function search(Request $request)
    {
        try {
            if($request->has('q')) {
                $search = $this->trainznation->post('/admin/wiki/sector/search', ['q' => $request->get('q')])->object()->data;
            } elseif ($request->has('category')) {
                $search = $this->trainznation->post('/admin/wiki/sector/search', ['category' => $request->get('category')])->object()->data;
            } else {
                $search = $this->trainznation->post('/admin/wiki/sector/search', [])->object()->data;
            }
        } catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json($search);
    }

    public function store(Request $request)
    {
        try {
            $sector = $this->trainznation->post('/admin/wiki/sector', [
                "name" => $request->get('name'),
                "wiki_category_id" => $request->get('wiki_category_id')
            ])->object()->data;
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json($sector);
    }

    public function delete($sector_id)
    {
        try {
            $this->trainznation->delete('/admin/wiki/sector/'.$sector_id);
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json(null, 200);
    }
}
