<?php

namespace App\Http\Controllers\Support;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class SupportSourceController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * SupportSourceController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        try {
            $sources = $this->trainznation->get('/admin/support/source/list')->object()->data;
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        $page_title = "Paramétrage du support";
        $page_description = 'Source';

        return view('settings.support.source.index', compact('page_title', 'page_description', 'sources'));
    }

    public function store(Request $request)
    {
        try {
            $this->trainznation->post('/admin/support/source', [
                "designation" => $request->get('designation')
            ]);
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json(null);
    }

    public function get($source_id)
    {
        try {
            $source = $this->trainznation->get('/admin/support/source/'.$source_id)->object()->data;
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json($source);
    }

    public function update(Request $request, $source_id)
    {
        try {
            $this->trainznation->put('/admin/support/source/'.$source_id, [
                "designation" => $request->get('designation')
            ]);
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json(null);
    }

    public function delete($source_id)
    {
        try {
            $this->trainznation->delete('/admin/support/source/'.$source_id);
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json(null);
    }
}
