<?php

namespace App\Http\Controllers\Support;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class SupportStatusController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * SupportSourceController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        try {
            $statuses = $this->trainznation->get('/admin/support/status/list')->object()->data;
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        $page_title = "Paramétrage du support";
        $page_description = 'Status';

        return view('settings.support.status.index', compact('page_title', 'page_description', 'statuses'));
    }

    public function store(Request $request)
    {
        try {
            $this->trainznation->post('/admin/support/status', [
                "designation" => $request->get('designation'),
                "color" => $request->get('color'),
                "icon" => $request->get('icon')
            ]);
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json(null);
    }

    public function get($status_id)
    {
        try {
            $status = $this->trainznation->get('/admin/support/status/'.$status_id)->object()->data;
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json($status);
    }

    public function update(Request $request, $status_id)
    {
        try {
            $this->trainznation->put('/admin/support/status/'.$status_id, [
                "designation" => $request->get('designation'),
                "color" => $request->get('color'),
                "icon" => $request->get('icon')
            ]);
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json(null);
    }

    public function delete($status_id)
    {
        try {
            $this->trainznation->delete('/admin/support/status/'.$status_id);
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json(null);
    }
}
