<?php

namespace App\Http\Controllers\Support;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class SupportSectorController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * SupportSectorController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        try {
            $sectors = $this->trainznation->get('/admin/support/sector/list')->object()->data;
            $categories = $this->trainznation->get('/admin/support/category/list')->object()->data;
        } catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        $page_title = "Paramétrage du support";
        $page_description = 'Secteurs';

        return view('settings.support.sector.index', compact('page_title', 'page_description', 'sectors', 'categories'));
    }

    public function store(Request $request)
    {
        try {
            $this->trainznation->post('/admin/support/sector', [
                "support_category_id" => $request->get('support_category_id'),
                "name" => $request->get('name')
            ]);
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json(null);
    }

    public function get($sector_id)
    {
        try {
            $sector = $this->trainznation->get('/admin/support/sector/'.$sector_id)->object()->data;
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json($sector);
    }

    public function update(Request $request, $sector_id)
    {
        try {
            $this->trainznation->put('/admin/support/sector/'.$sector_id, [
                "support_category_id" => $request->get('support_category_id'),
                "name" => $request->get('name')
            ]);
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json(null);
    }

    public function delete($sector_id)
    {
        try {
            $this->trainznation->delete('/admin/support/sector/'.$sector_id);
        }catch (Exception $exception) {
            return response($exception->getMessage());
        }

        return response()->json(null);
    }

    public function searchByCategory($category_id)
    {
        try {
            $q = $this->trainznation->get('/admin/support/sector/list')->object()->data;
            $sectors = collect($q)->where('category.id', $category_id);
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json($sectors);
    }
}
