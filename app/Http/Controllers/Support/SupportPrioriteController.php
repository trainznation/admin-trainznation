<?php

namespace App\Http\Controllers\Support;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class SupportPrioriteController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * SupportPrioriteController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        try {
            $priorities = $this->trainznation->get('/admin/support/priority/list')->object()->data;
        } catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        $page_title = "Paramétrage du support";
        $page_description = 'Priorité';

        return view('settings.support.priority.index', compact('page_title', 'page_description', 'priorities'));
    }

    public function store(Request $request)
    {
        try {
            $this->trainznation->post('/admin/support/priority', [
                "designation" => $request->get('designation'),
                "color" => $request->get('color')
            ]);
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json(null);
    }

    public function get($priority_id)
    {
        try {
            $priority = $this->trainznation->get('/admin/support/priority/'.$priority_id)->object()->data;
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json($priority);
    }

    public function update(Request $request, $priority_id)
    {
        try {
            $this->trainznation->put('/admin/support/priority/'.$priority_id, [
                "designation" => $request->get('designation'),
                "color" => $request->get('color')
            ]);
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json(null);
    }

    public function delete($priority_id)
    {
        try {
            $this->trainznation->delete('/admin/support/priority/'.$priority_id);
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json(null);
    }
}
