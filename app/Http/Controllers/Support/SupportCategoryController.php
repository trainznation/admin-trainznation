<?php

namespace App\Http\Controllers\Support;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Arcanedev\LogViewer\Entities\Log;
use Exception;
use Illuminate\Http\Request;

class SupportCategoryController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * SupportCategoryController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        try {
            $categories = $this->trainznation->get('/admin/support/category/list')->object()->data;
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        $page_title = "Paramétrage du support";
        $page_description = 'Catégorie';

        return view('settings.support.category.index', compact('page_title', 'page_description', 'categories'));
    }

    public function store(Request $request)
    {
        try {
            $category = $this->trainznation->post('/admin/support/category', [
                "name" => $request->get('name')
            ])->object();
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json(null);
    }

    public function get($category_id)
    {
        try {
            $category = $this->trainznation->get('/admin/support/category/'.$category_id)->object();
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json($category->data);
    }

    public function update(Request $request, $category_id)
    {
        try {
            $this->trainznation->put('/admin/support/category/'.$category_id, [
                "name" => $request->get('name')
            ]);
        }catch (Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }

        return response()->json(null, 200);
    }

    public function delete($category_id)
    {
        try {
            $this->trainznation->delete('/admin/support/category/'.$category_id);
        }catch (Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }

        return response()->json(null, 200);
    }
}
