<?php

namespace App\Http\Controllers\Support;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SupportController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * SupportController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        try {
            $tickets = $this->trainznation->get('/admin/support/ticket/list')->object()->data;
            $categories = $this->trainznation->get('/admin/support/category/list')->object()->data;
            $statuses = $this->trainznation->get('/admin/support/status/list')->object()->data;
            $priorities = $this->trainznation->get('/admin/support/priority/list')->object()->data;
            $sources = $this->trainznation->get('/admin/support/source/list')->object()->data;
            $users = $this->trainznation->get('/admin/user/list')->object()->data;
            $admins = collect($users)->where('admin', 1);
        }catch (Exception $exception) {
            Log::error($exception);
            return back()->with('error', $exception->getMessage());
        }

        $page_title = "Gestion des tickets";
        $page_description = 'Tableau de Bord';

        return view('support.index', compact('page_title', 'page_description', 'tickets', 'statuses', 'priorities', 'sources', 'categories', 'users', 'admins'));
    }

    public function list()
    {
        try {
            $tickets = $this->trainznation->get('/admin/support/ticket/list')->object()->data;
        }catch (Exception $exception) {
            Log::error($exception);
            return response()->json($exception->getMessage());
        }

        return response()->json($tickets);
    }

    public function store(Request $request)
    {
        try {
            $ticket = $this->trainznation->post('/admin/support/ticket', [
                "subject" => $request->get('subject'),
                "message" => $request->get('message'),
                "support_category_id" => $request->get('support_category_id'),
                "support_sector_id" => $request->get('support_sector_id'),
                "support_ticket_status_id" => $request->get('support_ticket_status_id'),
                "support_ticket_source_id" => $request->get('support_ticket_source_id'),
                "requester_id" => $request->get('requester_id'),
                "assign_to" => session()->get('user')['id']
            ])->body();
        }catch (Exception $exception) {
            Log::error($exception);
            return response()->json($exception->getMessage());
        }

        return response()->json($ticket);
    }

    public function show($ticket_id)
    {
        try {
            $ticket = $this->trainznation->get('/admin/support/ticket/'.$ticket_id)->object()->data;
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        $page_title = "Gestion des tickets";
        $page_description = 'Ticket N° '.$ticket_id;

        return view('support.show', compact('page_title', 'page_description', 'ticket'));
    }

    public function conv($ticket_id)
    {
        try {
            $ticket = $this->trainznation->get('/admin/support/ticket/'.$ticket_id)->object()->data;
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }
        $convs = $ticket->conversations;

        ob_start();
        ?>
        <?php foreach ($convs as $conv): ?>
        <?php if($conv->user->id == session()->get('user')['id']): ?>
            <div class="d-flex flex-column mb-5 align-items-end">
                <div class="d-flex align-items-center">
                    <div class="symbol symbol-circle symbol-40 mr-3">
                        <img alt="Pic" src="<?= $conv->user->avatar->encoded ?>" />
                    </div>
                    <div>
                        <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6"><?= $conv->user->name ?></a>
                        <span class="text-muted font-size-sm"><?php if($conv->updated_at->format > now()->addDay()){echo $conv->updated_at->normalize;}else{echo $conv->updated_at->human;} ?></span>
                    </div>
                </div>
                <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">
                    <?= $conv->message; ?>
                </div>
            </div>
        <?php else: ?>
            <div class="d-flex flex-column mb-5 align-items-start">
                <div class="d-flex align-items-center">
                    <div class="symbol symbol-circle symbol-40 mr-3">
                        <img alt="Pic" src="<?= $conv->user->avatar->encoded ?>" />
                    </div>
                    <div>
                        <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6"><?= $conv->user->name ?></a>
                        <span class="text-muted font-size-sm"><?php if($conv->updated_at->format > now()->addDay()){echo $conv->updated_at->normalize;}else{echo $conv->updated_at->human;} ?></span>
                    </div>
                </div>
                <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">
                    <?= $conv->message; ?>
                </div>
            </div>
        <?php endif; ?>
        <?php endforeach; ?>
        <?php
        $content = ob_get_clean();

        return response()->json($content);
    }

    public function send(Request $request, $ticket_id)
    {
        try {
            $this->trainznation->post('/admin/support/ticket/'.$ticket_id.'/chat', [
                "message" => $request->get('messaging'),
                "acting" => 'admin'
            ]);
        }catch (Exception $exception) {
            Log::error($exception);
            return response()->json($exception->getMessage());
        }

        return response()->json(null,200);
    }
}
