<?php

namespace App\Http\Controllers\Asset;

use App\Helpers\Image;
use App\Http\Controllers\Controller;
use App\Jobs\Asset\Category\PostIcon;
use App\Jobs\UploadFile;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class CategoryController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * CategoryController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        $page_title = 'Gestion des Catégories d\'Objet';
        $page_description = 'Tableau de bord';

        $categories = (object)$this->trainznation->get('/admin/asset/category/list')->json();

        if (request()->wantsJson()) {
            return response()->json($categories, 200);
        } else {
            return view('asset.category.index', compact('page_title', 'page_description', 'categories'));
        }
    }

    public function list(Request $request)
    {
        $data = $this->trainznation->get('/admin/asset/category/list', ["q" => $request->get('q')])->json();
        $array = [];

        $array["total_count"] = count($data['data']);
        $array['items'] = [];
        foreach ($data['data'] as $category) {
            $array['items'][] = [
                "name" => $category['name'],
                "id" => $category['id'],
                "image" => $category['image']
            ];
        }

        return response()->json($array, 200);
    }

    public function store(Request $request)
    {
        //dd($request->all());
        try {
            $response = $this->trainznation->post('/admin/asset/category', [
                "name" => $request->name
            ])->object();

            if($request->hasFile('icon') == true) {
                try {
                    $uploadIcon = $request->file('icon');
                    $file = $uploadIcon->move(public_path('uploads'), $uploadIcon->getClientOriginalName());
                    $this->uploadIcon($file, $response->data);
                }catch (Exception $exception) {
                    return back()->with('error', $exception->getMessage());
                }
            }
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        return back()->with('success', "La Catégorie <strong>".$response->data->name."</strong> à été créer");
    }

    public function destroy($category_id)
    {
        $response = $this->trainznation->delete('/admin/asset/category/' . $category_id);
        $json = $response->json();
        if ($response->status() == 200) {
            return back()->with('success', $json['data']['message']);
        } else {
            return back()->with('error', $json['data']['error']);
        }
    }

    private function uploadIcon($file, $category)
    {
        try {
            dispatch(new UploadFile($file->getRealPath(), 'assets/category/', $category->id.'.png'));
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        return null;
    }
}
