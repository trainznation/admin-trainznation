<?php

namespace App\Http\Controllers\Asset;

use App\Helpers\FileTransfert;
use App\Http\Controllers\Controller;
use App\Jobs\UploadFile;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Trainznation\Sketchfab\Sketchfab;
use ZanySoft\Zip\Zip;

class AssetController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;
    /**
     * @var FileTransfert
     */
    private $fileTransfert;
    /**
     * @var Sketchfab
     */
    private $sketchfab;

    /**
     * AssetController constructor.
     * @param Trainznation $trainznation
     * @param FileTransfert $fileTransfert
     * @param Sketchfab $sketchfab
     */
    public function __construct(Trainznation $trainznation, FileTransfert $fileTransfert, Sketchfab $sketchfab)
    {
        $this->trainznation = $trainznation;
        $this->fileTransfert = $fileTransfert;
        $this->sketchfab = $sketchfab;
    }

    public function index()
    {
        $page_title = 'Gestion des Objets';
        $page_description = 'Tableau de bord';

        $assets = (object)$this->trainznation->get('/admin/asset')->json();

        return view('asset.index', compact('page_title', 'page_description', 'assets'));
    }

    public function create()
    {
        $page_title = 'Gestion des Objets';
        $page_description = "Création d'un objet";

        return view('asset.create', compact('page_title', 'page_description'));
    }

    public function store(Request $request)
    {
        //dd($request->all());
        try {
            $asset = $this->trainznation->post('/admin/asset', [
                "designation" => $request->get('designation'),
                "short_description" => $request->get('short_description'),
                "description" => ($request->has('description') == true) ? $request->get('description') : null,
                "social" => ($request->get('social') == 'on') ? 1 : 0,
                "published" => ($request->get('published') == 'on') ? 1 : 0,
                "meshes" => ($request->get('meshes') == 'on') ? 1 : 0,
                "pricing" => ($request->get('pricing') == 'on') ? 1 : 0,
                "price" => null,
                "published_at" => ($request->has('published_at') == true) ? now() : null,
                "asset_category_id" => $request->get('asset_category_id')
            ])->object();

        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        if($request->hasFile('file_asset') == true) {
            try {
                $uploadAsset = $request->file('file_asset');
                $file = $uploadAsset->move(public_path('uploads'), $uploadAsset->getClientOriginalName());
                $this->uploadAsset($file, $asset->data);
            }catch (Exception $exception) {
                return back()->with('error', $exception->getMessage());
            }
        }

        if($request->hasFile('asset_img') == true) {
            try {
                $uploadImg = $request->file('asset_img');
                $file = $uploadImg->move(public_path('uploads'), $uploadImg->getClientOriginalName());
                $this->uploadImg($file, $asset->data);
            }catch (Exception $exception) {
                return back()->with('error', $exception->getMessage());
            }
        }

        return redirect()->route('Asset.index')->with('success', "L'objet <strong>".$asset->data->designation."</strong> à été créer");
    }

    public function show($asset_id)
    {
        $asset = $this->trainznation->get('/admin/asset/' . $asset_id);
        $data = $asset->json();
        //dd($data);
        if ($asset->status() == 200) {
            $page_title = 'Gestion des Objets';
            $page_description = "Objet: " . $data['data']['designation'];
            //dd($data['data']);
            return view('asset.show', compact('page_title', 'page_description'), [
                'asset' => $data['data']
            ]);
        } else {
            return abort($asset->status(), $asset->json()['error']);
        }
    }

    public function edit($asset_id)
    {
        $asset = $this->trainznation->get('/admin/asset/' . $asset_id);
        $data = $asset->json();
        if ($asset->status() == 200) {
            $page_title = 'Gestion des Objets';
            $page_description = "Edition d'un objet: " . $data['data']['designation'];
            //dd($data['data']);
            return view('asset.edit', compact('page_title', 'page_description'), [
                'asset' => $data['data']
            ]);
        } else {
            return abort($asset->status(), $asset->json()['error']);
        }
    }

    public function update(Request $request, $asset_id)
    {
        //dd($request->all());
        try {
            $asset = $this->trainznation->put('/admin/asset/' . $asset_id, [
                "designation" => $request->get('designation'),
                "short_description" => $request->get('short_description'),
                "description" => ($request->has('description') == true) ? $request->get('description') : null,
                "social" => ($request->get('social') == 'on') ? 1 : 0,
                "published" => ($request->get('published') == 'on') ? 1 : 0,
                "meshes" => ($request->get('meshes') == 'on') ? 1 : 0,
                "pricing" => ($request->get('pricing') == 'on') ? 1 : 0,
                "price" => ($request->has('pricing') == true) ? $request->get('price') : null,
                "published_at" => ($request->has('published_at') == true) ? $request->get('published_at') : null,
                "asset_category_id" => $request->get('asset_category_id')
            ])->object();

            if($request->hasFile('file_asset') == true) {
                try {
                    $uploadAsset = $request->file('file_asset');
                    $file = $uploadAsset->move(public_path('uploads'), $uploadAsset->getClientOriginalName());
                    $this->uploadAsset($file, $asset->data);
                }catch (Exception $exception) {
                    return back()->with('error', $exception->getMessage());
                }
            }

            if($request->hasFile('file_meshes') == true) {
                try {
                    $uploadMeshes = $request->file('file_meshes');
                    $file = $uploadMeshes->move(public_path('uploads'), $uploadMeshes->getClientOriginalName());
                    $this->uploadMesh($file, $asset->data);
                }catch (Exception $exception) {
                    return back()->with('error', $exception->getMessage());
                }
            }

            if($request->hasFile('asset_img') == true) {
                try {
                    $uploadImg = $request->file('asset_img');
                    $file = $uploadImg->move(public_path('uploads'), $uploadImg->getClientOriginalName());
                    $this->uploadImg($file, $asset->data);
                }catch (Exception $exception) {
                    return back()->with('error', $exception->getMessage());
                }
            }
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        return back()->with('success', "L'objet <strong>".$asset->data->designation."</strong> à été mis à jour");
    }

    public function destroy($asset_id)
    {
        $response = $this->trainznation->delete('/admin/asset/' . $asset_id);
        if ($response->status() == 200) {
            $this->deleteAllFile($asset_id);
            return response()->json($response->json(), 200);
        } else {
            return response()->json($response->json(), $response->status());
        }
    }

    public function publish($asset_id)
    {
        $asset = $this->trainznation->get('/admin/asset/' . $asset_id . '/publish');
        if ($asset->status() == 200) {
            $data = $asset->json();
            return back()->with('success', $data['data']['message']);
        } else {
            return back()->with('error', $asset['data']['error']);
        }
    }

    public function dispublish($asset_id)
    {
        $asset = $this->trainznation->get('/admin/asset/' . $asset_id . '/dispublish');
        if ($asset->status() == 200) {
            $data = $asset->json();
            return back()->with('success', $data['data']['message']);
        } else {
            return back()->with('error', $asset['data']['error']);
        }
    }

    private function deleteAllFile($asset_id)
    {
        if (Storage::disk(env('FILESYSTEM_DRIVER'))->exists('v3/assets/' . $asset_id) == true) {
            try {
                Storage::disk(env('FILESYSTEM_DRIVER'))->deleteDirectory('v3/assets/' . $asset_id);
            } catch (FileException $exception) {
                return $exception;
            }
        }
        if (Storage::disk(env('FILESYSTEM_DRIVER'))->exists('v3/assets/' . $asset_id . '.png') == true) {
            try {
                Storage::disk(env('FILESYSTEM_DRIVER'))->delete('v3/assets/' . $asset_id . '.png');
                return null;
            } catch (FileException $exception) {
                return $exception;
            }
        }

        return null;
    }

    private function uploadImg($file, $asset)
    {
        try {
            dispatch(new UploadFile($file->getRealPath(), 'assets/'.$asset->id, $asset->id.'.png'));
        }catch (Exception $exception) {
            Log::error($exception, $exception->getTrace());
        }

        return null;
    }

    private function uploadMesh($file, $asset)
    {
        try {
            dispatch(new UploadFile($file->getRealPath(), 'assets/'.$asset->id.'/meshes', 'asset.glb'));
        }catch (Exception $exception) {
            Log::error($exception, $exception->getTrace());
        }

        return null;
    }

    public function uploadAsset($file, $asset)
    {
        try {
            dispatch(new UploadFile($file->getRealPath(), 'assets/'.$asset->id, $asset->uuid.'.zip'));
        }catch (Exception $exception) {
            Log::error($exception, $exception->getTrace());
        }

        return null;
    }

    public function addSketchfab(Request $request, $asset_id)
    {
        try {
            $this->trainznation->post('/admin/asset/'.$asset_id.'/addSketchfab', [
                "sketchfab_uid" => $request->get('sketchfab_uid')
            ]);
        }catch (Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }

        return response()->json(null, 200);
    }

    public function addPrice(Request $request, $asset_id)
    {
        try {
            $this->trainznation->post('/admin/asset/'.$asset_id.'/addPrice', [
                "price" => $request->get('price')
            ]);
        }catch (Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }

        return response()->json(null, 200);
    }

}
