<?php

namespace App\Http\Controllers\Asset;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class KuidController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * KuidController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function store(Request $request, $asset_id)
    {
        $query = $this->trainznation->post('/admin/asset/'.$asset_id.'/kuid', [
            "kuid" => str_replace('_', '', $request->get('kuid')),
            "published_at" => $request->get('published_at')
        ]);
        $kuid = $query->json();

        if($query->status() == 200) {
            ob_start();
            ?>
            <tr>
                <td><?= $kuid['data']['id']; ?></td>
                <td><?php echo "'<'".$kuid['data']['kuid']."'>'"; ?></td>
                <td><?= Carbon::createFromTimestamp(strtotime($kuid['data']['published_at']))->format('d/m/Y à H:i'); ?></td>
                <td>
                    <button class="btn btn-sm btn-icon btn-danger" data-href="<?= route('Asset.Kuid.delete', [$kuid['data']['asset_id'], $kuid['data']['id']]) ?>" data-toggle="tooltip" title="Supprimer"><i class="fa fa-trash"></i> </button>
                </td>
            </tr>
            <?php
            $content = ob_get_clean();
            return response()->json($content, 200);
        } else {
            return response()->json($kuid, $query->status());
        }
    }

    public function delete($asset_id, $kuid_id)
    {
        $query = $this->trainznation->delete('/admin/asset/'.$asset_id.'/kuid/'.$kuid_id);
        $kuid = $query->json();

        if($query->status() == 200) {
            return response()->json($kuid['data']['message'], 200);
        }else {
            return response()->json($kuid['data']['error'], $query->status());
        }
    }
}
