<?php

namespace App\Http\Controllers\Asset;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * TagsController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function store(Request $request, $asset_id)
    {
        try {
            $this->trainznation->post('/admin/asset/'.$asset_id.'/tags', [
                "tags" => $request->get('tags')
            ])->object();
        }catch (Exception $exception) {
            return response($exception, 500);
        }

        return response()->json(null, 200);

    }

    public function delete($asset_id, $tag)
    {
        $tag = $this->trainznation->delete('/admin/asset/'.$asset_id.'/tags/'.$tag);
        $data = $tag->json();

        if($tag->status() == 200) {
            return response()->json($data, 200);
        } else {
            return response()->json($data, $tag->status());
        }
    }
}
