<?php

namespace App\Http\Controllers\Settings;

use App\Helpers\Image;
use App\Http\Controllers\Controller;
use App\Jobs\UploadFile;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SlideshowController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;
    /**
     * @var Image
     */
    private $image;

    /**
     * SlideshowController constructor.
     * @param Trainznation $trainznation
     * @param Image $image
     */
    public function __construct(Trainznation $trainznation, Image $image)
    {
        $this->trainznation = $trainznation;
        $this->image = $image;
    }

    public function index()
    {
        try {
            $lists = $this->trainznation->get('/admin/settings/slideshow/list')->object();
        } catch (Exception $exception) {
            return $exception->getMessage();
        }

        $page_title = "Gestion du slideshow";

        return view('settings.slideshow.index', compact('page_title'), [
            "slideshows" => $lists->data
        ]);
    }

    public function store(Request $request)
    {
        try {
            $slide = $this->trainznation->post('/admin/settings/slideshow', [
                "link" => $request->get('link')
            ])->object();
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }


        if($request->hasFile('img_slide') == true) {
            try {
                $uploadImg = $request->file('img_slide');
                $file = $uploadImg->move(public_path('uploads'), $uploadImg->getClientOriginalName());
                $this->uploadImg($file, $slide->data);
            }catch (Exception $exception) {
                return back()->with('error', $exception->getMessage());
            }
        }

        return back()->with('success', "Slide ajouté avec succès");
    }

    public function delete($slide_id)
    {
        try {
            $this->trainznation->delete('/admin/settings/slideshow/'.$slide_id);
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        try {
            $this->image->deleteImg('slideshow', $slide_id.'.png');
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        return back()->with('success', "Le slide à été supprimer");
    }

    private function uploadImg($file, $slide)
    {
        try {
            dispatch(new UploadFile($file->getRealPath(), 'slideshow', $slide->id.'.png'));
        }catch (Exception $exception) {
            Log::error($exception, $exception->getTrace());
        }

        return null;
    }
}
