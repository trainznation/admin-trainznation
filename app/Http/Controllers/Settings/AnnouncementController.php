<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class AnnouncementController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * AnnouncementController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        $page_title = 'Gestion des annonces';

        return view('settings.annonce.index', compact('page_title'));
    }

    public function list()
    {
        try {
            $lists = $this->trainznation->get('/admin/settings/announcement/list')->object();
        } catch (Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }

        return response()->json($lists, 200);
    }

    public function store(Request $request)
    {
        if($request->get('expiring_at') < now()) {
            return response()->json('La date saisie est inférieurs à la date actuelle !', 900);
        }

        try {
            $ann = $this->trainznation->post('/admin/settings/announcement', [
                "message" => $request->get('message'),
                "type" => $request->get('type'),
                "expiring_at" => $request->get('expiring_at')
            ]);
        }catch (Exception $exception) {
            return response($exception->getMessage(), 500);
        }

        return response()->json(null, 200);
    }

    public function delete($announcement_id)
    {
        try {
            $ann = $this->trainznation->delete('/admin/settings/announcement/' . $announcement_id)->object();
        } catch (Exception $exception) {
            return back()->with('error', $exception);
        }

        return back()->with('success', "L'annonce à été supprimer");
    }
}
