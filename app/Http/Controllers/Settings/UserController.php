<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * UserController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        try {
            $users = $this->trainznation->get('/admin/user/list')->object()->data;
        }catch (Exception $exception) {
            return $exception;
        }

        $page_title = "Liste des utilisateurs";

        return view('settings.user.index', compact('page_title', 'users'));
    }

    public function show($user_id)
    {
        try {
            $user = $this->trainznation->get('/admin/user/'.$user_id)->object()->data;
        }catch (Exception $exception) {
            return $exception;
        }

        $page_title = "Utilisateur: ".$user->name;

        return view('settings.user.show', compact('page_title', 'user'));
    }
}
