<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Packages\Api\Auth;
use App\Packages\Api\Trainznation;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;
    /**
     * @var Auth
     */
    private $auth;

    /**
     * AuthController constructor.
     * @param Trainznation $trainznation
     * @param Auth $auth
     */
    public function __construct(Trainznation $trainznation, Auth $auth)
    {
        $this->trainznation = $trainznation;
        $this->auth = $auth;
    }

    public function loginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $response = $this->trainznation->authUser($request->get('email'), $request->get('password'));
        if ($response->status() == 200) {
            $data = $response->json();
            session()->put('api_token', $data['access_token']);
            $meResponse = $this->auth->me();
            if ($meResponse->status() == 200) {
                $me = $meResponse->json();
                if ($me['admin'] == 0) {
                    return response()->json(['error' => "Vous n'etes pas autorisée à acceder au panel administrateur"], 401);
                } else {
                    session()->put('user', $me);
                    return response()->json(null, 200);
                }
            } else {
                return response()->json($response->json(), $response->status());
            }
        } else {
            return response()->json($response->json(), $response->status());
        }
    }

    public function logout()
    {
        $response = $this->auth->logout();
        session()->flush();
        if ($response->status() == 200) {
            return redirect()->route('login');
        } else {
            return abort($response->status(), $response->json()['error']);
        }
    }

    public function me()
    {
        return response()->json(Auth::Mee()->object());
    }

    public function readNotif($user_id, $notif_id)
    {
        try {
            $notif = $this->trainznation->get("/admin/user/".$user_id."/notifications/".$notif_id."/read");
        }catch (\Exception $exception) {
            return response()->json(["error" => $exception], 500);
        }

        return response()->json(null, 200);
    }
}
