<?php

namespace App\Http\Controllers\Route;

use App\Helpers\FileTransfert;
use App\Helpers\Image;
use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class RouteSessionController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;
    /**
     * @var FileTransfert
     */
    private $fileTransfert;
    /**
     * @var Image
     */
    private $image;

    /**
     * RouteSessionController constructor.
     * @param Trainznation $trainznation
     * @param FileTransfert $fileTransfert
     * @param Image $image
     */
    public function __construct(Trainznation $trainznation, FileTransfert $fileTransfert, Image $image)
    {
        $this->trainznation = $trainznation;
        $this->fileTransfert = $fileTransfert;
        $this->image = $image;
    }

    public function list($route_id)
    {
        $query = $this->trainznation->get('/admin/route/' . $route_id . '/session/list');
        $sessions = $query->object();

        if ($query->status() == 200) {
            return response()->json($sessions, 200);
        } else {
            return response()->json($sessions, 500);
        }
    }

    public function store(Request $request, $route_id)
    {
        try {
            $query = $this->trainznation->post('/admin/route/' . $route_id . '/session', [
                "name" => $request->get('name'),
                "short_content" => $request->get('short_content'),
                "content" => $request->get('content'),
                "published" => ($request->has('published') == true) ? 1 : 0,
                "published_at" => ($request->has('published') == true) ? $request->get('published_at') : null,
                "kuid" => $request->get('kuid')
            ]);
            $session = $query->object();

            try {
                $this->fileTransfert->uploadFile($request->file('file_session'), 'route/' . $route_id . '/session/' . $session->data->id, $session->data->uuid . '.zip');

                try {
                    $this->image->savingImg($request->file('session_img'), 'route/' . $route_id . '/session/' . $session->data->id, 'session.png');
                } catch (FileException $exception) {
                    return back()->with('error', "Erreur File Système: " . $exception->getMessage());
                }
            } catch (FileException $exception) {
                return back()->with('error', "Erreur File Système: " . $exception->getMessage());
            }

            return back()->with('success', "La session <strong>" . $session->data->name . "</strong> à été ajouté");
        }catch (\Exception $exception) {
            return back()->with('error', "Erreur Système: ".$exception->getMessage());
        }
    }

    public function get($route_id, $session_id)
    {
        $query = $this->trainznation->get('/admin/route/' . $route_id . '/session/' . $session_id);
        $session = $query->object();

        if ($query->status() == 200) {
            if (\request()->wantsJson() == true) {
                return response()->json($session, 200);
            } else {
                return $session;
            }
        } else {
            if (\request()->wantsJson() == true) {
                return response()->json($session, $query->status());
            } else {
                return $session;
            }
        }
    }

    public function delete($route_id, $session_id)
    {
        $query = $this->trainznation->get('/admin/route/' . $route_id . '/session/' . $session_id);
        $session = $query->object();
        $query2 = $this->trainznation->delete('/admin/route/'.$route_id.'/session/'.$session_id);
        $dl = $query2->object();

        if($query2->status() == 200) {
            // Suppression du fichier de la session
            try {
                $this->fileTransfert->deleteFile('route/'.$route_id.'/session/'.$session_id, $session->data->uuid.".zip");

                try {
                    $this->image->deleteImg('route/'.$route_id.'/session/'.$session_id, "session.png");
                }catch (FileException $exception) {
                    if(\request()->wantsJson() == true) {
                        return response()->json($exception->getMessage(), 500);
                    } else {
                        return back()->with('error', $exception->getMessage());
                    }
                }
            }catch (FileException $exception) {
                if(\request()->wantsJson() == true) {
                    return response()->json($exception->getMessage(), 500);
                } else {
                    return back()->with('error', $exception->getMessage());
                }
            }

            if(\request()->wantsJson() == true) {
                return response()->json($dl->data->message, 200);
            } else {
                return back()->with('success', $dl->data->message);
            }
        } else {
            if(\request()->wantsJson() == true) {
                return response()->json($dl->data->error, 500);
            } else {
                return back()->with('error', $dl->data->error);
            }
        }
    }
}
