<?php

namespace App\Http\Controllers\Route;

use App\Helpers\FileTransfert;
use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class VersionController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;
    /**
     * @var FileTransfert
     */
    private $fileTransfert;

    /**
     * VersionController constructor.
     * @param Trainznation $trainznation
     * @param FileTransfert $fileTransfert
     */
    public function __construct(Trainznation $trainznation, FileTransfert $fileTransfert)
    {
        $this->trainznation = $trainznation;
        $this->fileTransfert = $fileTransfert;
    }

    public function list($route_id, $roadmap_id)
    {
        $query = $this->trainznation->get('/admin/route/' . $route_id . '/roadmap/' . $roadmap_id . '/version/list');
        $versions = $query->json();

        if ($query->status() == 200) {
            return response()->json($versions, 200);
        } else {
            return response()->json($versions, $query->status());
        }
    }

    public function get($route_id, $roadmap_id, $version_id)
    {
        $query = $this->trainznation->get('/admin/route/' . $route_id . '/roadmap/' . $roadmap_id . '/version/' . $version_id);
        $version = $query->json();

        if ($query->status() == 200) {
            return response()->json($version);
        } else {
            return response()->json($version, $query->status());
        }
    }

    public function show($route_id, $roadmap_id, $version_id)
    {
        $query = $this->trainznation->get('/admin/route/' . $route_id . '/roadmap/' . $roadmap_id . '/version/' . $version_id);
        $version = $query->json();

        if ($query->status() == 200) {
            $page_title = 'Gestion des routes';
            $page_description = 'Version: ' . $version_id;

            return view('route.version', compact('page_title', 'page_description'), [
                "version" => (object)$version['data']
            ]);
        } else {
            return abort(500, $version['message']);
        }
    }


    public function allTask($route_id, $roadmap_id, $version_id)
    {
        $query = $this->trainznation->get('/admin/route/' . $route_id . '/roadmap/' . $roadmap_id . '/version/' . $version_id . '/tasks');
        $tasks = $query->json();

        if ($query->status() == 200) {
            return response()->json($tasks, 200);
        } else {
            return response()->json($tasks, $query->status());
        }
    }

    public function store(Request $request, $route_id, $roadmap_id)
    {
        /*$query = $this->trainznation->post('/admin/route/' . $route_id . '/roadmap/' . $roadmap_id . '/version', $request->all());
        $version = $query->json();

        if ($query->status() == 200) {
            if($request->hasFile('img_version') == true) {
                $this->fileTransfert->uploadFile($request->get('img_version'), 'route/version/'.$version->data->id, 'version.png');
            }
            if($request->hasFile('video_version') == true) {
                $this->fileTransfert->uploadFile($request->get('video_version'), 'route/version/'.$version->data->id, 'video.mp4');
            }
            return response()->json($version, 200);
        } else {
            return response()->json($version, $query->status());
        }*/

        try {
            $version = $this->trainznation->post('/admin/route/' . $route_id . '/roadmap/' . $roadmap_id . '/version', $request->all())->object();
        } catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        if ($request->hasFile('img_version') == true) {
            try {
                $this->fileTransfert->uploadFile($request->file('img_version'), 'route/version/'.$version->data->id, 'version.png');
            }catch (FileException $exception) {
                return back()->with('error', $exception->getMessage());
            }
        }

        if ($request->hasFile('video_version') == true) {
            try {
                $this->fileTransfert->uploadFile($request->file('video_version'), 'route/version/'.$version->data->id, 'version.mp4');
            }catch (FileException $exception) {
                return back()->with('error', $exception->getMessage());
            }
        }

        return back()->with('success', "La version à été ajouté avec succès");
    }
}
