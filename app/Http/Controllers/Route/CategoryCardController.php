<?php

namespace App\Http\Controllers\Route;

use App\Helpers\Image;
use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class CategoryCardController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;
    /**
     * @var Image
     */
    private $image;

    /**
     * CategoryCardController constructor.
     * @param Trainznation $trainznation
     * @param Image $image
     */
    public function __construct(Trainznation $trainznation, Image $image)
    {
        $this->trainznation = $trainznation;
        $this->image = $image;
    }

    public function addCard(Request $request, $route_id, $roadmap_id, $version_id)
    {
        $query = $this->trainznation->post('/admin/route/'.$route_id.'/roadmap/'.$roadmap_id.'/version/'.$version_id.'/category/'.$request->get('category_id').'/card', [
            "name" => $request->get('name'),
            "status" => $request->get('status'),
            "description" => $request->get('description')
        ]);
        $card = $query->json();
        if($query->status() == 200) {
            if($request->hasFile('card_img') == true) {
                try {
                    $this->image->savingImg($request->file('card_img'), 'route/tasks/', $card['data']['id'].'.png');
                }catch (FileException $exception) {
                    return back()->with('error', "Erreur Fichier: ".$exception->getMessage());
                }
            }
            return back()->with('success', "La catégorie ".$card['data']['name']." à été ajouté.");
        }else {
            return back()->with('error', "Erreur Serveur: ".$card['data']["error"]);
        }
    }

    public function edit($route_id, $roadmap_id, $version_id, $card_id)
    {
        $query = $this->trainznation->get('/admin/route/'.$route_id.'/roadmap/'.$roadmap_id.'/version/'.$version_id.'/card/'.$card_id);
        $card = $query->json();
        $query2 = $this->trainznation->get('/admin/route/'.$route_id.'/roadmap/'.$roadmap_id.'/version/'.$version_id);
        $version = $query2->json();

        if($query->status() == 200) {
            $page_title = 'Gestion des routes';
            $page_description = 'Edition de la carte: ' . $card['data']['name'];

            return view('route.update_card', compact('page_title', 'page_description', 'route_id', 'roadmap_id', 'version_id', 'card_id'), [
                "card" => (object) $card['data'],
                "version" => (object) $version['data']
            ]);
        } else {
            return back()->with('error', $card['data']['error']);
        }
    }

    public function update(Request $request, $route_id, $roadmap_id, $version_id, $card_id)
    {
        $query = $this->trainznation->put('/admin/route/'.$route_id.'/roadmap/'.$roadmap_id.'/version/'.$version_id.'/card/'.$card_id, [
            "name" => $request->get('name'),
            "status" => $request->get('status'),
            "description" => $request->get('description'),
            "category_id" => $request->get('category_id')
        ]);
        $card = $query->json();

        if($query->status() == 200) {
            if($request->hasFile('card_img') == true) {
                try {
                    $this->image->savingImg($request->file('card_img'), 'route/tasks/', $card['data']['id'].'.png');
                }catch (FileException $exception) {
                    return back()->with('error', "Erreur Fichier: ".$exception->getMessage());
                }
            }
            return back()->with('success', "La catégorie ".$card['data']['name']." à été edité.");
        }else {
            return back()->with('error', "Erreur Serveur: ".$card['data']["error"]);
        }
    }

    public function delete($route_id, $roadmap_id, $version_id, $card_id)
    {
        $query = $this->trainznation->delete('/admin/route/'.$route_id.'/roadmap/'.$roadmap_id.'/version/'.$version_id.'/card/'.$card_id);

        if($query->status() == 200) {
            try {
                $this->image->deleteImg('route/tasks/', $card_id.'.png');
            }catch (FileException $exception) {
                return back()->with('error', "Erreur Fichier: ".$exception->getMessage());
            }
        } else {
            return back()->with('error', "Erreur Serveur API");
        }

        return back()->with('success', "La carte N°".$card_id." à été supprimé");
    }
}
