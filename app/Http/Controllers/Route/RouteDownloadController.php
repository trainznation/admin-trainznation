<?php

namespace App\Http\Controllers\Route;

use App\Helpers\FileTransfert;
use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Illuminate\Http\Request;

class RouteDownloadController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;
    /**
     * @var FileTransfert
     */
    private $fileTransfert;

    /**
     * RouteDownloadController constructor.
     * @param Trainznation $trainznation
     * @param FileTransfert $fileTransfert
     */
    public function __construct(Trainznation $trainznation, FileTransfert $fileTransfert)
    {
        $this->trainznation = $trainznation;
        $this->fileTransfert = $fileTransfert;
    }

    public function list($route_id)
    {
        $query = $this->trainznation->get("/admin/route/$route_id/download/list");
        $downloads = $query->json();

        if($query->status() == 200) {
            return response()->json($downloads, 200);
        } else {
            return response()->json($downloads, 500);
        }
    }

    public function store(Request $request, $route_id)
    {
        //dd($request->all());
        $query = $this->trainznation->post('/admin/route/'.$route_id.'/download', [
            "version" => $request->get('version'),
            "build" => $request->get('build'),
            "note" => $request->get('note'),
            "published" => ($request->has('published')) ? 1 : 0,
            "alpha" => ($request->get('type') == 'alpha') ? 1 : 0,
            "beta" => ($request->get('type') == 'beta') ? 1 : 0,
            "release" => ($request->get('type') == 'release') ? 1 : 0
        ]);
        $download = $query->object();

        if($query->status() == 200) {
            try {
                $this->fileTransfert->uploadFile($request->file('cdp'), 'route/'.$route_id, $download->data->uuid.".zip");
            }catch (\Exception $exception) {
                return redirect()->back()->with('error', "Erreur Système: ".$exception->getMessage());
            }
            return redirect()->back()->with('success', "Le téléchargement pour la version <strong>V".$download->data->version.":".$download->data->build."</strong> à été ajouté.");
        } else {
            return redirect()->back()->with('error', "Erreur Système: ".$download->error);
        }
    }

    public function delete($route_id, $download_id)
    {
        $query2 = $this->trainznation->get('/admin/route/'.$route_id.'/download/'.$download_id);
        $query = $this->trainznation->delete('/admin/route/'.$route_id.'/download/'.$download_id);
        $route = $query2->object();
        $download = $query->object();

        try {
            $this->fileTransfert->deleteFile('route/'.$route_id, $route->data->uuid.".zip");
            return response()->json($download, 200);
        }catch (\Exception $exception) {
            return response()->json($exception, 500);
        }
    }
}
