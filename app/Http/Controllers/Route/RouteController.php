<?php

namespace App\Http\Controllers\Route;

use App\Helpers\Image;
use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class RouteController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;
    /**
     * @var Image
     */
    private $image;

    /**
     * RouteController constructor.
     * @param Trainznation $trainznation
     * @param Image $image
     */
    public function __construct(Trainznation $trainznation, Image $image)
    {
        $this->trainznation = $trainznation;
        $this->image = $image;
    }

    public function index()
    {
        $page_title = 'Gestion des routes';
        $page_description = 'Tableau de bord';

        $route = (object)$this->trainznation->get('/admin/route/list')->json();

        return view('route.index', compact('page_title', 'page_description'), [
            "routes" => $route
        ]);
    }

    public function create()
    {
        $page_title = 'Gestion des routes';
        $page_description = 'Création d\'une route';

        return view('route.create', compact('page_title', 'page_description'));
    }

    public function store(Request $request)
    {
        $query = $this->trainznation->post('/admin/route', [
            "name" => $request->get('name'),
            "description" => $request->get('description'),
            "published" => ($request->has('published')) ? 1 : 0,
            "version" => $request->get('version'),
            "build" => $request->get('build'),
            "route_kuid" => $request->get('route_kuid'),
            "dependance_kuid" => $request->get('dependance_kuid')
        ]);
        $route = $query->json();

        if ($query->status() == 200) {
            try {
                $this->image->savingImg($request->file('route_img'), 'route/' . $route['data']['id'], 'route.png');
            } catch (FileException $exception) {
                return back()->with('error', "Erreur lors du traitement de l'image: <i>" . $exception->getMessage() . "</i>");
            }

            return redirect()->route('Route.index')->with('success', "La Route <strong>" . $route["data"]["name"] . "</strong> à été ajouté avec succès");
        } else {
            return back()->with('error', "Erreur: <i>" . $route['data']['error'] . "</i>");
        }
    }

    public function show($route_id)
    {
        $query = $this->trainznation->get('/admin/route/' . $route_id);
        $route = $query->json();

        if ($query->status() == 200) {
            $page_title = 'Gestion des routes';
            $page_description = 'Route: ' . $route['data']['name'];

            return view('route.show', compact('page_title', 'page_description'), [
                "route" => (object)$route['data']
            ]);
        } else {
            return abort(500, $route['message']);
        }
    }

    public function edit($route_id)
    {
        $query = $this->trainznation->get('/admin/route/' . $route_id);
        $route = $query->json();

        if ($query->status() == 200) {
            $page_title = 'Gestion des routes';
            $page_description = 'Edition de la route: ' . $route['data']['name'];

            return view('route.edit', compact('page_title', 'page_description'), [
                "route" => $route['data']
            ]);
        } else {
            return abort(500, $route['message']);
        }
    }

    public function update(Request $request, $route_id)
    {
        $query = $this->trainznation->put('/admin/route/' . $route_id, [
            "name" => $request->get('name'),
            "description" => $request->get('description'),
            "published" => ($request->has('published')) ? 1 : 0,
            "version" => $request->get('version'),
            "build" => $request->get('build'),
            "route_kuid" => $request->get('route_kuid'),
            "dependance_kuid" => $request->get('dependance_kuid')
        ]);
        $route = $query->json();

        if ($query->status() == 200) {
            if ($request->hasFile('route_img') == true) {
                try {
                    $this->image->savingImg($request->file('route_img'), 'route/' . $route['data']['id'], 'route.png');
                } catch (FileException $exception) {
                    return back()->with('error', "Erreur lors du traitement de l'image: <i>" . $exception->getMessage() . "</i>");
                }
                return back()->with('success', "La route <strong>" . $route['data']['name'] . "</strong> à été mise à jour");
            } else {
                return back()->with('success', "La route <strong>" . $route['data']['name'] . "</strong> à été mise à jour");
            }
        } else {
            return back()->with('error', "Erreur: <i>" . $route['data']['error'] . "</i>");
        }
    }

    public function delete($route_id)
    {
        /*$query = $this->trainznation->delete('/admin/route/' . $route_id);
        $route = $query->json();

        if ($query->status() == 200) {
            try {
                $this->image->deleteDirectory('route/' . $route_id);
            } catch (FileException $exception) {
                if (\request()->wantsJson()) {
                    return response()->json($exception, 500);
                } else {
                    return back()->with('error', "Erreur: " . $exception->getMessage());
                }
            }
            return back()->with('success', $route['data']['message']);
        } else {
            if(\request()->wantsJson()) {
                return response()->json($query->json(), $query->status());
            } else {
                return back()->with('error', $route['data']['error']);
            }
        }*/

        try {
            $this->trainznation->delete('/admin/route/'.$route_id);
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        return back()->with('success', "La route à été supprimer");
    }
}
