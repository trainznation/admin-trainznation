<?php

namespace App\Http\Controllers\Route;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class RouteVersionTaskController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * RouteVersionTaskController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function store(Request $request, $route_id, $version_id)
    {
        try {
            $task = $this->trainznation->post('/admin/route/'.$route_id.'/version/'.$version_id.'/tasks', [
                "name" => $request->get('name'),
                "description" => $request->get('description'),
                "lieu" => $request->get('lieu'),
                "etat" => $request->get('status'),
                "priority" => $request->get('priority'),
                "started_at" => $request->get('started_at'),
                "finished_at" => $request->get('finished_at'),
                "route_version_id" => $version_id,
                "route_id" => $route_id
            ])->json();
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json($task, 200);
    }

    public function edit($route_id, $version_id, $task_id)
    {
        try {
            $version = $this->trainznation->get('/admin/route/'.$route_id.'/version/'.$version_id)->object();
            $task = $this->trainznation->get('/admin/route/'.$route_id.'/version/'.$version_id.'/tasks/'.$task_id)->object();
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        $page_title = 'Gestion des routes';
        $page_description = 'Edition de la tache: ' . $task->data->name;

        return view('route.update_task', compact('page_title', 'page_description', 'route_id', 'version_id', 'task_id'), [
            "task" => $task->data,
            "version" => $version->data
        ]);
    }

    public function update(Request $request, $route_id, $version_id, $task_id)
    {
        try {
            $task = $this->trainznation->put('/admin/route/'.$route_id.'/version/'.$version_id.'/tasks/'.$task_id, [
                "name" => $request->get('name'),
                "description" => $request->get('description'),
                "lieu" => $request->get('lieu'),
                "etat" => $request->get('status'),
                "priority" => $request->get('priority'),
                "started_at" => $request->get('started_at'),
                "finished_at" => $request->get('finished_at'),
                "route_version_id" => $version_id,
                "route_id" => $route_id
            ])->object();

        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        return redirect()->route('Route.Version.show', [$task->data->route_id, $task->data->version_id])->with('success', "La Tache <strong>".$task->data->name."</strong> à été modifier");
    }

    public function delete($route_id, $version_id, $task_id)
    {
        try {
            $this->trainznation->delete('/admin/route/'.$route_id.'/version/'.$version_id.'/tasks/'.$task_id);
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        return back()->with('success', "La tache à été supprimer");
    }
}
