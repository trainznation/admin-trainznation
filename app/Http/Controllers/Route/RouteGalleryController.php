<?php

namespace App\Http\Controllers\Route;

use App\Helpers\Image;
use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class RouteGalleryController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;
    /**
     * @var Image
     */
    private $image;

    /**
     * RouteGalleryController constructor.
     * @param Trainznation $trainznation
     * @param Image $image
     */
    public function __construct(Trainznation $trainznation, Image $image)
    {
        $this->trainznation = $trainznation;
        $this->image = $image;
    }

    public function delete($route_id, $gallery_id)
    {
        $query = $this->trainznation->get('/admin/route/' . $route_id . '/gallery/' . $gallery_id);
        $gallery = $query->json();
        $delete = $this->trainznation->delete('/admin/route/' . $route_id . '/gallery/' . $gallery_id);
        $data = $delete->json();

        if ($delete->status() == 200) {
            try {
                $this->image->deleteImg('/route/' . $route_id . '/gallery/' . $gallery['data']['category'] . '/', $gallery_id . '.png');
            } catch (FileException $exception) {
                return response()->json(['error' => $exception], 500);
            }
            return response()->json(['message' => $data['data']['message']], 200);
        } else {
            return response()->json(['error' => $data['data']['error']], 500);
        }
    }

    public function addFile(Request $request, $route_id)
    {
        $query = $this->trainznation->post('/admin/route/' . $route_id . '/gallery/addFile', [
            "category_id" => $request->get('category_id')
        ]);
        $gallery = $query->json();

        if ($query->status() == 200) {
            try {
                $this->image->savingImg($request->file('file'), '/route/' . $route_id . '/gallery/' . $request->get('category_id'), $gallery['data']['id'] . '.png');
            } catch (FileException $exception) {
                return response()->json(['error' => $exception->getMessage()], $query->status());
            }
        } else {
            return response()->json(['error' => $gallery['data']['error']], $query->status());
        }
        return response()->json(['message' => "L'image à été ajouté"], 200);
    }
}
