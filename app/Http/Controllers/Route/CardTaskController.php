<?php

namespace App\Http\Controllers\Route;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Illuminate\Http\Request;

class CardTaskController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * CardTaskController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function addTask(Request $request, $route_id, $roadmap_id, $version_id)
    {
        $query = $this->trainznation->post('/admin/route/'.$route_id.'/roadmap/'.$roadmap_id.'/version/'.$version_id.'/card/addTask', [
            "name" => $request->get('name'),
            "description" => $request->get('description'),
            "status" => $request->get('status'),
            "started_at" => $request->get('started_at'),
            "finished_at" => $request->get('finished_at'),
            "category_id" => $request->get('category_id'),
            "card_id" => $request->get('card_id')
        ]);
        $task = $query->json();

        if($query->status() == 200) {
            return response()->json($task, 200);
        } else {
            return response()->json($query->body(), $query->status());
        }
    }

    public function edit($route_id, $roadmap_id, $version_id, $card_id, $task_id)
    {
        $query = $this->trainznation->get("/admin/route/$route_id/roadmap/$roadmap_id/version/$version_id/card/$card_id/task/$task_id");
        $task = $query->object();
        $query2 = $this->trainznation->get('/admin/route/'.$route_id.'/roadmap/'.$roadmap_id.'/version/'.$version_id);
        $version = $query2->object();

        if($query->status() == 200) {
            $page_title = 'Gestion des routes';
            $page_description = 'Edition de la tache: ' . $task->data->name;

            return view('route.update_task', compact('page_title', 'page_description', 'route_id', 'roadmap_id', 'version_id', 'card_id', 'task_id'), [
                "task" => $task->data,
                "version" => $version->data
            ]);
        } else {
            return back()->with('error', $task->data->error);
        }
    }

    public function update(Request $request, $route_id, $roadmap_id, $version_id, $card_id, $task_id)
    {
        $query = $this->trainznation->put("/admin/route/$route_id/roadmap/$roadmap_id/version/$version_id/card/$card_id/task/$task_id", [
            "name" => $request->get('name'),
            "description" => $request->get('description'),
            "status" => $request->get('status'),
            "started_at" => $request->get('started_at'),
            "finished_at" => $request->get('finished_at'),
            "card_id" => $card_id
        ]);
        $task = $query->object();

        if($query->status() == 200) {
            return redirect()->route('Route.Version.show', [$route_id, $roadmap_id, $version_id])->with('success', "La Tache ".$request->get('name')." à été modifié");
        } else {
            dd($query->body());
            return back()->with('error', "Error Système: ".$task->data->error);
        }
    }
}
