<?php

namespace App\Http\Controllers\Route;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Illuminate\Http\Request;

class RouteGalleryCategoryController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * RouteGalleryCategoryController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function list(Request $request, $route_id)
    {
        if($request->has('q') == true) {
            $query = $this->trainznation->get('/admin/route/' . $route_id . '/gallery/category/list', [
                "q" => $request->get('q')
            ]);
            $datas = $query->json();
            $category = [];

            $category['total_count'] = count($datas['data']);
            $category['items'] = [];
            foreach ($datas['data'] as $data) {
                $category['items'][] = [
                    "name" => $data['name'],
                    "id" => $data['id']
                ];
            }
        } else {
            $query = $this->trainznation->get('/admin/route/' . $route_id . '/gallery/category/list');
            $category = $query->json();
        }


        if ($query->status() == 200) {
            return response()->json($category, 200);
        } else {
            return response()->json($category, $query->status());
        }
    }

    public function store(Request $request, $route_id)
    {
        $query = $this->trainznation->post('/admin/route/' . $route_id . '/gallery/category', [
            "name" => $request->get('name')
        ]);
        $category = $query->json();

        if ($query->status() == 200) {
            return back()->with("success", "La Catégorie <strong>" . $category['data']['name'] . "</strong> à été ajouté");
        } else {
            return back()->with('error', "Erreur: ".$category['data']['error']);
        }
    }

    public function get($route_id, $category_id)
    {
        $query = $this->trainznation->get('/admin/route/' . $route_id . '/gallery/category/' . $category_id);
        $category = $query->json();

        if ($query->status() == 200) {
            return response()->json($category, 200);
        } else {
            return response()->json($category, $query->status());
        }
    }
}
