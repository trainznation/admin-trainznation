<?php

namespace App\Http\Controllers\Route;

use App\Helpers\FileTransfert;
use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class RouteVersionController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;
    /**
     * @var FileTransfert
     */
    private $fileTransfert;

    /**
     * RouteVersionController constructor.
     * @param Trainznation $trainznation
     * @param FileTransfert $fileTransfert
     */
    public function __construct(Trainznation $trainznation, FileTransfert $fileTransfert)
    {
        $this->trainznation = $trainznation;
        $this->fileTransfert = $fileTransfert;
    }

    public function store(Request $request, $route_id)
    {
        try {
            $version = $this->trainznation->post('/admin/route/' . $route_id . '/version', $request->all())->object();
        } catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        if ($request->hasFile('img_version') == true) {
            try {
                $this->fileTransfert->uploadFile($request->file('img_version'), 'route/version/'.$version->data->id, 'version.png');
            }catch (FileException $exception) {
                return back()->with('error', $exception->getMessage());
            }
        }

        if ($request->hasFile('video_version') == true) {
            try {
                $this->fileTransfert->uploadFile($request->file('video_version'), 'route/version/'.$version->data->id, 'video.mp4');
            }catch (FileException $exception) {
                return back()->with('error', $exception->getMessage());
            }
        }

        return back()->with('success', "La version à été ajouté avec succès");
    }

    public function show($route_id, $version_id)
    {
        try {
            $version = $this->trainznation->get('/admin/route/' . $route_id . '/version/' . $version_id)->object();
        } catch (Exception $exception) {
            return abort(500, $exception->getMessage());
        }

        $page_title = 'Gestion des routes';
        $page_description = 'Version: ' . $version_id;

        return view('route.version', compact('page_title', 'page_description'), [
            "version" => $version->data
        ]);
    }

    public function info($route_id, $version_id)
    {
        try {
            $version = $this->trainznation->get('/admin/route/' . $route_id . '/version/' . $version_id)->json();
        } catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json($version);
    }

    public function publish($route_id, $version_id)
    {
        try {
            $this->trainznation->get('/admin/route/'.$route_id.'/version/'.$version_id.'/publish');
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        return back()->with('success', "Cette version à été publier");
    }
}
