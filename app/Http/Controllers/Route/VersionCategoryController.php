<?php

namespace App\Http\Controllers\Route;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Illuminate\Http\Request;

class VersionCategoryController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * VersionCategoryController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function listCards(Request $request, $route_id, $roadmap_id, $version_id)
    {
        if($request->has('selector') == true) {
            $query = $this->trainznation->get('/admin/route/'.$route_id.'/roadmap/'.$roadmap_id.'/version/'.$version_id.'/category/'.$request->get('category_id').'/card/list');
            $cards = $query->json();

            if($query->status() == 200) {
                ob_start();
                ?>
                <div class="form-group">
                    <label for="card_id">Carte</label>
                    <select name="card_id" id="card_id" class="form-control" required>
                        <option value=""></option>
                        <?php foreach ($cards['data'] as $card): ?>
                            <option value="<?= $card['id']; ?>"><?= $card['name']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <?php
                $content = ob_get_clean();

                return response()->json($content, 200);
            } else {
                return response()->json($query->body(), $query->status());
            }
        } else {
            $query = $this->trainznation->get('/api/admin/route/'.$route_id.'/roadmap/'.$roadmap_id.'/version/'.$version_id.'/category/'.$request->get('category_id').'/card/list');
            $cards = $query->json();

            if($query->status() == 200) {
                return response()->json($cards, 200);
            } else {
                return response()->json($query->body(), $query->status());
            }
        }
    }

    public function store(Request $request, $route_id, $roadmap_id, $version_id)
    {
        $query = $this->trainznation->post('/admin/route/'.$route_id.'/roadmap/'.$roadmap_id.'/version/'.$version_id.'/category', [
            "name" => $request->get('name'),
        ]);
        $category = $query->json();

        if($query->status() == 200) {
            return response()->json(["message" => "La catégory ".$category['data']['name']." à été ajouté"], 200);
        } else {
            return response()->json(["error" => $category['data']['error']], $query->status());
        }
    }

    public function delete($route_id, $roadmap_id, $version_id, $category_id)
    {
        $query = $this->trainznation->delete('/admin/route/'.$route_id.'/roadmap/'.$roadmap_id.'/version/'.$version_id.'/category/'.$category_id);
        $category = $query->json();

        if($query->status() == 200) {
            return back()->with('success', "La catégorie ".$category_id." à été supprimer");
        } else {
            return back()->with('error', $category['data']['error']);
        }
    }
}
