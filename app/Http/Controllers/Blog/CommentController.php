<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * CommentController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function approve($article_id, $comment_id)
    {
        $query = $this->trainznation->get('/admin/blog/'.$article_id.'/comment/'.$comment_id.'/approve');
        $comment = $query->json();

        if($query->status() == 200) {
            return response()->json($comment, 200);
        } else {
            return response()->json($comment, $query->status());
        }
    }

    public function disapprove($article_id, $comment_id)
    {
        $query = $this->trainznation->get('/admin/blog/'.$article_id.'/comment/'.$comment_id.'/disapprove');
        $comment = $query->json();

        if($query->status() == 200) {
            return response()->json($comment, 200);
        } else {
            return response()->json($comment, $query->status());
        }
    }
}
