<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * CategoryController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        $page_title = 'Gestion des Catégories d\'Article';
        $page_description = 'Tableau de bord';

        $categories = (object)$this->trainznation->get('/admin/blog/category/list')->json();

        if (request()->wantsJson()) {
            return response()->json($categories, 200);
        } else {
            return view('blog.category.index', compact('page_title', 'page_description', 'categories'));
        }
    }

    public function list(Request $request)
    {
        $data = $this->trainznation->get('/admin/blog/category/list', ["q" => $request->get('q')])->json();
        $array = [];

        $array["total_count"] = count($data['data']);
        $array['items'] = [];
        foreach ($data['data'] as $category) {
            $array['items'][] = [
                "id" => $category['id'],
                "name" => $category['name'],
            ];
        }

        return response()->json($array, 200);
    }

    public function store(Request $request)
    {
        $query = $this->trainznation->post('/admin/blog/category', [
            "name" => $request->get('name')
        ]);
        $data = $query->json();
        if ($query->status() == 200) {
            ob_start();
            ?>
            <tr>
                <td><?= $data['data']['id'] ?></td>
                <td><?= $data['data']['name'] ?></td>
                <td>
                    <button data-href="<?= route('Blog.Category.delete', $data['data']['id']) ?>"
                            class="btn btn-icon btn-danger btn-sm mr-2 btn-delete"><i class="fa fa-trash"></i></button>
                </td>
            </tr>
            <?php
            $content = ob_get_clean();
            return response()->json([
                "content" => $content,
                "data" => $data
            ], 200);
        } else {
            return request()->json(['error' => $data], $query->status());
        }
    }

    public function delete($category_id)
    {
        /*$query = $this->trainznation->delete('/admin/blog/category/' . $category_id);
        $data = $query->json();

        if ($query->status() == 200) {
            return response()->json($data, 200);
        } else {
            return response()->json($data, $query->status());
        }*/

        try {
            $this->trainznation->delete('/admin/blog/category/'.$category_id);
        }catch (Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }

        return redirect()->back()->with('success', "La catégorie à été supprimer");
    }
}
