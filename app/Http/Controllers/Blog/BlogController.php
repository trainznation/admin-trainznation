<?php

namespace App\Http\Controllers\Blog;

use App\Helpers\FileTransfert;
use App\Helpers\Image;
use App\Http\Controllers\Controller;
use App\Jobs\UploadFile;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class BlogController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * @var FileTransfert
     */
    private $fileTransfert;

    /**
     * BlogController constructor.
     * @param Trainznation $trainznation
     * @param FileTransfert $fileTransfert
     */
    public function __construct(Trainznation $trainznation, FileTransfert $fileTransfert)
    {
        $this->trainznation = $trainznation;
        $this->fileTransfert = $fileTransfert;
    }

    public function index()
    {
        $page_title = 'Gestion des Articles';
        $page_description = 'Tableau de bord';

        $articles = (object)$this->trainznation->get('/admin/blog/list')->json();
        //dd($articles);

        return view('blog.index', compact('page_title', 'page_description', 'articles'));
    }

    public function create()
    {
        $page_title = 'Gestion des Articles';
        $page_description = "Création d'un article";

        return view('blog.create', compact('page_title', 'page_description'));
    }

    public function store(Request $request)
    {
        try {
            $article = $this->trainznation->post('/admin/blog', [
                "blog_category_id" => $request->get('blog_category_id'),
                "title" => $request->get('title'),
                "short_content" => $request->get('short_content'),
                "content" => $request->get('content'),
                "published" => ($request->has('published')) ? 1 : 0,
                "published_at" => ($request->has('published_at')) ? $request->get('published_at') : null,
                "social" => ($request->has('social')) ? 1 : 0,
            ])->object();

            if($request->hasFile('blog_img') == true) {
                try {
                    $this->fileTransfert->uploadFile($request->file('blog_img'), 'blog/', $article->id.'.png');
                }catch (Exception $exception) {
                    return back()->with('error', $exception->getMessage());
                }
            }
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        return redirect()->route('Blog.index')->with('success', "L'article <strong>".$article->data->title."</strong> à été créer");
    }

    public function show($blog_id)
    {
        $query = $this->trainznation->get('/admin/blog/' . $blog_id);
        $blog = $query->json();
        //dd($blog);

        if ($query->status() == 200) {
            $page_title = 'Gestion des articles';
            $page_description = "Article: " . $blog['data']['title'];

            return view('blog.show', compact('page_title', 'page_description'), [
                "blog" => $blog['data']
            ]);
        } else {
            //dd($blog);
            return abort(500, $blog['message']);
        }
    }

    public function edit($blog_id)
    {
        $query = $this->trainznation->get('/admin/blog/'.$blog_id);

        if($query->status() == 200) {
            $blog = $query->json();
            //dd($blog);
            $page_title = 'Gestion des articles';
            $page_description = "Edition de l'Article: " . $blog['data']['title'];

            return view('blog.edit', compact('page_title', 'page_description'), [
                "blog" => $blog['data']
            ]);
        }else{
            return abort($query->status(), $query->json()['error']);
        }
    }

    public function update(Request $request, $blog_id)
    {
        try {
            $article = $this->trainznation->put('/admin/blog/'.$blog_id, [
                "blog_category_id" => $request->get('blog_category_id'),
                "title" => $request->get('title'),
                "short_content" => $request->get('short_content'),
                "content" => $request->get('content'),
                "published" => ($request->has('published')) ? 1 : 0,
                "published_at" => ($request->has('published_at')) ? $request->get('published_at') : null,
                "social" => ($request->has('social')) ? 1 : 0,
            ])->object();

            if($request->hasFile('blog_img') == true) {
                try {
                    $this->fileTransfert->uploadFile($request->file('blog_img'), 'blog/', $article->id.'.png');
                }catch (Exception $exception) {
                    return back()->with('error', $exception->getMessage());
                }
            }
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        return back()->with('success', "L'article <strong>".$article->data->title."</strong> à été mis à jour");
    }

    public function delete($blog_id)
    {
        try {
            $this->trainznation->delete('/admin/blog/'.$blog_id);
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        try {
            $this->fileTransfert->deleteFile('blog/', $blog_id.'.png');
        }catch (FileException $exception) {
            return back()->with('error', $exception->getMessage());
        }

        return back()->with('success', "L'article à été supprimer");
    }

    public function publish($blog_id)
    {
        $query = $this->trainznation->get('/admin/blog/'.$blog_id.'/publish');
        $blog = $query->json();

        if($query->status() == 200) {
            return back()->with('success', $blog['data']['message']);
        } else {
            return back()->with('error', $blog['data']['error']);
        }
    }

    public function dispublish($blog_id)
    {
        $query = $this->trainznation->get('/admin/blog/'.$blog_id.'/dispublish');
        $blog = $query->json();

        if($query->status() == 200) {
            return back()->with('success', $blog['data']['message']);
        } else {
            return back()->with('error', $blog['data']['error']);
        }
    }
}
