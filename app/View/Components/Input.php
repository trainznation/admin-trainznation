<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Input extends Component
{
    public $name;
    public $type;
    public $label;
    /**
     * @var string
     */
    public $value;
    /**
     * @var bool
     */
    public $required;
    /**
     * @var bool
     */
    public $autofocus;
    /**
     * @var string
     */
    public $classInput;
    /**
     * @var string
     */
    public $classLabel;

    /**
     * Create a new component instance.
     *
     * @param $name
     * @param $type
     * @param $label
     * @param string $value
     * @param bool $required
     * @param bool $autofocus
     * @param string $classInput
     * @param string $classLabel
     */
    public function __construct($name, $type, $label, $value = '', $required = false, $autofocus = false, $classInput = '', $classLabel = '')
    {

        $this->name = $name;
        $this->type = $type;
        $this->label = $label;
        $this->value = $value;
        $this->required = $required;
        $this->autofocus = $autofocus;
        $this->classInput = $classInput;
        $this->classLabel = $classLabel;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.input');
    }
}
