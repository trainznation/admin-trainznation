<?php

namespace App\Jobs;

use App\Helpers\FileTransfert;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;

class UploadFile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $file;
    private $destPath;
    /**
     * @var FileTransfert
     */
    private $fileTransfert;
    private $namefile;

    /**
     * Create a new job instance.
     *
     * @param $file
     * @param $destPath
     * @param $namefile
     */
    public function __construct($file, $destPath, $namefile)
    {
        //
        $this->file = $file;
        $this->destPath = $destPath;
        $this->fileTransfert = new FileTransfert();
        $this->namefile = $namefile;
    }

    /**
     * Execute the job.
     *
     * @return Exception
     */
    public function handle()
    {
        if (!Storage::disk(env('FILESYSTEM_DRIVER'))->exists($this->destPath . '/' . $this->namefile)) {
            try {
                Storage::disk(env('FILESYSTEM_DRIVER'))->put($this->destPath . '/' . $this->namefile, file_get_contents($this->file));
                Storage::disk(env('FILESYSTEM_DRIVER'))->setVisibility($this->destPath . '/' . $this->namefile, 'public');
            } catch (FileException $exception) {
                return $exception;
            }

            try {
                unlink(public_path('uploads/'.$this->namefile));
            }catch (Exception $exception) {
                return $exception;
            }

            return null;
        } else {
            try {
                Storage::disk(env('FILESYSTEM_DRIVER'))->delete($this->destPath.'/'.$this->namefile);
                try {
                    Storage::disk(env('FILESYSTEM_DRIVER'))->put($this->destPath . '/' . $this->namefile, file_get_contents($this->file));
                    Storage::disk(env('FILESYSTEM_DRIVER'))->setVisibility($this->destPath . '/' . $this->namefile, 'public');
                }catch (FileException $exception) {
                    return $exception;
                }
            }catch (FileException $exception) {
                return $exception;
            }

            try {
                unlink(public_path('uploads/'.$this->namefile));
            }catch (Exception $exception) {
                return $exception;
            }

            return null;
        }
    }
}
