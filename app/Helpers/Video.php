<?php


namespace App\Helpers;


use Carbon\Carbon;
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;

class Video
{
    public static function exctractDuration($file)
    {
        $dur = shell_exec('ffmpeg -i '.$file.' 2>&1');
        if(preg_match("/: Invalid /", $dur)) {
            return false;
        }

        preg_match("/Duration: (.{2}):(.{2}):(.{2})/", $dur, $duration);
        if(!isset($duration[1])){
            return false;
        }

        $hours = $duration[1];
        $minutes = $duration[2];
        $seconds = $duration[3];

        return $hours.':'.$minutes.':'.$seconds;
    }
}
