<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;
use phpDocumentor\Reflection\Types\True_;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;

class Image
{
    public function savingImg($file, $path, $nameFile)
    {
        if (!Storage::disk(env('FILESYSTEM_DRIVER'))->exists($path . '/' . $nameFile)) {
            try {
                $file->storePubliclyAs( $path, $nameFile, env("FILESYSTEM_DRIVER"));
                Storage::disk(env('FILESYSTEM_DRIVER'))->setVisibility($path.'/'.$nameFile, 'public');
                return true;
            } catch (FileException $exception) {
                return $exception;
            }
        } else {
            try {
                Storage::disk(env('FILESYSTEM_DRIVER'))->delete($path . '/' . $nameFile);
                try {
                    $file->storePubliclyAs( $path, $nameFile, env("FILESYSTEM_DRIVER"));
                    Storage::disk(env('FILESYSTEM_DRIVER'))->setVisibility($path.'/'.$nameFile, 'public');
                    return true;
                } catch (FileException $exception) {
                    return $exception;
                }
            } catch (FileException $exception) {
                return $exception;
            }
        }
    }

    public function deleteImg($path, $nameFile)
    {
        if(Storage::disk(env('FILESYSTEM_DRIVER'))->exists($path.'/'.$nameFile)) {
            try {
                Storage::disk(env('FILESYSTEM_DRIVER'))->delete($path.'/'.$nameFile);
                return true;
            }catch (FileException $exception) {
                return $exception;
            }
        } else {
            return true;
        }
    }

    public function createDirectory($path)
    {
        Storage::disk(env('FILESYSTEM_DRIVER'))->makeDirectory($path);
        Storage::disk(env('FILESYSTEM_DRIVER'))->setVisibility($path, 'public');
    }

    public function deleteDirectory($path)
    {
        try {
            Storage::disk(env('FILESYSTEM_DRIVER'))->deleteDirectory($path);
            return null;
        }catch (FileException $exception) {
            return $exception;
        }
    }
}
