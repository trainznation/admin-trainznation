<?php


namespace App\Helpers;


use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class FileTransfert
{
    public function uploadFile($file, $path, $namefile)
    {
        if(!Storage::disk(env('FILESYSTEM_DRIVER'))->exists($path.'/'.$namefile)) {
            try {
                $file->storeAs($path, $namefile, env('FILESYSTEM_DRIVER'));
                Storage::disk(env('FILESYSTEM_DRIVER'))->setVisibility($path.'/'.$namefile, 'public');
                return true;
            }catch (FileException $exception) {
                return $exception;
            }
        }else{
            try {
                Storage::disk(env('FILESYSTEM_DRIVER'))->delete($path.'/'.$namefile);
                try {
                    $file->storeAs($path, $namefile, env('FILESYSTEM_DRIVER'));
                    Storage::disk(env('FILESYSTEM_DRIVER'))->setVisibility($path.'/'.$namefile, 'public');
                    return true;
                }catch (FileException $exception) {
                    return $exception;
                }
            }catch (FileException $exception) {
                return $exception;
            }
        }
    }

    public function deleteFile($path, $namefile) {
        if(Storage::disk(env('FILESYSTEM_DRIVER'))->exists($path.'/'.$namefile)) {
            try {
                Storage::disk(env('FILESYSTEM_DRIVER'))->delete($path.'/'.$namefile);
                return true;
            }catch (FileException $exception) {
                return $exception;
            }
        }else{
            return true;
        }
    }
}
