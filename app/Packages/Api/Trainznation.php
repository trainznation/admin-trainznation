<?php


namespace App\Packages\Api;


use Illuminate\Support\Facades\Http;

class Trainznation
{
    /**
     * @var mixed
     */
    protected $endpoint;
    protected static $endpoint_static;

    protected $options = ["verify" => false];

    public function __construct()
    {
        $this->endpoint = env("API_ENDPOINT");
        self::$endpoint_static = env("API_ENDPOINT");
    }

    public function authUser($email, $password)
    {
        return Http::withOptions($this->options)->post($this->endpoint.'/auth/login', [
            'email' => $email,
            'password' => $password
        ]);
    }

    public function registerUser($name, $email, $password)
    {
        return Http::withOptions($this->options)->post($this->endpoint.'/auth/register', [
            'name' => $name,
            'email' => $email,
            'password' => $password
        ]);
    }

    public function get($uri, $parameters = [])
    {
        $response = Http::withOptions($this->options)->withToken(session('api_token'))->get($this->endpoint.$uri, $parameters);
        if($response->status() == '401'){return redirect()->route('login');} else {return $response;}
    }

    public function post($uri, $parameters = [])
    {
        $response = Http::withOptions($this->options)->withToken(session('api_token'))->post($this->endpoint.$uri, $parameters);
        if($response->status() == '401'){return redirect()->route('login');} else {return $response;}
    }

    public function put($uri, $parameters = [])
    {
        $response = Http::withOptions($this->options)->withToken(session('api_token'))->put($this->endpoint.$uri, $parameters);
        if($response->status() == '401'){return redirect()->route('login');} else {return $response;}
    }

    public function delete($uri, $parameters = [])
    {
        $response = Http::withOptions($this->options)->withToken(session('api_token'))->delete($this->endpoint.$uri, $parameters);
        if($response->status() == '401'){return redirect()->route('login');} else {return $response;}
    }
}
