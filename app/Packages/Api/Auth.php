<?php


namespace App\Packages\Api;


use Illuminate\Support\Facades\Http;

class Auth extends Trainznation
{
    public function me()
    {
        return $this->post('/auth/me');
    }

    public function logout()
    {
        return $this->post('/auth/logout');
    }

    public function refresh()
    {
        return $this->post('/auth/refresh');
    }

    public static function Mee()
    {
        return Http::withOptions(['verify' => false])->withToken(session('api_token'))->post(self::$endpoint_static."/auth/me");
    }
}
