<?php

use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "route", "namespace" => "Route", "middleware" => ['connect']], function () {
    Route::get('/', ["as" => "Route.index", "uses" => "RouteController@index"]);
    Route::get('create', ["as" => "Route.create", "uses" => "RouteController@create"]);
    Route::post('create', ["as" => "Route.store", "uses" => "RouteController@store"]);
    Route::get('{route_id}', ["as" => "Route.show", "uses" => "RouteController@show"]);
    Route::get('{route_id}/edit', ["as" => "Route.edit", "uses" => "RouteController@edit"]);
    Route::put('{route_id}/edit', ["as" => "Route.update", "uses" => "RouteController@update"]);
    Route::get('{route_id}/delete', ["as" => "Route.delete", "uses" => "RouteController@delete"]);

    /*Route::group(["prefix" => '{route_id}/roadmap'], function () {
        Route::group(["prefix" => "{roadmap_id}/version"], function () {
            Route::post('/store', ["as" => "Route.Version.store", "uses" => 'VersionController@store']);
            Route::get('list', 'VersionController@list');
            Route::get('{version_id}', ["as" => "Route.Version.show", "uses" => 'VersionController@show']);
            Route::get('{version_id}/info', ["as" => "Route.Version.get", "uses" => 'VersionController@get']);
            Route::get('{version_id}/tasks', 'VersionController@allTask');

            Route::group(["prefix" => "{version_id}/card"], function() {
                Route::get('{card_id}/edit', 'CategoryCardController@edit');
                Route::put('{card_id}/edit', ["as" => "Version.Card.update", "uses" => 'CategoryCardController@update']);
                Route::get('{card_id}/delete', 'CategoryCardController@delete');
                Route::post('addTask', 'CardTaskController@addTask')->name('Version.Task.store');

                Route::group(["prefix" => "{card_id}/task"], function () {
                    Route::get('{task_id}', 'CardTaskController@edit');
                    Route::put('{task_id}', 'CardTaskController@update')->name('Version.Task.update');
                    Route::get('{task_id}/delete', 'CardTaskController@delete');
                });
            });

            Route::group(["prefix" => '{version_id}/category'], function () {
                Route::post('/listCards', 'VersionCategoryController@listCards');
                Route::post('', ["as" => "Version.Category.store", "uses" => "VersionCategoryController@store"]);
                Route::get('{category_id}/delete', ["as" => "Version.Category.delete", "uses" => "VersionCategoryController@delete"]);
                Route::post('/addCard', ["as" => "Category.Card.store", "uses" => "CategoryCardController@addCard"]);
            });
        });
    });*/

    Route::group(["prefix" => "{route_id}/version"], function () {
        Route::post('/store', ["as" => "Route.Version.store", "uses" => "RouteVersionController@store"]);
        Route::get('{version_id}', ["as" => "Route.Version.show", "uses" => "RouteVersionController@show"]);
        Route::get('{version_id}/info', ["as" => "Route.Version.info", "uses" => "RouteVersionController@info"]);
        Route::get('{version_id}/publish', ["as" => "Route.Version.publish", "uses" => "RouteVersionController@publish"]);

        Route::group(["prefix" => "{version_id}/tasks"], function () {
            Route::post('/store', ["as" => "Version.Task.store", "uses" => "RouteVersionTaskController@store"]);
            Route::get('{task_id}', ["as" => "Version.Task.edit", "uses" => "RouteVersionTaskController@edit"]);
            Route::put('{task_id}', ["as" => "Version.Task.update", "uses" => "RouteVersionTaskController@update"]);
            Route::get('{task_id}/delete', ["as" => "Version.Task.delete", "uses" => "RouteVersionTaskController@delete"]);
        });
    });

    Route::group(["prefix" => "{route_id}/gallery"], function () {
        Route::group(["prefix" => "category"], function () {
            Route::get('list', 'RouteGalleryCategoryController@list');
            Route::post('/', ["as" => "Route.Gallery.Category.store", "uses" => 'RouteGalleryCategoryController@store']);
            Route::get('{category_id}', 'RouteGalleryCategoryController@get');
        });

        Route::post('addFile', 'RouteGalleryController@addFile');
        Route::get('{gallery_id}/delete', 'RouteGalleryController@delete');
    });

    Route::group(["prefix" => "{route_id}/download"], function () {
        Route::get('list', 'RouteDownloadController@list');
        Route::post('', ["as" => "Route.Download.store", "uses" => "RouteDownloadController@store"]);
        Route::delete('{download_id}', ["as" => "Route.Download.delete", "uses" => "RouteDownloadController@delete"]);
    });

    Route::group(['prefix' => "{route_id}/session"], function () {
        Route::get('list', 'RouteSessionController@list');
        Route::post('/', ["as" => 'Route.Session.store', 'uses' => 'RouteSessionController@store']);
        Route::delete('{session_id}', 'RouteSessionController@delete');
    });
});
