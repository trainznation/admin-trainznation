<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "tutoriel", "namespace" => "Tutoriel", 'middleware' => ['connect']], function () {
    Route::group(["prefix" => "category"], function () {
        Route::get('/', ["as" => "Tutoriel.Category.index", "uses" => "TutorielCategoryController@index"]);
        Route::post('/', ["as" => "Tutoriel.Category.store", "uses" => "TutorielCategoryController@store"]);
        Route::get('list', ["as" => "Tutoriel.Category.list", "uses" => "TutorielCategoryController@list"]);
        Route::get('{category_id}', ["as" => "Tutoriel.Category.get", "uses" => "TutorielCategoryController@get"]);
        Route::delete('{category_id}', ["as" => "Tutoriel.Category.delete", "uses" => "TutorielCategoryController@delete"]);
    });

    Route::get('/', ["as" => "Tutoriel.index", "uses" => "TutorielController@index"]);
    Route::get('list', ["as" => "Tutoriel.list", "uses" => "TutorielController@list"]);
    Route::get('create', ["as" => "Tutoriel.create", "uses" => "TutorielController@create"]);
    Route::post('create', ["as" => "Tutoriel.store", "uses" => "TutorielController@store"]);
    Route::get('{tutoriel_id}', ["as" => "Tutoriel.show", "uses" => "TutorielController@show"]);
    Route::get('{tutoriel_id}/edit', ["as" => "Tutoriel.edit", "uses" => "TutorielController@edit"]);
    Route::put('{tutoriel_id}/edit', ["as" => "Tutoriel.update", "uses" => "TutorielController@update"]);
    Route::get('{tutoriel_id}/delete', ["as" => "Tutoriel.delete", "uses" => "TutorielController@delete"]);
    Route::get('{tutoriel_id}/publish', ["as" => "Tutoriel.publish", "uses" => "TutorielController@publish"]);
    Route::get('{tutoriel_id}/dispublish', ["as" => "Tutoriel.dispublish", "uses" => "TutorielController@dispublish"]);
});
