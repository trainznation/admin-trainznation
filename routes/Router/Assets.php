<?php

use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "asset", "namespace" => "Asset", "middleware" => ['connect']], function () {
    Route::group(["prefix" => "category"], function () {
        Route::get('/', ["as" => "Asset.Category.index", "uses" => "CategoryController@index"]);
        Route::get('list', ["as" => "Asset.Category.list", "uses" => "CategoryController@list"]);
        Route::post('/', ["as" => "Asset.Category.store", "uses" => "CategoryController@store"]);
        Route::get('{category_id}/delete', ["as" => "Asset.Category.destroy", "uses" => "CategoryController@destroy"]);
    });

    Route::get('/', ["as" => "Asset.index", "uses" => "AssetController@index"]);
    Route::get('create', ["as" => "Asset.create", "uses" => "AssetController@create"]);
    Route::post('create', ["as" => "Asset.store", "uses" => "AssetController@store"]);
    Route::get('{asset_id}', ["as" => "Asset.show", "uses" => "AssetController@show"]);
    Route::get('{asset_id}/edit', ["as" => "Asset.edit", "uses" => "AssetController@edit"]);
    Route::put('{asset_id}/edit', ["as" => "Asset.update", "uses" => "AssetController@update"]);
    Route::delete('{asset_id}', ['as' => "Asset.delete", "uses" => 'AssetController@destroy']);
    Route::get('{asset_id}/publish', ['as' => "Asset.publish", "uses" => 'AssetController@publish']);
    Route::get('{asset_id}/dispublish', ['as' => "Asset.dispublish", "uses" => 'AssetController@dispublish']);
    Route::post('{asset_id}/addSketchfab', ["as" => "Asset.addSketchfab", "uses" => "AssetController@addSketchfab"]);
    Route::post('{asset_id}/addPrice', ["as" => "Asset.addPrice", "uses" => "AssetController@addPrice"]);

    Route::group(["prefix" => "{asset_id}/kuid"], function () {
        Route::post('/', ["as" => "Asset.Kuid.store", "uses" => "KuidController@store"]);
        Route::delete('{kuid_id}', ["as" => "Asset.Kuid.delete", "uses" => "KuidController@delete"]);
    });

    Route::group(["prefix" => "{asset_id}/tags"], function () {
        Route::post('/', ["as" => "Asset.Tags.store", "uses" => "TagsController@store"]);
        Route::delete('{tag}', ["as" => "Asset.Tags.delete", "uses" => "TagsController@delete"]);
    });
});
