<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "support", "namespace" => "Support", "middleware" => ["connect"]], function () {
    Route::group(["prefix" => "category"], function () {
        Route::get('/', ["as" => "Support.Category.index", "uses" => "SupportCategoryController@index"]);
        Route::post('/', 'SupportCategoryController@store')->name('Support.Category.store');
        Route::get('{category_id}', ["as" => "Support.Category.get", "uses" => "SupportCategoryController@get"]);
        Route::put('{category_id}', ["as" => "Support.Category.update", "uses" => "SupportCategoryController@update"]);
        Route::delete('{category_id}', ["as" => "Support.Category.delete", "uses" => "SupportCategoryController@delete"]);
    });

    Route::group(["prefix" => "sector"], function () {
        Route::get('/', ["as" => "Support.Sector.index", "uses" => "SupportSectorController@index"]);
        Route::post('/', 'SupportSectorController@store')->name('Support.Sector.store');
        Route::get('{sector_id}', ["as" => "Support.Sector.get", "uses" => "SupportSectorController@get"]);
        Route::put('{sector_id}', ["as" => "Support.Sector.update", "uses" => "SupportSectorController@update"]);
        Route::delete('{sector_id}', ["as" => "Support.Sector.delete", "uses" => "SupportSectorController@delete"]);

        Route::get('{category_id}/search', 'SupportSectorController@searchByCategory');
    });

    Route::group(["prefix" => "priorite"], function () {
        Route::get('/', ["as" => "Support.Priorite.index", "uses" => "SupportPrioriteController@index"]);
        Route::post('/', 'SupportPrioriteController@store')->name('Support.Priorite.store');
        Route::get('{priority_id}', ["as" => "Support.Priorite.get", "uses" => "SupportPrioriteController@get"]);
        Route::put('{priority_id}', ["as" => "Support.Priorite.update", "uses" => "SupportPrioriteController@update"]);
        Route::delete('{priority_id}', ["as" => "Support.Priorite.delete", "uses" => "SupportPrioriteController@delete"]);
    });

    Route::group(["prefix" => "source"], function () {
        Route::get('/', ["as" => "Support.Source.index", "uses" => "SupportSourceController@index"]);
        Route::post('/', 'SupportSourceController@store')->name('Support.Source.store');
        Route::get('{source_id}', ["as" => "Support.Source.get", "uses" => "SupportSourceController@get"]);
        Route::put('{source_id}', ["as" => "Support.Source.update", "uses" => "SupportSourceController@update"]);
        Route::delete('{source_id}', ["as" => "Support.Source.delete", "uses" => "SupportSourceController@delete"]);
    });

    Route::group(["prefix" => "status"], function () {
        Route::get('/', ["as" => "Support.Status.index", "uses" => "SupportStatusController@index"]);
        Route::post('/', 'SupportStatusController@store')->name('Support.Status.store');
        Route::get('{status_id}', ["as" => "Support.Status.get", "uses" => "SupportStatusController@get"]);
        Route::put('{status_id}', ["as" => "Support.Status.update", "uses" => "SupportStatusController@update"]);
        Route::delete('{status_id}', ["as" => "Support.Status.delete", "uses" => "SupportStatusController@delete"]);
    });

    Route::get('/', ["as" => "Support.index", "uses" => "SupportController@index"]);
    Route::post('list', ["as" => "Support.list", "uses" => "SupportController@list"]);
    Route::post('/', ["as" => "Support.store", "uses" => "SupportController@store"]);
    Route::get('{ticket_id}', ["as" => "Support.show", "uses" => "SupportController@show"]);
    Route::get('{ticket_id}/conv', ["as" => "Support.conv", "uses" => "SupportController@conv"]);
    Route::post('{ticket_id}/conv', ["as" => "Support.send", "uses" => "SupportController@send"]);
    Route::get('{ticket_id}/edit', ["as" => "Support.edit", "uses" => "SupportController@edit"]);
    Route::put('{ticket_id}/edit', ["as" => "Support.update", "uses" => "SupportController@update"]);
    Route::get('{ticket_id}/delete', ["as" => "Support.delete", "uses" => "SupportController@delete"]);
});
