<?php

use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "blog", "namespace" => "Blog", 'middleware' => ['connect']], function () {
    Route::group(["prefix" => "category"], function () {
        Route::get('/', ["as" => "Blog.Category.index", "uses" => "CategoryController@index"]);
        Route::get('list', ["as" => "Blog.Category.list", "uses" => "CategoryController@list"]);
        Route::post('/', ["as" => "Blog.Category.store", "uses" => "CategoryController@store"]);
        Route::get('{category_id}/delete', ["as" => "Blog.Category.delete", "uses" => "CategoryController@delete"]);
    });

    Route::get('/', ["as" => "Blog.index", "uses" => "BlogController@index"]);
    Route::get('create', ["as" => "Blog.create", "uses" => "BlogController@create"]);
    Route::post('create', ["as" => "Blog.store", "uses" => "BlogController@store"]);
    Route::get('{article_id}', ["as" => "Blog.show", "uses" => "BlogController@show"]);
    Route::get('{article_id}/edit', ["as" => "Blog.edit", "uses" => "BlogController@edit"]);
    Route::put('{article_id}/edit', ["as" => "Blog.update", "uses" => "BlogController@update"]);
    Route::get('{article_id}/delete', ["as" => "Blog.delete", "uses" => "BlogController@delete"]);

    Route::get('{article_id}/dispublish', ["as" => "Blog.dispublish", "uses" => "BlogController@dispublish"]);
    Route::get('{article_id}/publish', ["as" => "Blog.publish", "uses" => "BlogController@publish"]);

    Route::group(["prefix" => "{article_id}/comment"], function () {
        Route::get('{comment_id}/approve', 'CommentController@approve');
        Route::get('{comment_id}/disapprove', 'CommentController@disapprove');
    });
});
