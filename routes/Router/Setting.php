<?php

use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "settings", "namespace" => "Settings", "middleware" => ['connect']], function () {
    Route::group(["prefix" => "announcement"], function () {
        Route::get('', ["as" => "Settings.Announcement.index", "uses" => "AnnouncementController@index"]);
        Route::get('list', 'AnnouncementController@list');
        Route::post('', 'AnnouncementController@store');
        Route::get('{announcement_id}', 'AnnouncementController@delete');
    });

    Route::group(["prefix" => "slideshow"], function () {
        Route::get('/', ["as" => "Settings.Slideshow.index", "uses" => "SlideshowController@index"]);
        Route::post('/', ["as" => "Settings.Slideshow.store", "uses" => "SlideshowController@store"]);
        Route::get('{slide_id}', ["as" => "Settings.Slideshow.delete", "uses" => "SlideshowController@delete"]);
    });

    Route::group(["prefix" => "user"], function () {
        Route::get('/', ["as" => "Settings.User.index", "uses" => "UserController@index"]);
        Route::get('{user_id}', ["as" => "Settings.User.show", "uses" => "UserController@show"]);
    });
});
