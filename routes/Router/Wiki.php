<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "wiki", "namespace" => "Wiki", 'middleware' => ['connect']], function () {
    Route::group(["prefix" => "category"], function () {
        Route::get('/', ["as" => "Wiki.Category.index", "uses" => "WikiCategoryController@index"]);
        Route::post('search', 'WikiCategoryController@search');
        Route::post('/', ["as" => "Wiki.Category.store", "uses" => "WikiCategoryController@store"]);
        Route::get('{category_id}', 'WikiCategoryController@delete');
    });

    Route::group(["prefix" => "sector"], function () {
        Route::get('/', ["as" => "Wiki.Sector.index", "uses" => "WikiSectorController@index"]);
        Route::post('search', "WikiSectorController@search");
        Route::post('/', ["as" => "Wiki.Sector.store", "uses" => "WikiSectorController@store"]);
        Route::get('{sector_id}', "WikiSectorController@delete");
    });

    Route::get('/', ["as" => "Wiki.index", "uses" => "WikiController@index"]);
    Route::post('search', "WikiController@search");
    Route::get('create', ["as" => "Wiki.create", "uses" => "WikiController@create"]);
    Route::post('create', ["as" => "Wiki.store", "uses" => "WikiController@store"]);
    Route::get('{article_id}', ["as" => "Wiki.show", "uses" => "WikiController@show"]);
    Route::get('{article_id}/edit', ["as" => "Wiki.edit", "uses" => "WikiController@edit"]);
    Route::put('{article_id}/edit', ["as" => "Wiki.update", "uses" => "WikiController@update"]);
    Route::get('{article_id}/delete', ["as" => "Wiki.delete", "uses" => "WikiController@delete"]);
    Route::get('{article_id}/publish', ["as" => "Wiki.show", "uses" => "WikiController@publish"]);
    Route::get('{article_id}/unpublish', ["as" => "Wiki.show", "uses" => "WikiController@unpublish"]);
});
