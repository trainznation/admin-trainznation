<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', ["as" => "login", "uses" => "Auth\AuthController@loginForm"]);
Route::post('login', ["as" => "postLogin", "uses" => "Auth\AuthController@login"]);
Route::get('logout', ["as" => "logout", "uses" => "Auth\AuthController@logout"]);

Route::get('/', ["as" => "home", "uses" => "PagesController@index"])->middleware('connect');
Route::get('/quick-search', 'PagesController@quickSearch')->name('quick-search')->middleware('connect');

include('Router/Assets.php');
include('Router/Blog.php');
include('Router/Route.php');
include('Router/Tutoriel.php');
include('Router/Setting.php');
include('Router/Support.php');
include('Router/Wiki.php');

Route::get('/me', 'Auth\AuthController@me');
Route::get('/notif/{user_id}/{notif_id}', 'Auth\AuthController@readNotif');
Route::post('/upload', 'CoreController@upload');

Route::get('test', 'TestController@test');
