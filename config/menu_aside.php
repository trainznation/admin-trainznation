<?php
// Aside menu
return [

    'items' => [
        // Dashboard
        [
            'title' => 'Tableau de Bord',
            'root' => true,
            'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
            'page' => '/',
            'new-tab' => false,
        ],
        [
            'title' => "Articles",
            'root' => true,
            'icon' => 'media/svg/icons/Devices/TV2.svg',
            'submenu' => [
                [
                    'title' => 'Gestion des catégories',
                    'bullet' => 'dot',
                    'root' => true,
                    'page' => '/blog/category'
                ],
                [
                    'title' => 'Gestion des articles',
                    'bullet' => 'dot',
                    'root' => true,
                    'page' => '/blog'
                ],
            ]
        ],
        [
            'title' => 'Objets',
            'root' => true,
            'icon' => 'media/svg/icons/Shopping/Box2.svg',
            'submenu' => [
                [
                    'title' => 'Gestion des catégories',
                    'bullet' => 'dot',
                    'root' => true,
                    'page' => '/asset/category'
                ],
                [
                    'title' => 'Gestion des objets',
                    'bullet' => 'dot',
                    'root' => true,
                    'page' => '/asset'
                ],
            ]
        ],
        [
            'title' => 'Route',
            'root' => true,
            'icon' => 'media/svg/icons/Code/Git4.svg',
            'page' => '/route'
        ],
        [
            'title' => 'Tutoriel',
            'root' => true,
            'icon' => 'media/svg/icons/Media/Youtube.svg',
            'submenu' => [
                [
                    'title' => 'Gestion des catégories',
                    'bullet' => 'dot',
                    'root' => true,
                    'page' => '/tutoriel/category'
                ],
                [
                    'title' => 'Gestion des tutoriels',
                    'bullet' => 'dot',
                    'root' => true,
                    'page' => '/tutoriel'
                ],
            ]
        ],
        [
            'title' => 'Wiki',
            'root' => true,
            'icon' => 'media/svg/icons/Text/H2.svg',
            'submenu' => [
                [
                    'title' => 'Gestion des catégories',
                    'bullet' => 'dot',
                    'root' => true,
                    'page' => '/wiki/category'
                ],
                [
                    'title' => 'Gestion des secteurs',
                    'bullet' => 'dot',
                    'root' => true,
                    'page' => '/wiki/sector'
                ],
                [
                    'title' => 'Gestion des articles',
                    'bullet' => 'dot',
                    'root' => true,
                    'page' => '/wiki'
                ],
            ]
        ],
        [
            'title' => "Conversations",
            'root' => true,
            'icon' => 'media/svg/icons/Communication/Group-chat.svg',
            'page' => '/support'
        ],
        [
            'title' => 'Parametrage',
            'root' => true,
            'icon' => 'media/svg/icons/General/Settings-2.svg',
            'submenu' => [
                [
                    'title' => 'Annonce',
                    'bullet' => 'dot',
                    'root' => true,
                    'page' => '/settings/announcement'
                ],
                [
                    'title' => 'Slideshow',
                    'bullet' => 'dot',
                    'root' => true,
                    'page' => '/settings/slideshow'
                ],
                [
                    'title' => 'Utilisateurs',
                    'bullet' => 'dot',
                    'root' => true,
                    'page' => '/settings/user'
                ],
                [
                    'title' => 'Support',
                    'bullet' => 'dot',
                    'submenu' => [
                        [
                            'title' => 'Gestion des catégories',
                            'bullet' => 'dot',
                            'page' => '/support/category'
                        ],
                        [
                            'title' => 'Gestion des secteurs',
                            'bullet' => 'dot',
                            'page' => '/support/sector'
                        ],
                        [
                            'title' => 'Gestion des priorités',
                            'bullet' => 'dot',
                            'page' => '/support/priorite'
                        ],
                        [
                            'title' => 'Gestion des sources',
                            'bullet' => 'dot',
                            'page' => '/support/source'
                        ],
                        [
                            'title' => 'Gestion des status',
                            'bullet' => 'dot',
                            'page' => '/support/status'
                        ],
                    ]
                ],
            ],
        ]

    ]

];
