<?php

/*
 * You can place your custom package configuration in here.
 */
return [
    "api_key" => env('SKETCHFAB_API_KEY', null),
    "supported_format" => [
        '3dc',
        '3ds',
        'ac',
        'blend',
        'bvh',
        'dae',
        'dw',
        'dwf',
        'fbx',
        'flt',
        'geo',
        'gta',
        'iv',
        'ive',
        'kmz',
        'lwo',
        'lws',
        'obj',
        'osg',
        'osgb',
        'osgt',
        'ply',
        'shp',
        'stl',
        'vpk',
        'wrl',
        'x',
    ]
];
