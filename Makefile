sy := php artisan
sp := php
ipAddr := 192.168.0.18
user := root
path := /www/wwwroot/
namePath := admin.trainznation.tk/current
repository := https://gitlab.com/trainznation/admin-trainznation.git
ssh := ssh $(user)@$(ipAddr)

.DEFAULT_GOAL := help
.PHONY: help
help: ## Affiche cette aide
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: clear
clear: ## Nettoie toues les caches du système
	$(sy) down
	$(sy) clear-compiled
	$(sy) optimize
	$(sy) cache:clear
	$(sy) config:clear
	$(sy) event:clear
	$(sy) route:clear
	$(sy) view:clear
	$(sy) up

.PHONY: coverage
test: ## Effectue un test par artisan avec coverage
	$(sy) test --coverage-html=coverage

.PHONY: deploy_install
deploy_install: ## Déploie le site pour installation primaire
	echo "############ Deploiement Initial ##############"
	clear
	$(ssh) cd $(path)
	@echo "############ Clone du repository $(repository) dans le dossier $(path)$(namePath) ##############"
	$(ssh) git clone $(repository) $(path)$(namePath)
	@echo "############ Installation des dépendances ##############"
	$(ssh) cd $(path)$(namePath) && composer install
	@echo "############ Configuration de composer ##############"
	$(ssh) cd $(path)$(namePath) && cp .env.example .env
	$(ssh) cd $(path)$(namePath) && chmod -R 777 storage/ bootstrap/
	@echo "############ Tache Node ##############"
	$(ssh) cd $(path)$(namePath) && npm install
	$(ssh) cd $(path)$(namePath) && npm run prod
	@echo "############ Tache Laravel ##############"
	$(ssh) cd $(path)$(namePath) && $(sy) key:generate
	@echo "############ Installation Terminer ##############"

.PHONY: deploy_update
deploy_update: ## Deploie le site pour mise à jour
	@echo "############ Deploiement Initial ##############"
	clear
	$(ssh) cd $(path)
	$(ssh) cd $(path)$(namePath) && git pull
	$(ssh) cd $(path)$(namePath) && composer install
	$(ssh) cd $(path)$(namePath) && composer update
	@echo "############ Tache Node ##############"
	$(ssh) cd $(path)$(namePath) && npm install
	$(ssh) cd $(path)$(namePath) && npm run prod
	$(ssh) cd $(path)$(namePath) && clear
	@echo "############ Mise à jour Terminer ##############"

composer.lock: composer.json
	@echo "############ Mise à jour des dépendances ##############"
	$(ssh) composer update

vendor: composer.lock
	@echo "############ Installation des dépendances ##############"
	$(ssh) composer install

