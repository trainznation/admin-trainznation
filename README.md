# Installation Initial
`git clone https://gitlab.com/trainznation/admin-trainznation.git admin.trainznation.tk`

``composer install --no-dev``

``npm install``

``npm run dev``

``cp .env.example .env`` (Configurer ce fichier)
- ne pas oublier de changer le FILESYSTEM DRIVER à sftp

``php artisan key:generate``

``chmod -R 777 storage/ bootstrap/ public/``

``screen`` & ``php artisan horizon``

# Update

``git pull``

``composer update``

``npm install && npm run dev``

``php artisan cache:clear && php artisan config:clear && php artisan route:clear && php artisan view:clear && php artisan clear``
