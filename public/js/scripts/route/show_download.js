/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 14);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/scripts/route/show_download.js":
/*!*****************************************************!*\
  !*** ./resources/js/scripts/route/show_download.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

//const spinning = 'spinner spinner-right spinner-white pr-15'
var listeDownload;
var route = document.querySelector('#route');
$.ajax({
  url: "/route/".concat(route.dataset.id, "/download/list"),
  success: function success(data) {
    loadListDownload(data.data);
  }
});

function loadListDownload(data) {
  listeDownload = $("#liste_download").KTDatatable({
    data: {
      type: 'local',
      source: data,
      pageSize: 10
    },
    // layout definition
    layout: {
      scroll: false,
      // enable/disable datatable scroll both horizontal and vertical when needed.
      // height: 450, // datatable's body's fixed height
      footer: false // display/hide footer

    },
    // column sorting
    sortable: true,
    pagination: true,
    search: {
      input: $('#search_query_download'),
      key: 'generalSearch'
    },
    columns: [{
      field: 'designation',
      title: 'Designation',
      sortable: false,
      width: 90,
      template: function template(row) {
        return "\n                        V.".concat(row.version, ":").concat(row.build, "\n                    ");
      }
    }, {
      field: 'published',
      title: "Publication",
      width: 120,
      textAlign: 'center',
      template: function template(row) {
        var statePublish = {
          0: {
            'icon': 'fa fa-close',
            'color': 'danger',
            'text': 'Non Publier'
          },
          1: {
            'icon': 'ki ki-check',
            'color': 'success',
            'text': 'Publier'
          }
        };
        return "<span class=\"label label-inline label-pill label-".concat(statePublish[row.published].color, "\"><i class=\"").concat(statePublish[row.published].icon, " text-white\"></i>&nbsp; ").concat(statePublish[row.published].text, "</span>");
      }
    }, {
      field: 'alpha',
      title: 'Type',
      width: 120,
      textAlign: 'center',
      template: function template(row) {
        var type = {
          0: {
            'color': 'light-dark'
          },
          1: {
            'color': 'success'
          }
        };
        return "\n                        <div class=\"symbol-group symbol-hover\">\n                            <div class=\"symbol symbol-circle symbol-".concat(type[row.alpha].color, "\" data-toggle=\"tooltip\" title=\"Alpha\">\n                                <span class=\"symbol-label\">A</span>\n                            </div>\n                            <div class=\"symbol symbol-circle symbol-").concat(type[row.beta].color, "\" data-toggle=\"tooltip\" title=\"Beta\">\n                                <span class=\"symbol-label\">B</span>\n                            </div>\n                            <div class=\"symbol symbol-circle symbol-").concat(type[row.release].color, "\" data-toggle=\"tooltip\" title=\"Release\">\n                                <span class=\"symbol-label\">R</span>\n                            </div>\n                        </div>\n                    ");
      }
    }, {
      field: 'Action',
      title: 'Action',
      template: function template(row) {
        return "\n                    <button data-href=\"/route/".concat(row.route_id, "/download/").concat(row.id, "\" class=\"btn btn-icon btn-xs btn-danger\" data-toggle=\"tooltip\" title=\"Supprimer le t\xE9l\xE9chargement\" onclick=\"deleteDownload($(this))\"><i class=\"fa fa-trash\"></i> </button>\n                    ");
      }
    }]
  });
}

function deleteDownload(btn) {
  var uri = btn.attr('data-href');
  btn.html("<div class=\"".concat(spinning, "\"></div>"));
  $.ajax({
    url: uri,
    method: 'DELETE',
    success: function success(data) {
      btn.html('<i class="fa fa-trash"></i>');
      toastr.success(data.data.message);
      btn.parents('tr').fadeOut();
    },
    error: function error(_error) {
      btn.html('<i class="fa fa-trash"></i>');
      console.error(_error);
      toastr.error('Erreur Système, consulter les logs', "Erreur 500");
    }
  });
}

/***/ }),

/***/ 14:
/*!***********************************************************!*\
  !*** multi ./resources/js/scripts/route/show_download.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\site\admin.trainznation\resources\js\scripts\route\show_download.js */"./resources/js/scripts/route/show_download.js");


/***/ })

/******/ });