/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 15);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/scripts/route/show_gallery.js":
/*!****************************************************!*\
  !*** ./resources/js/scripts/route/show_gallery.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var navs = document.querySelectorAll('.navi-gallery');
var content = document.querySelector('#js-content');
var route_id = $("#route_id").val();
var dropImages = "#dropImages";
var category_id;
Array.from(navs).forEach(function (nav) {
  nav.addEventListener('click', function (e) {
    call(nav.dataset.route, nav.dataset.categoryid);
  });
});

function call(route, categoryid) {
  KTApp.block(content, {
    overlayColor: '#000000',
    state: 'danger',
    message: 'Veuillez patienter...'
  });
  $.ajax({
    url: "/route/".concat(route, "/gallery/category/").concat(categoryid),
    success: function success(data) {
      KTApp.unblock(content);
      var galleries = data.data.galleries;
      content.innerHTML = '';

      if (Array.from(galleries).length === 0) {
        content.innerHTML = "\n                    <div class=\"col-md-12 bg-gray-200 text-center\">\n                        <h3 class=\"p-7\"><i class=\"flaticon-warning-sign icon-2x\"></i> Aucune image dans cette cat\xE9gorie de gallerie</h3>\n                    </div>\n                ";
      } else {
        Array.from(galleries).forEach(function (gallery) {
          content.innerHTML += "\n                <div class=\"col-md-4\">\n                <div class=\"card card-custom overlay mb-5\">\n                    <div class=\"card-body p-0\">\n                        <div class=\"overlay-wrapper\">\n                            <img src=\"".concat(gallery.image, "\" alt=\"\" class=\"w-100 rounded\">\n                        </div>\n                            <div class=\"overlay-layer align-items-start justify-content-end pt-5 pr-5\">\n                                <a href=\"").concat(gallery.image, "\" data-lightbox=\"image-").concat(gallery.category, "\" class=\"btn btn-clean btn-icon\">\n                                    <i class=\"flaticon-eye icon-lg text-primary\"></i>\n                                </a>\n                                <a data-href=\"/route/").concat(route, "/gallery/").concat(gallery.id, "/delete\" class=\"btn btn-clean btn-icon\" onclick=\"deleteImg($(this))\">\n                                    <i class=\"flaticon2-trash icon-lg text-primary\"></i>\n                                </a>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                ");
        });
      }
    },
    error: function error(err) {
      KTApp.unblock(content);
      toastr.error("Erreur d'affichage de la gallerie");
      console.error(err);
    }
  });
}

function deleteImg(btn) {
  KTApp.block(btn.parents('.overlay'));
  $.ajax({
    url: btn.attr('data-href'),
    success: function success(data) {
      btn.parents('.overlay').fadeOut();
      toastr.success(data.message);
    },
    error: function error(err) {
      KTApp.unblock(btn.parents('.overlay'));
      toastr.error(err.error);
    }
  });
}

function formatSearch(data) {
  if (data.loading) return data.text;
  var markup = "\n        <div class=\"row\">\n            <div class=\"col-md-12\">".concat(data.name, "</div>\n        </div>\n    ");
  return markup;
}

function formatSelect(data) {
  category_id = data.id;
  return data.name || data.text;
}

$("#route_category_id").select2({
  placeholder: 'Rechercher...',
  allowClear: true,
  ajax: {
    url: "/route/".concat(route_id, "/gallery/category/list"),
    dataType: 'json',
    delay: 250,
    data: function data(params) {
      return {
        q: params.term
      };
    },
    processResults: function processResults(data, params) {
      return {
        results: data.items
      };
    },
    cache: true
  },
  escapeMarkup: function escapeMarkup(markup) {
    return markup;
  },
  minimumInputLength: 1,
  templateResult: formatSearch,
  templateSelection: formatSelect,
  language: {
    inputTooShort: function inputTooShort() {
      return "Veuillez entrer au moins 1 caractère...";
    },
    searching: function searching() {
      return "Recherche en cours...";
    },
    errorLoading: function errorLoading() {
      return "Les résultats ne peuvent pas être chargés.";
    }
  }
});

function dropzoneImage() {
  var dropzoneImage = new Dropzone(dropImages, {
    url: "/route/".concat(route_id, "/gallery/addFile"),
    paramName: "file",
    // The name that will be used to transfer the file
    maxFilesize: 10,
    // MB
    addRemoveLinks: true
  });
  dropzoneImage.on('sending', function (file, xhr, formData) {
    formData.append("category_id", category_id);
  });
}

dropzoneImage();

/***/ }),

/***/ 15:
/*!**********************************************************!*\
  !*** multi ./resources/js/scripts/route/show_gallery.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\site\admin.trainznation\resources\js\scripts\route\show_gallery.js */"./resources/js/scripts/route/show_gallery.js");


/***/ })

/******/ });