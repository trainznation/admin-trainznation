/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 16);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/scripts/route/show_roadmap.js":
/*!****************************************************!*\
  !*** ./resources/js/scripts/route/show_roadmap.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var spinning = 'spinner spinner-right spinner-white pr-15';
var Elements = {
  averageVersion: KTUtil.getById('averageVersion'),
  chartAverage: KTUtil.getById('chartAverage'),
  textTaskRegistered: KTUtil.getById('textTaskRegistered'),
  textTaskProgress: KTUtil.getById('textTaskProgress'),
  textTaskFinish: KTUtil.getById('textTaskFinish'),
  naviVersions: document.querySelectorAll('.navi-versions'),
  content: KTUtil.getById('versionContent'),
  dataVersion: '',
  listeCategory: null,
  listeCartes: null,
  listeTasks: null
};
Array.from(Elements.naviVersions).forEach(function (nav) {
  nav.addEventListener('click', function (e) {
    callVersion(nav.dataset.route, nav.dataset.roadmap, nav.dataset.version);
  });
});
$("#formAddVersion").on('submit', function (e) {
  e.preventDefault();
  var form = $("#formAddVersion");
  var uri = form.attr('action');
  var btn = document.querySelector('#btnAddVersion');
  var data = form.serializeArray();
  KTUtil.btnWait(btn, spinning, 'Chargement...');
  $.ajax({
    url: uri,
    method: 'get',
    data: data,
    success: function success(data) {
      KTUtil.btnRelease(btn);
      toastr.success("La Version ".concat(data.data.id, " \xE0 \xE9t\xE9 ajout\xE9 avec succ\xE8s"));
      setTimeout(function () {
        window.location.reload();
      }, 1200);
    },
    error: function error(err) {
      KTUtil.btnRelease(btn);
      console.log(err);
      toastr.error("Erreur Technique, consulter les logs système", "Erreur 500");
    }
  });
});

function blockElement() {
  for (var _len = arguments.length, elements = new Array(_len), _key = 0; _key < _len; _key++) {
    elements[_key] = arguments[_key];
  }

  Array.from(elements).forEach(function (element) {
    KTApp.block(element, {
      overlayColor: '#000000',
      state: 'danger',
      message: 'Veuillez patienter...'
    });
  });
}

function unblockElement() {
  for (var _len2 = arguments.length, elements = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
    elements[_key2] = arguments[_key2];
  }

  Array.from(elements).forEach(function (element) {
    KTApp.unblock(element);
  });
}

function callVersion(route, roadmap, version) {
  blockElement(Elements.chartAverage, Elements.averageVersion, Elements.textTaskRegistered, Elements.content, Elements.textTaskFinish, Elements.textTaskProgress);
  $.ajax({
    url: "/route/".concat(route, "/roadmap/").concat(roadmap, "/version/").concat(version),
    success: function success(data) {
      Elements.dataVersion = data.data;
      unblockElement(Elements.chartAverage, Elements.averageVersion, Elements.textTaskRegistered, Elements.content, Elements.textTaskFinish, Elements.textTaskProgress);
      changeAverageVersion(Elements.averageVersion, {
        version: data.data.version,
        build: data.data.build
      });
      changeChartAverage(Elements.chartAverage, data.data.percent_task);
      changeTextTask({
        registered: data.data.registered_task,
        progress: data.data.progress_task,
        finish: data.data.finish_task
      });
    }
  });
}

/***/ }),

/***/ 16:
/*!**********************************************************!*\
  !*** multi ./resources/js/scripts/route/show_roadmap.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\site\admin.trainznation\resources\js\scripts\route\show_roadmap.js */"./resources/js/scripts/route/show_roadmap.js");


/***/ })

/******/ });