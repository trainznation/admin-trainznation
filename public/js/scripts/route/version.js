/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 20);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/scripts/route/version.js":
/*!***********************************************!*\
  !*** ./resources/js/scripts/route/version.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var spinning = 'spinner spinner-right spinner-white pr-15';
var Elements = {
  averageVersion: KTUtil.getById('averageVersion'),
  chartAverage: KTUtil.getById('chartAverage'),
  textTaskRegistered: KTUtil.getById('textTaskRegistered'),
  textTaskProgress: KTUtil.getById('textTaskProgress'),
  textTaskFinish: KTUtil.getById('textTaskFinish'),
  dataVersion: ''
};

function formatInput() {
  moment.locale('fr');
  $("#started_at, #finished_at").datetimepicker({
    locale: 'fr',
    format: "YYYY-MM-DD HH:mm:ss",
    icons: {
      time: "ki ki-clock",
      date: "ki ki-calendar",
      up: "ki ki-arrow-up",
      down: "ki ki-arrow-down"
    }
  });
}

function changeAverageVersion(el, data) {
  el.innerText = "Version: ".concat(data.version, " | Build: ").concat(data.build);
}

function changeColorChart(value) {
  if (value <= 33) {
    return '#f44336';
  } else if (value > 33 && value <= 66) {
    return '#ffc107';
  } else {
    return '#4caf50';
  }
}

function changeChartAverage(el, data) {
  var element = el;
  var height = parseInt(KTUtil.css(element, 'height'));
  el.innerHTML = null;

  if (!element) {
    return;
  }

  var options = {
    series: [data],
    chart: {
      height: height,
      type: 'radialBar',
      offsetY: 0
    },
    plotOptions: {
      radialBar: {
        startAngle: -90,
        endAngle: 90,
        hollow: {
          margin: 0,
          size: "70%"
        },
        dataLabels: {
          showOn: "always",
          name: {
            show: true,
            fontSize: "13px",
            fontWeight: "700",
            offsetY: -5,
            color: '#9e9e9e'
          },
          value: {
            color: '#616161',
            fontSize: "30px",
            fontWeight: "700",
            offsetY: -40,
            show: true
          }
        },
        track: {
          background: '#c6c3c3',
          strokeWidth: '100%'
        }
      }
    },
    colors: [changeColorChart(data)],
    stroke: {
      lineCap: "round"
    },
    labels: ["Progression"]
  };
  var chart = new ApexCharts(element, options);
  chart.render();
}

function changeTextTask(dataAverage) {
  Elements.textTaskRegistered.innerText = dataAverage.registered;
  Elements.textTaskProgress.innerText = dataAverage.progress;
  Elements.textTaskFinish.innerText = dataAverage.finish;
}

function loadListTasksTable(tasks) {
  $("#liste_tasks").KTDatatable({
    data: {
      type: 'local',
      source: tasks,
      pageSize: 10
    },
    layout: {
      scroll: false,
      // enable/disable datatable scroll both horizontal and vertical when needed.
      // height: 450, // datatable's body's fixed height
      footer: false // display/hide footer

    },
    // column sorting
    sortable: true,
    pagination: true,
    search: {
      input: $('#search_query_task'),
      key: 'generalSearch'
    },
    columns: [{
      field: "id",
      title: "#",
      width: 20
    }, {
      field: 'name',
      title: "Anomalie/Titre"
    }, {
      field: 'description',
      title: "Correction/Description"
    }, {
      field: 'etat',
      title: 'Etat',
      template: function template(row) {
        var status = {
          0: {
            "class": "danger",
            "title": "Non Commencer",
            "icon": "fas fa-times text-white"
          },
          1: {
            "class": "warning",
            "title": "En cours",
            "icon": "fas fa-reload text-white"
          },
          2: {
            "class": "success",
            "title": "Terminer",
            "icon": "fas fa-check-circle text-white"
          }
        };
        return "<span class=\"label label-inline label-".concat(status[row.etat]["class"], "\"><i class=\"").concat(status[row.etat].icon, "\"></i> &nbsp; ").concat(status[row.etat].title, "</span>");
      }
    }, {
      field: 'priority',
      title: 'Priorité',
      template: function template(row) {
        var priorite = {
          0: {
            "class": "success",
            "title": "Basse"
          },
          1: {
            "class": "warning",
            "title": "Moyenne"
          },
          2: {
            "class": "info",
            "title": "Haute"
          },
          3: {
            "class": "danger",
            "title": "Critique"
          }
        };
        return "<span class=\"label label-inline label-".concat(priorite[row.priority.value]["class"], "\">").concat(priorite[row.priority.value].title, "</span>");
      }
    }, {
      field: "actions",
      title: "Actions",
      template: function template(row) {
        return "\n                    <a href=\"#descTask\" data-toggle=\"modal\" class=\"btn btn-sm btn-icon btn-hover-light-primary\" data-content=\"".concat(row.description, "\" onclick=\"appearDescTask($(this))\"><i class=\"fa fa-eye\"></i> </a>\n                    <a href=\"/route/").concat(Elements.dataVersion.route_id, "/version/").concat(Elements.dataVersion.id, "/tasks/").concat(row.id, "\" class=\"btn btn-sm btn-icon btn-hover-light-info\"><i class=\"fa fa-edit\"></i> </a>\n                    <a href=\"/route/").concat(Elements.dataVersion.route_id, "/version/").concat(Elements.dataVersion.id, "/tasks/").concat(row.id, "/delete\" class=\"btn btn-sm btn-icon btn-hover-light-danger\"><i class=\"fa fa-trash\"></i> </a>\n                    ");
      }
    }]
  });
}

function callVersion() {
  $.ajax({
    url: document.querySelector('#version_uri').value,
    success: function success(data) {
      Elements.dataVersion = data.data;
      changeAverageVersion(Elements.averageVersion, Elements.dataVersion);
      changeChartAverage(Elements.chartAverage, Elements.dataVersion.percent_task);
      changeTextTask({
        registered: Elements.dataVersion.registered_task,
        progress: Elements.dataVersion.progress_task,
        finish: Elements.dataVersion.finish_task
      });
      loadListTasksTable(Elements.dataVersion.tasks);
    }
  });
}

function appearDescTask(e) {
  $("#descTask").find('.modal-body').html(e.attr('data-content')).modal('show');
}

$("#formAddTask").on('submit', function (e) {
  e.preventDefault();
  var form = $("#formAddTask");
  var uri = form.attr('action');
  var btn = KTUtil.getById('btnAddTask');
  var data = form.serializeArray();
  KTUtil.btnWait(btn, spinning, 'Chargement');
  $.ajax({
    url: uri,
    method: 'post',
    data: data,
    success: function success(data) {
      KTUtil.btnRelease(btn);
      toastr.success("La T\xE2che ".concat(data.data.name, " \xE0 \xE9t\xE9 ajout\xE9"), "Succès");
      setTimeout(function () {
        window.location.reload();
      }, 1500);
    },
    error: function error(err) {
      KTUtil.btnRelease(btn);
      console.error("Erreur Système, consultez les logs", "Erreur 500");
      console.error(err);
    }
  });
});
new KTImageInput('card_img');
callVersion();
formatInput();

/***/ }),

/***/ 20:
/*!*****************************************************!*\
  !*** multi ./resources/js/scripts/route/version.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\site\admin.trainznation\resources\js\scripts\route\version.js */"./resources/js/scripts/route/version.js");


/***/ })

/******/ });