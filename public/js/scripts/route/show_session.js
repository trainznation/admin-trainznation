/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 17);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/scripts/route/show_session.js":
/*!****************************************************!*\
  !*** ./resources/js/scripts/route/show_session.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var listeSessions;
var route = document.querySelector('#route');
$.ajax({
  url: "/route/".concat(route.dataset.id, "/session/list"),
  success: function success(data) {
    loadListeSession(data.data);
  }
});

function loadListeSession(data) {
  listeSessions = $("#liste_session").KTDatatable({
    data: {
      type: 'local',
      source: data,
      pageSize: 10
    },
    // layout definition
    layout: {
      scroll: false,
      // enable/disable datatable scroll both horizontal and vertical when needed.
      // height: 450, // datatable's body's fixed height
      footer: false // display/hide footer

    },
    // column sorting
    sortable: true,
    pagination: true,
    search: {
      input: $('#search_query_session'),
      key: 'generalSearch'
    },
    columns: [{
      field: 'name',
      title: "Session"
    }, {
      field: 'images',
      title: 'Thumbnail',
      sortable: false,
      width: 120,
      template: function template(row) {
        return "<img src=\"".concat(row.image, "\" alt=\"").concat(row.name, "\" width=\"120\" class=\"img-fluid\">");
      }
    }, {
      field: 'kuid',
      title: 'KUID'
    }, {
      field: 'published',
      title: 'Publication',
      template: function template(row) {
        var publish = {
          0: {
            'color': 'danger',
            'icon': 'ki ki-close',
            'text': 'Non Publier'
          },
          1: {
            'color': 'success',
            'icon': 'ki ki-check',
            'text': 'Publier'
          }
        };
        return "<span class=\"label label-inline label-".concat(publish[row.published].color, "\"><i class=\"").concat(publish[row.published].icon, "\"></i>&nbsp; ").concat(publish[row.published].text, "</span>");
      }
    }, {
      field: 'Actions',
      title: 'Actions',
      sortable: false,
      template: function template(row) {
        return "\n                        <button data-link=\"/route/".concat(row.route.id, "/session/").concat(row.id, "\" class=\"btn btn-icon btn-xs btn-danger\" data-toggle=\"tooltip\" title=\"Supprimer la session\" onclick=\"deleteSession($(this))\"><i class=\"fa fa-trash\"></i> </button>\n                    ");
      }
    }, {
      field: 'short_content',
      title: 'Courte description',
      autoHide: true
    }, {
      field: 'content',
      title: 'Description',
      autoHide: true,
      template: function template(row) {
        return "".concat(marked(row.content));
      }
    }]
  });
}

function chargeElement() {
  $('#short_content').maxlength({
    threshold: 5,
    warningClass: "label label-danger label-rounded label-inline",
    limitReachedClass: "label label-primary label-rounded label-inline"
  });
  $('[data-switch=true]').bootstrapSwitch();
  $("#published_at").datetimepicker({
    useCurrent: true,
    locale: 'fr',
    icons: {
      time: 'fa fa-clock',
      date: 'fa fa-calendar',
      up: 'fa fa-arrow-up',
      down: 'fa fa-arrow-down',
      previous: 'fa fa-chevron-left',
      next: 'fa fa-chevron-right',
      today: 'fa fa-calendar-check-o',
      clear: 'fa fa-trash',
      close: 'fa fa-times'
    },
    format: 'Y-MM-DD HH:mm'
  });
  new KTImageInput('session_img');
}

function appearPublishedAt() {
  var published = $("#published");
  var published_at = $("#show_published_at");

  if (published.is(':checked')) {
    published_at.show();
  } else {
    published_at.hide();
  }
}

function deleteSession(btn) {
  var uri = btn.attr('data-link');
  btn.html("<div class=\"".concat(spinning, "\"></div>"));
  $.ajax({
    url: uri,
    method: 'DELETE',
    success: function success(data) {
      btn.html('<i class="fa fa-trash"></i>');
      toastr.success(data.data.message);
      btn.parents('tr').fadeOut();
    },
    error: function error(_error) {
      btn.html('<i class="fa fa-trash"></i>');
      console.error(_error);
      toastr.error('Erreur Système, consulter les logs', "Erreur 500");
    }
  });
}

chargeElement();
appearPublishedAt();

/***/ }),

/***/ 17:
/*!**********************************************************!*\
  !*** multi ./resources/js/scripts/route/show_session.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\site\admin.trainznation\resources\js\scripts\route\show_session.js */"./resources/js/scripts/route/show_session.js");


/***/ })

/******/ });