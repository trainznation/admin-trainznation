/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 44);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/scripts/notificator.js":
/*!*********************************************!*\
  !*** ./resources/js/scripts/notificator.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var notifElement = {
  pulsingNotif: KTUtil.getById('pulsingNotif'),
  counter: null,
  counterNotif: KTUtil.getById('counterNotif'),
  notifList: KTUtil.getById('notifList')
};
$.ajax({
  url: '/me',
  success: function success(data) {
    var notifications = data.notifications;
    verifyCountUnreadNotification(notifications);
  }
});

function verifyCountUnreadNotification(notifications) {
  var counter = 0;
  Array.from(notifications).forEach(function (notif) {
    if (notif.read_at === null) {
      counter++;
    }
  });

  if (counter !== 0) {
    notifElement.pulsingNotif.classList.add('pulse-ring');
  } else {
    notifElement.pulsingNotif.classList.remove('pulse-ring');
  }

  if (counter === 0 || counter <= 1) {
    notifElement.counterNotif.innerHTML = "".concat(counter, " Nouvelle Notification");
  } else {
    notifElement.counterNotif.innerHTML = "".concat(counter, " Nouvelles Notifications");
  }

  appearListNotifications(notifications);
}

function appearListNotifications(notifications) {
  notifElement.notifList.innerHTML = null;
  Array.from(notifications).forEach(function (notif) {
    if (notif.read_at === null) {
      var data = notif;
      var date = moment(notif.updated_at);
      var now = moment();
      var diff = now.diff(date);
      var dateTransform;

      if (diff < 60000) {
        dateTransform = "Il y a ".concat(now.diff(date, 'seconds'), " Secondes");
      } else if (diff < 3600000) {
        dateTransform = "Il y a ".concat(now.diff(date, 'minutes'), " Minutes");
      } else if (diff < 86400000) {
        dateTransform = "Il y a ".concat(now.diff(date, 'hours'), " Heures");
      } else if (diff < 2592000000) {
        dateTransform = "Il y a ".concat(now.diff(date, 'days'), " Jours");
      } else {
        dateTransform = "Il y a ".concat(now.diff(date, 'months'), " Mois");
      }

      notifElement.notifList.innerHTML += "\n                <a data-href=\"/notif/".concat(notif.user_id, "/").concat(notif.id, "\" class=\"navi-item readNotif\">\n                    <div class=\"navi-link rounded\">\n                        <div class=\"symbol symbol-50 symbol-circle mr-3\">\n                            <div class=\"symbol-label\"><i class=\"").concat(data.icon, " text-").concat(data.status, " icon-lg\"></i> </div>\n                        </div>\n                        <div class=\"navi-text\">\n                            <div class=\"font-weight-bold font-size-lg\">").concat(data.title, "</div>\n                            <div class=\"text-muted\">").concat(dateTransform, "</div>\n                        </div>\n                    </div>\n                </a>\n            ");
    }
  });
  initReadNotif();
}

function initReadNotif() {
  var readNotif = document.querySelectorAll('.readNotif');
  Array.from(readNotif).forEach(function (notif) {
    notif.addEventListener('click', function (e) {
      var uri = notif.dataset.href;
      KTApp.block(notif, {
        overlayColor: '#000000',
        state: 'warning',
        // a bootstrap color
        size: 'lg' //available custom sizes: sm|lg

      });
      $.ajax({
        url: uri,
        success: function success(data) {
          console.log(data);
          KTApp.unblock(notif);
          notif.style.display = 'none';
          verifyCountUnreadNotification(readNotif);
        },
        error: function error(err) {
          console.log(err);
          toastr.error("Erreur Système, consulter les logs", "Erreur 500");
          KTApp.unblock(notif);
        }
      });
    });
  });
}

function readNotif(notif, event) {
  var uri = notif.attr('data-href');
  KTApp.block(notif, {
    overlayColor: '#000000',
    state: 'warning',
    // a bootstrap color
    size: 'lg' //available custom sizes: sm|lg

  });
  $.ajax({
    url: uri,
    success: function success(data) {
      console.log(data);
      KTApp.unblock(notif);
      notif.fadeOut();
    },
    error: function error(err) {
      console.log(err);
      toastr.error("Erreur Système, consulter les logs", "Erreur 500");
      KTApp.unblock(notif);
    }
  });
}

/***/ }),

/***/ 44:
/*!***************************************************!*\
  !*** multi ./resources/js/scripts/notificator.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\site\admin.trainznation\resources\js\scripts\notificator.js */"./resources/js/scripts/notificator.js");


/***/ })

/******/ });