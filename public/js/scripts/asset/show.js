/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/scripts/asset/show.js":
/*!********************************************!*\
  !*** ./resources/js/scripts/asset/show.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var spinning = 'spinner spinner-right spinner-white pr-15';
var tableKuid = document.querySelector('#tableKuid');
var tableTags = document.querySelector('#tableTags');
document.querySelector('#markedDown').innerHTML = marked(document.querySelector('#markedDown').dataset.content);
$("#btnTrash").on('click', function (e) {
  e.preventDefault();
  var btn = document.querySelector('#btnTrash');
  var uri = $("#btnTrash").attr('href');
  KTUtil.btnWait(btn, 'spinner');
  $.ajax({
    url: uri,
    method: 'DELETE',
    success: function success(data) {
      KTUtil.btnRelease(btn);
      toastr.success(data.data.message);
      setTimeout(function () {
        window.location.href = '/asset';
      }, 1500);
    },
    error: function error(_error) {
      KTUtil.btnRelease(btn);
      console.log(_error);
      toastr.error(_error.message);
    }
  });
});
$("#formaddKuid").on('submit', function (e) {
  e.preventDefault();
  var form = $("#formaddKuid");
  var btn = document.querySelector('#btnFormAddKuid');
  var uri = form.attr('action');
  var data = form.serializeArray();
  KTUtil.btnWait(btn, spinning);
  $.ajax({
    url: uri,
    method: "POST",
    data: data,
    success: function success(data) {
      KTUtil.btnRelease(btn);
      form[0].reset();
      var tr = data;
      $('#tableKuid tbody').prepend(tr).fadeIn();
      $("#addKuid").modal('hide');
      toastr.success("Le kuid à été ajouté");
    },
    error: function error(_error2) {
      KTUtil.btnRelease(btn);
      console.log(_error2);
      toastr.error("Erreur Système");
    }
  });
});
$("#formAddTags").on('submit', function (e) {
  e.preventDefault();
  var form = $("#formAddTags");
  var btn = document.querySelector('#btnFormAddTags');
  var uri = form.attr('action');
  var data = form.serializeArray();
  KTUtil.btnWait(btn, spinning);
  $.ajax({
    url: uri,
    method: 'POST',
    data: data,
    success: function success(data) {
      KTUtil.btnRelease(btn);
      form[0].reset();
      toastr.success("Le ou les tags ont été ajoutés", "Succès");
    },
    error: function error(_error3) {
      KTUtil.btnRelease(btn);
      console.log(_error3);
      toastr.error("Erreur Systeme");
    }
  });
});
$("#formAddSketchfab").on('submit', function (e) {
  e.preventDefault();
  var form = $("#formAddSketchfab");
  var uri = form.attr('action');
  var btn = document.querySelector("#btnFormAddSketchfab");
  var data = form.serializeArray();
  KTUtil.btnWait(btn, spinning, 'Veuillez patienter...', true);
  $.ajax({
    url: uri,
    method: 'POST',
    data: data,
    success: function success(data) {
      KTUtil.btnRelease(btn);
      form[0].reset();
      toastr.success("L'UID Sketchfab pour la vue 3D à été paramétrer");
    },
    error: function error(_error4) {
      KTUtil.btnRelease(btn);
      console.error(_error4);
      toastr.error("Erreur Système, consulter les logs", "Erreur Serveur");
    }
  });
});
$("#formAddPrice").on('submit', function (e) {
  e.preventDefault();
  var form = $("#formAddPrice");
  var uri = form.attr('action');
  var btn = document.querySelector("#btnFormAddPrice");
  var data = form.serializeArray();
  KTUtil.btnWait(btn, spinning, 'Veuillez patienter...', true);
  $.ajax({
    url: uri,
    method: 'POST',
    data: data,
    success: function success(data) {
      KTUtil.btnRelease(btn);
      form[0].reset();
      toastr.success("Le prix de l'asset à été fixé");
    },
    error: function error(_error5) {
      KTUtil.btnRelease(btn);
      console.error(_error5);
      toastr.error("Erreur Système, consulter les logs", "Erreur Serveur");
    }
  });
});

function formatField() {
  moment.locale('fr');
  $("#published_at").datetimepicker({
    locale: 'fr',
    format: "YYYY-MM-DD HH:mm:ss",
    icons: {
      time: "fa fa-clock-o",
      date: "fa fa-calendar",
      up: "fa fa-arrow-up",
      down: "fa fa-arrow-down"
    }
  });
  $("#kuid_field").inputmask('kuid:400722:9999999', {
    rightAlignNumerics: false //numericInput: true

  });
  var tags_field = document.querySelector('#tags_field');
  var tagify = new Tagify(tags_field, {
    originalInputValueFormat: function originalInputValueFormat(valuesArr) {
      return valuesArr.map(function (item) {
        return item.value;
      }).join(',');
    }
  });
}

function deleteKuid() {
  var btns = document.querySelectorAll('.btn-delete-kuid');
  Array.from(btns).forEach(function (btn) {
    btn.addEventListener('click', function (e) {
      e.preventDefault();
      var uri = btn.dataset.href;
      var tr = btn.parentElement.parentElement;
      KTUtil.btnWait(btn, spinning);
      $.ajax({
        url: uri,
        method: 'DELETE',
        success: function success(data) {
          KTUtil.btnRelease(btn);
          tableKuid.deleteRow(tr.rowIndex);
          toastr.success(data);
        },
        error: function error(_error6) {
          KTUtil.btnRelease(btn);
          console.log(_error6);
          toastr.error(_error6);
        }
      });
    });
  });
}

function deleteTag() {
  var btns = document.querySelectorAll('.btn-delete-tag');
  Array.from(btns).forEach(function (btn) {
    btn.addEventListener('click', function (e) {
      e.preventDefault();
      var uri = btn.dataset.href;
      var tr = btn.parentElement.parentElement;
      KTUtil.btnWait(btn, spinning);
      $.ajax({
        url: uri,
        method: 'DELETE',
        success: function success(data) {
          KTUtil.btnRelease(btn);
          tableTags.deleteRow(tr.rowIndex);
          toastr.success(data.data.message);
        },
        error: function error(_error7) {
          KTUtil.btnRelease(btn);
          console.log(_error7);
          toastr.error(_error7.data.error);
        }
      });
    });
  });
}

formatField();
deleteKuid();
deleteTag();

/***/ }),

/***/ 4:
/*!**************************************************!*\
  !*** multi ./resources/js/scripts/asset/show.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\site\admin.trainznation\resources\js\scripts\asset\show.js */"./resources/js/scripts/asset/show.js");


/***/ })

/******/ });