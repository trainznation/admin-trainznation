/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/scripts/asset/index.js":
/*!*********************************************!*\
  !*** ./resources/js/scripts/asset/index.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var table;

function loadListObject() {
  table = $("#listeObjets").KTDatatable({
    data: {
      saveState: {
        cookie: false
      }
    },
    search: {
      input: $('#search_liste_objet'),
      key: 'generalSearch'
    },
    columns: [{
      field: "#",
      width: 40
    }, {
      field: 'Image',
      width: 120
    }, {
      field: 'Designation',
      width: 250
    }, {
      field: 'Publier',
      title: 'Publier',
      autoHide: false,
      template: function template(row) {
        var publier = {
          0: {
            'title': 'Non Publier',
            'class': 'label-danger'
          },
          1: {
            'title': 'Publier',
            'class': 'label-success'
          }
        };
        return "<span class=\"label font-weight-bold label-inline label-lg ".concat(publier[row.Publier]["class"], "\">").concat(publier[row.Publier].title, "</span>");
      }
    }]
  });
  $("#kt_datatable_search_publier").on('change', function () {
    table.search($('#kt_datatable_search_publier').val().toLowerCase(), 'Publier');
  });
  $("#kt_datatable_search_publier").selectpicker();
  table.on('click', '.btn-danger', function (e) {
    e.preventDefault();
    var btn = $(".btn-danger");
    KTUtil.btnWait(document.querySelector('.btn-danger'), 'spinner');
    fetch("/asset/".concat(btn.attr('data-id')), {
      method: 'delete'
    }).then(function (response) {
      return response.text();
    }).then(function (result) {
      KTUtil.btnRelease(document.querySelector('.btn-danger'));
      var data = JSON.parse(result);
      toastr.success(data.data.message);
    })["catch"](function (error) {
      KTUtil.btnRelease(document.querySelector('.btn-danger'));
      var data = JSON.parse(error);
      toastr.error(data.data.message);
    });
  });
}

loadListObject();

/***/ }),

/***/ 1:
/*!***************************************************!*\
  !*** multi ./resources/js/scripts/asset/index.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\site\admin.trainznation\resources\js\scripts\asset\index.js */"./resources/js/scripts/asset/index.js");


/***/ })

/******/ });