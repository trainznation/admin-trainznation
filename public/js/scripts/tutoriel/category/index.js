/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 21);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/scripts/tutoriel/category/index.js":
/*!*********************************************************!*\
  !*** ./resources/js/scripts/tutoriel/category/index.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var listeCategory;
var spinning = 'spinner spinner-right spinner-white pr-15';
$.ajax({
  url: '/tutoriel/category/list',
  success: function success(data) {
    loadListeCategory(data.data);
  }
});

function loadListeCategory(data) {
  listeCategory = $("#listeCategories").KTDatatable({
    data: {
      type: 'local',
      source: data,
      pageSize: 10
    },
    // layout definition
    layout: {
      scroll: false,
      // enable/disable datatable scroll both horizontal and vertical when needed.
      // height: 450, // datatable's body's fixed height
      footer: false // display/hide footer

    },
    // column sorting
    sortable: true,
    pagination: true,
    search: {
      input: $('#search_query_category'),
      key: 'generalSearch'
    },
    columns: [{
      field: 'name',
      title: "Categorie",
      template: function template(row) {
        return "\n                        <div class=\"d-flex align-items-center\">\n                            <div class=\"symbol symbol-30 mr-5\">\n                                <img src=\"".concat(row.image, "\" alt=\"").concat(row.name, "\">\n                            </div>\n                            <div class=\"d-flex flex-column font-weight-bold\">\n                                <span class=\"text-dark font-size-lg\">").concat(row.name, "</span>\n                            </div>\n                        </div>\n                    ");
      }
    }, {
      field: 'Actions',
      title: 'Actions',
      template: function template(row) {
        return "\n                        <button class=\"btn btn-icon btn-sm btn-danger\" data-href=\"/tutoriel/category/".concat(row.id, "\" data-toggle=\"tooltip\" title=\"Supprimer la cat\xE9gorie\" onclick=\"deleteCategory($(this))\"><i class=\"fa fa-trash\"></i> </button>\n                    ");
      }
    }]
  });
}

function deleteCategory(btn) {
  var uri = btn.attr('data-href');
  btn.html("<div class=\"".concat(spinning, "\"></div>"));
  $.ajax({
    url: uri,
    method: 'DELETE',
    success: function success(data) {
      btn.html('<i class="fa fa-trash"></i>');
      toastr.success(data.data.message);
      btn.parents('tr').fadeOut();
    },
    error: function error(_error) {
      btn.html('<i class="fa fa-trash"></i>');
      console.error(_error);
      toastr.error('Erreur Système, consulter les logs', "Erreur 500");
    }
  });
}

/***/ }),

/***/ 21:
/*!***************************************************************!*\
  !*** multi ./resources/js/scripts/tutoriel/category/index.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\site\admin.trainznation\resources\js\scripts\tutoriel\category\index.js */"./resources/js/scripts/tutoriel/category/index.js");


/***/ })

/******/ });