/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 22);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/scripts/tutoriel/index.js":
/*!************************************************!*\
  !*** ./resources/js/scripts/tutoriel/index.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var listeTutoriel;
var spinning = 'spinner spinner-right spinner-white pr-15';
$.ajax({
  url: '/tutoriel/list',
  success: function success(data) {
    loadListeTutoriels(data.data);
  }
});

function loadListeTutoriels(data) {
  listeTutoriel = $("#listeTutoriel").KTDatatable({
    data: {
      type: 'local',
      source: data,
      pageSize: 10
    },
    // layout definition
    layout: {
      scroll: false,
      // enable/disable datatable scroll both horizontal and vertical when needed.
      // height: 450, // datatable's body's fixed height
      footer: false // display/hide footer

    },
    // column sorting
    sortable: true,
    pagination: true,
    search: {
      input: $('#search_liste_tutoriels'),
      key: 'generalSearch'
    },
    columns: [{
      field: 'id',
      title: 'ID',
      width: 30
    }, {
      field: 'title',
      title: 'Titre',
      template: function template(row) {
        return "\n                    <div class=\"d-flex flex-column\">\n                        <span class=\"font-size-lg font-weight-bold\">".concat(row.title, "</span>\n                        <span class=\"text-muted\">").concat(row.category.name, "</span>\n                    </div>\n                ");
      }
    }, {
      field: 'image',
      title: 'Thumbnail',
      width: 160,
      template: function template(row) {
        return "<img src=\"".concat(row.image, "\" class=\"img-fluid\" style=\"width: 160px\" alt>");
      }
    }, {
      field: 'stat',
      title: "Etat",
      template: function template(row) {
        var status = {
          0: {
            'class': 'text-dark'
          },
          1: {
            'class': 'text-primary'
          }
        };
        return "\n                        <i class=\"fab fa-facebook-square ".concat(status[row.social]["class"], "\"></i>\n                        <i class=\"far fa-file-archive ").concat(status[row.source]["class"], "\"></i>\n                        <i class=\"fas fa-certificate ").concat(status[row.premium]["class"], "\"></i>\n                    ");
      }
    }, {
      field: 'published',
      title: "Publier",
      textAlign: 'center',
      width: 50,
      template: function template(row) {
        var published = {
          0: {
            'class': 'danger',
            'icon': 'fas fa-times-circle'
          },
          1: {
            'class': 'success',
            'icon': 'far fa-check-circle'
          }
        };
        return "<i class=\"".concat(published[row.published].icon, " text-").concat(published[row.published]["class"], " fa-2x\"></i>");
      }
    }, {
      field: 'Actions',
      title: 'Actions',
      textAlign: "center",
      template: function template(row) {
        return "\n                        <div class=\"btn-group\">\n                            <a href=\"/tutoriel/".concat(row.id, "\" class=\"btn btn-sm btn-icon btn-primary btn-show\"><i class=\"fa fa-eye\"></i> </a>\n                            <a href=\"/tutoriel/").concat(row.id, "/edit\" class=\"btn btn-sm btn-icon btn-secondary btn-edit\"><i class=\"fa fa-edit\"></i> </a>\n                            <a href=\"/tutoriel/").concat(row.id, "/delete\" class=\"btn btn-sm btn-icon btn-danger btn-delete\"><i class=\"fa fa-trash\"></i> </a>\n                        </div>\n                    ");
      }
    }]
  });
}

/***/ }),

/***/ 22:
/*!******************************************************!*\
  !*** multi ./resources/js/scripts/tutoriel/index.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\site\admin.trainznation\resources\js\scripts\tutoriel\index.js */"./resources/js/scripts/tutoriel/index.js");


/***/ })

/******/ });