/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 35);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/scripts/support/index.js":
/*!***********************************************!*\
  !*** ./resources/js/scripts/support/index.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var spinning = 'spinner spinner-right spinner-white pr-15';
var dataTable;
dataTable = $("#listeTickets").KTDatatable({
  data: {
    type: 'remote',
    source: {
      read: {
        url: $("#listeTickets").attr('data-href')
      }
    }
  },
  search: {
    input: $("#liste_search_tickets"),
    key: 'generalSearch'
  },
  columns: [{
    field: 'id',
    title: "#",
    width: 25
  }, {
    field: 'subject',
    title: 'Sujet'
  }, {
    field: 'priority.id',
    title: 'Priorite',
    template: function template(row) {
      return "<span class=\"label label-inline label-".concat(row.priority.color, "\">").concat(row.priority.designation, "</span>");
    }
  }, {
    field: 'status.id',
    title: 'Status',
    template: function template(row) {
      return "<span class=\"label label-inline label-".concat(row.status.color, "\"><i class=\"").concat(row.status.icon, " text-white\"></i> ").concat(row.status.designation, "</span>");
    }
  }, {
    field: 'source.id',
    title: 'Source',
    template: function template(row) {
      return "<span class=\"label label-inline label-default\">".concat(row.source.designation, "</span>");
    },
    width: 100
  }, {
    field: 'Actions',
    title: 'Actions',
    template: function template(row) {
      return "\n                <a href=\"/support/".concat(row.id, "\" class=\"btn btn-primary btn-icon\" data-toggle=\"tooltip\" title=\"Voir le ticket\"><i class=\"fas fa-eye\"></i> </a>\n                <a href=\"/support/").concat(row.id, "/edit\" class=\"btn btn-info btn-icon\" data-toggle=\"tooltip\" title=\"Editer le ticket\"><i class=\"fas fa-edit\"></i> </a>\n                <a href=\"/support/").concat(row.id, "/delete\" class=\"btn btn-danger btn-icon\" data-toggle=\"tooltip\" title=\"Supprimer le ticket\"><i class=\"fas fa-trash\"></i> </a>\n                ");
    }
  }, {
    field: 'category',
    title: 'Catégorie',
    template: function template(row) {
      return "".concat(row.category.name, " - ").concat(row.sector.name);
    },
    autoHide: true
  }, {
    field: 'requester',
    title: 'Utilisateur',
    template: function template(row) {
      return "<span class=\"label label-inline label-default\">".concat(row.requester.name, "</span>");
    },
    autoHide: true
  }, {
    field: 'created_at',
    title: 'Horodatage',
    template: function template(row) {
      return "<strong>Cr\xE9er le:</strong> ".concat(row.created_at.normalize, "<br><strong>Mise \xE0 jour le:</strong> ").concat(row.updated_at.normalize);
    },
    autoHide: true
  }]
});
$('#kt_datatable_search_status').on('change', function () {
  dataTable.search($(this).val().toLowerCase(), 'status.id');
});
$('#kt_datatable_search_priority').on('change', function () {
  dataTable.search($(this).val().toLowerCase(), 'priority.id');
});
$('#kt_datatable_search_source').on('change', function () {
  dataTable.search($(this).val().toLowerCase(), 'source.id');
});
$('#kt_datatable_search_status, #kt_datatable_search_priority, #kt_datatable_search_source').selectpicker();
$("#support_category_id").on('change', function (e) {
  $.ajax({
    url: "/support/sector/".concat($("#support_category_id").val(), "/search"),
    success: function success(data) {
      var content = "";
      data.forEach(function (item) {
        console.log(item);
        content += "<option value=\"".concat(item.id, "\">").concat(item.name, "</option>");
      });
      $("#support_sector_id").html(content);
    }
  });
});
$("#formAddTicket").on('submit', function (e) {
  e.preventDefault();
  var form = $("#formAddTicket");
  var url = form.attr('action');
  var btn = document.querySelector('.btn-submit');
  var data = form.serializeArray();
  KTUtil.btnWait(btn, spinning);
  $.ajax({
    url: url,
    method: 'POST',
    data: data,
    success: function success(data) {
      KTUtil.btnRelease(btn);
      console.log(data);
      /*toastr.success("Le ticket à été créer")
      $("#addTicket").modal('hide')
          .on('hidden.bs.modal', (e) => {
              window.location.reload()
          })*/
    },
    error: function error(err) {
      KTUtil.btnRelease(btn);
      console.error(err);
      toastr.error("Erreur Système, consulter les logs");
    }
  });
});

/***/ }),

/***/ 35:
/*!*****************************************************!*\
  !*** multi ./resources/js/scripts/support/index.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\site\admin.trainznation\resources\js\scripts\support\index.js */"./resources/js/scripts/support/index.js");


/***/ })

/******/ });