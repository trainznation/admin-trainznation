/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 8);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/scripts/blog/edit.js":
/*!*******************************************!*\
  !*** ./resources/js/scripts/blog/edit.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var wizard;

function WizardObj() {
  var wizardEl = document.querySelector('#kt_wizard');
  var formEl = document.querySelector('#kt_form');
  var validations = [];
  wizard = new KTWizard(wizardEl, {
    startStep: 1,
    clickableSteps: false
  });
  wizard.on('change', function (wizard) {
    if (wizard.getStep() > wizard.getNewStep()) {
      return;
    }

    var validator = validations[wizard.getStep() - 1];

    if (validator) {
      validator.validate().then(function (status) {
        if (status === 'Valid') {
          wizard.goTo(wizard.getNewStep());
          KTUtil.scrollTop();
        } else {
          Swal.fire({
            text: "Désolé, il semble que des erreurs ont été détectées, veuillez réessayer.",
            icon: "error",
            buttonsStyling: false,
            confirmButtonText: "Ok, compris!",
            customClass: {
              confirmButton: "btn font-weight-bold btn-light"
            }
          }).then(function () {
            KTUtil.scrollTop();
          });
        }
      });
    }

    return false;
  });
  wizard.on('changed', function () {
    KTUtil.scrollTop();
  });
  wizard.on('submit', function () {
    Swal.fire({
      text: "Tout est bon! Veuillez confirmer la soumission du formulaire.",
      icon: "success",
      showCancelButton: true,
      buttonsStyling: false,
      confirmButtonText: "Oui, soumettre",
      cancelButtonText: "Non, annuler",
      customClass: {
        confirmButton: "btn font-weight-bold btn-primary",
        cancelButton: "btn font-weight-bold btn-default"
      }
    }).then(function (result) {
      if (result.value) {
        formEl.submit();
      } else if (result.dismiss === 'cancel') {
        Swal.fire({
          text: "Le formulaire n'a pas été soumis",
          icon: "error",
          buttonsStyling: false,
          confirmButtonText: "Ok, compris !",
          customClass: {
            confirmButton: "btn font-weight-bold btn-primary"
          }
        });
      }
    });
  });

  var initValidation = function initValidation() {
    validations.push(FormValidation.formValidation(formEl, {
      fields: {
        blog_category_id: {
          validators: {
            notEmpty: {
              message: "La catégorie est requise"
            }
          }
        },
        title: {
          validators: {
            notEmpty: {
              message: "Le titre est requis"
            }
          }
        }
      },
      plugins: {
        trigger: new FormValidation.plugins.Trigger(),
        // Bootstrap Framework Integration
        bootstrap: new FormValidation.plugins.Bootstrap({
          //eleInvalidClass: '',
          eleValidClass: ''
        })
      }
    }));
    validations.push(FormValidation.formValidation(formEl, {
      fields: {
        short_content: {
          validators: {
            notEmpty: {
              message: "La courte description est requise"
            },
            stringLength: {
              max: 255,
              message: 'La courte description ne doit pas être supérieur à 255 caractères'
            }
          }
        },
        content: {
          validators: {
            notEmpty: {
              message: "le contenue est requis"
            }
          }
        }
      },
      plugins: {
        trigger: new FormValidation.plugins.Trigger(),
        // Bootstrap Framework Integration
        bootstrap: new FormValidation.plugins.Bootstrap({
          //eleInvalidClass: '',
          eleValidClass: ''
        })
      }
    }));
    validations.push(FormValidation.formValidation(formEl, {
      fields: {},
      plugins: {
        trigger: new FormValidation.plugins.Trigger(),
        // Bootstrap Framework Integration
        bootstrap: new FormValidation.plugins.Bootstrap({
          //eleInvalidClass: '',
          eleValidClass: ''
        })
      }
    }));
  };

  initValidation();
}

function formatSearch(data) {
  if (data.loading) return data.text;
  var markup = "\n        <div class=\"row\">\n            <div class=\"col-md-12\">".concat(data.name, "</div>\n        </div>\n    ");
  return markup;
}

function formatSelect(data) {
  return data.name || data.text;
}

function formatedField() {
  moment.locale('fr');
  $("#published_at").datetimepicker({
    locale: 'fr',
    format: "YYYY-MM-DD HH:mm:ss",
    icons: {
      time: "fa fa-clock-o",
      date: "fa fa-calendar",
      up: "fa fa-arrow-up",
      down: "fa fa-arrow-down"
    }
  });
  $('[data-switch=true]').bootstrapSwitch();
  $('#short_content').maxlength({
    alwaysShow: true
  });
  $("#copyContent").on('click', function (e) {
    e.preventDefault();

    var _short = $("#short_content").val();

    var desc = $("#content");
    desc.html("_".concat(_short, "_"));
  });
  new KTImageInput('blog_img');
}

function appearPublishedAtField() {
  var published = $("#published");
  var published_at = $("#div_published_at");

  if (published.is(':checked')) {
    published_at.show();
  } else {
    published_at.hide();
  }
}

$("#blog_category_id").select2({
  placeholder: 'Rechercher...',
  allowClear: true,
  ajax: {
    url: '/blog/category/list',
    dataType: 'json',
    delay: 250,
    data: function data(params) {
      return {
        q: params.term
      };
    },
    processResults: function processResults(data, params) {
      return {
        results: data.items
      };
    },
    cache: true
  },
  escapeMarkup: function escapeMarkup(markup) {
    return markup;
  },
  minimumInputLength: 1,
  templateResult: formatSearch,
  templateSelection: formatSelect,
  language: {
    inputTooShort: function inputTooShort() {
      return "Veuillez entrer au moins 1 caractère...";
    },
    searching: function searching() {
      return "Recherche en cours...";
    },
    errorLoading: function errorLoading() {
      return "Les résultats ne peuvent pas être chargés.";
    }
  }
});
WizardObj();
formatedField();
appearPublishedAtField();

/***/ }),

/***/ 8:
/*!*************************************************!*\
  !*** multi ./resources/js/scripts/blog/edit.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\site\admin.trainznation\resources\js\scripts\blog\edit.js */"./resources/js/scripts/blog/edit.js");


/***/ })

/******/ });