/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 46);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/scripts/spotlight.js":
/*!*******************************************!*\
  !*** ./resources/js/scripts/spotlight.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _wrapNativeSuper(Class) { var _cache = typeof Map === "function" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== "function") { throw new TypeError("Super expression must either be null or a function"); } if (typeof _cache !== "undefined") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }

function _construct(Parent, args, Class) { if (_isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _isNativeFunction(fn) { return Function.toString.call(fn).indexOf("[native code]") !== -1; }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var Spotlight = /*#__PURE__*/function (_HTMLElement) {
  _inherits(Spotlight, _HTMLElement);

  var _super = _createSuper(Spotlight);

  /**
   * @property {HTMLInputElement} input
   * @property {spotlightItem[]} items
   */
  function Spotlight() {
    var _this;

    _classCallCheck(this, Spotlight);

    _this = _super.call(this);
    _this.shortcutHandler = _this.shortcutHandler.bind(_assertThisInitialized(_this));
    _this.hide = _this.hide.bind(_assertThisInitialized(_this));
    _this.onInput = _this.onInput.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(Spotlight, [{
    key: "connectedCallback",
    value: function connectedCallback() {
      this.classList.add('spotlight');
      this.innerHTML = "\n            <div class=\"spotlight-bar\">\n                <input type=\"text\">\n                <ul class=\"spotlight-suggestions\">\n                </ul>\n            </div>\n        ";
      this.input = this.querySelector('input');
      this.input.addEventListener('blur', this.hide);
      var suggestions = this.querySelector('.spotlight-suggestions');
      this.items = Array.from(document.querySelectorAll(this.getAttribute('target'))).map(function (a) {
        var title = a.innerText.trim();
        if (title === '') return;
        var item = new SpotlightItem(title, a.getAttribute('href'));
        suggestions.appendChild(item.element);
        return item;
      }).filter(function (i) {
        return i !== null;
      });
      window.addEventListener('keydown', this.shortcutHandler);
      this.input.addEventListener('input', this.onInput);
    }
  }, {
    key: "disconnectedCallback",
    value: function disconnectedCallback() {
      window.removeEventListener('keydown', this.shortcutHandler);
    }
  }, {
    key: "shortcutHandler",
    value: function shortcutHandler(e) {
      if (e.key === 'k' && e.ctrlKey === true) {
        e.preventDefault();
        this.classList.add('active');
        this.input.focus();
      }

      if (e.key === 'Escape' && document.activeElement === this.input) {
        this.input.blur();
      }
    }
  }, {
    key: "hide",
    value: function hide() {
      this.classList.remove('active');
    }
    /**
     *
     * @param {InputEvent} e
     */

  }, {
    key: "onInput",
    value: function onInput(e) {
      var search = e.target.value;
      var regexp = '^(.*)';

      for (var i in search) {
        regexp += "(".concat(search[i], ")(.*)");
      }

      regexp += '$';
      regexp = new RegExp(regexp, 'i');
      this.items.forEach(function (item) {
        return item.match(regexp);
      });
    }
  }]);

  return Spotlight;
}( /*#__PURE__*/_wrapNativeSuper(HTMLElement));
/**
 * @property {HTMLLIElement} element
 * @property {string} title
 */


var SpotlightItem = /*#__PURE__*/function () {
  /**
   *
   * @param {string} title
   * @param {string} href
   */
  function SpotlightItem(title, href) {
    _classCallCheck(this, SpotlightItem);

    var li = document.createElement('li');
    var a = document.createElement('a');
    a.setAttribute('href', href);
    a.innerText = title;
    li.appendChild(a);
    li.setAttribute('hidden', 'hidden');
    this.element = li;
    this.title = title;
  }
  /**
   *
   * @param {RegExp} regexp
   * @return {boolean}
   */


  _createClass(SpotlightItem, [{
    key: "match",
    value: function match(regexp) {
      var matches = this.title.match(regexp);

      if (matches === null) {
        this.element.setAttribute('hidden', 'hidden');
        return false;
      }

      this.element.firstElementChild.innerHTML = matches.reduce(function (acc, match, index) {
        if (index === 0) {
          return acc;
        }

        return acc + (index % 2 === 0 ? "<mark>".concat(match, "</mark>") : match);
      }, '');
      this.element.removeAttribute('hidden');
      return true;
    }
  }]);

  return SpotlightItem;
}();

customElements.define('spotlight-bar', Spotlight);

/***/ }),

/***/ 46:
/*!*************************************************!*\
  !*** multi ./resources/js/scripts/spotlight.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\site\admin.trainznation\resources\js\scripts\spotlight.js */"./resources/js/scripts/spotlight.js");


/***/ })

/******/ });