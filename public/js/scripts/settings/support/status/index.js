/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 34);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/scripts/settings/support/status/index.js":
/*!***************************************************************!*\
  !*** ./resources/js/scripts/settings/support/status/index.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var spinning = 'spinner spinner-right spinner-white pr-15';
var dataTable;
dataTable = $("#listeStatus").KTDatatable({
  data: {
    saveState: {
      cookie: false
    }
  },
  search: {
    input: $("#search_liste_status"),
    key: 'generalSearch'
  },
  columns: [{
    field: 'Id',
    width: 25
  }, {
    field: 'designation',
    title: 'Designation'
  }, {
    field: 'couleur',
    title: 'Couleur'
  }, {
    field: 'icon',
    title: 'Icone'
  }, {
    field: 'Actions',
    title: 'Actions'
  }]
});
var btn = document.querySelector('#refreshTable').addEventListener('click', function (e) {
  e.preventDefault();
  KTUtil.btnWait(btn, spinning);
  dataTable.reload();
  $("#listeStatus").on('datatable-on-reloaded', function (e) {
    KTUtil.btnRelease(btn);
  });
});
$("#formAddStatus").on('submit', function (e) {
  e.preventDefault();
  var form = $("#formAddStatus");
  var btn = document.querySelector('.btn-submit');
  var url = form.attr('action');
  var data = form.serializeArray();
  KTUtil.btnWait(btn, spinning);
  $.ajax({
    url: url,
    method: 'POST',
    data: data,
    success: function success(data) {
      KTUtil.btnRelease(btn);
      toastr.success("Le status à été ajouté");
      $("#addStatus").on('hidden.bs.modal', function (e) {
        window.location.reload();
      });
    },
    error: function (_error) {
      function error(_x) {
        return _error.apply(this, arguments);
      }

      error.toString = function () {
        return _error.toString();
      };

      return error;
    }(function (err) {
      console.error(error);
      KTUtil.btnRelease(btn);
      toastr.error('Erreur de script, consulter les logs', 'Erreur Serveur');
    })
  });
});
$("#formEditStatus").on('submit', function (e) {
  e.preventDefault();
  var form = $("#formEditStatus");
  var url = form.attr('action');
  var btn = document.querySelector('.btn-submit');
  var data = form.serializeArray();
  KTUtil.btnWait(btn, spinning);
  $.ajax({
    url: url,
    method: 'PUT',
    data: data,
    success: function success(data) {
      KTUtil.btnRelease(btn);
      toastr.success("Le status à été modifié");
      $("#editSource").modal('hide');
    },
    error: function error(err) {
      KTUtil.btnRelease(btn);
      toastr.error('Erreur Systeme, consulter les logs');
      toastr.error(err);
    }
  });
});
dataTable.on('click', '.btn-edit', function (e) {
  e.preventDefault();
  $.ajax({
    url: e.currentTarget.href,
    success: function success(data) {
      $("#formEditStatus").attr('action', e.currentTarget.href);
      $("#name_status").val(data.designation);
      $("#icon_status").val(data.icon);
      $("#editStatus").modal('show');
    },
    error: function error(err) {
      console.log(err);
      toastr.error("Erreur de script, consulter les logs", "Erreur Système");
    }
  });
});
dataTable.on('click', '.btn-trash', function (e) {
  e.preventDefault();
  $.ajax({
    url: e.currentTarget.href,
    method: 'DELETE',
    success: function success() {
      e.currentTarget.parentNode.parentNode.parentNode.style.display = 'none';
      toastr.success("Le status à été supprimer");
    },
    error: function error(err) {
      console.log(err);
      toastr.error("Erreur de script, consulter les logs", "Erreur Système");
    }
  });
});

/***/ }),

/***/ 34:
/*!*********************************************************************!*\
  !*** multi ./resources/js/scripts/settings/support/status/index.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\site\admin.trainznation\resources\js\scripts\settings\support\status\index.js */"./resources/js/scripts/settings/support/status/index.js");


/***/ })

/******/ });