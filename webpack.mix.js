const mix = require('laravel-mix');

mix.js('resources/js/scripts/asset/category/index.js', 'public/js/scripts/asset/category');
mix.js('resources/js/scripts/asset/index.js', 'public/js/scripts/asset');
mix.js('resources/js/scripts/asset/create.js', 'public/js/scripts/asset');
mix.js('resources/js/scripts/asset/edit.js', 'public/js/scripts/asset');
mix.js('resources/js/scripts/asset/show.js', 'public/js/scripts/asset');

mix.js('resources/js/scripts/blog/category/index.js', 'public/js/scripts/blog/category');
mix.js('resources/js/scripts/blog/index.js', 'public/js/scripts/blog');
mix.js('resources/js/scripts/blog/create.js', 'public/js/scripts/blog');
mix.js('resources/js/scripts/blog/edit.js', 'public/js/scripts/blog');
mix.js('resources/js/scripts/blog/show.js', 'public/js/scripts/blog');

mix.js('resources/js/scripts/route/create.js', 'public/js/scripts/route');
mix.js('resources/js/scripts/route/edit.js', 'public/js/scripts/route');
mix.js('resources/js/scripts/route/index.js', 'public/js/scripts/route');
mix.js('resources/js/scripts/route/show_dashboard.js', 'public/js/scripts/route');
mix.js('resources/js/scripts/route/show_download.js', 'public/js/scripts/route');
mix.js('resources/js/scripts/route/show_gallery.js', 'public/js/scripts/route');
mix.js('resources/js/scripts/route/show_roadmap.js', 'public/js/scripts/route');
mix.js('resources/js/scripts/route/show_session.js', 'public/js/scripts/route');
mix.js('resources/js/scripts/route/show_version.js', 'public/js/scripts/route');
mix.js('resources/js/scripts/route/update_card.js', 'public/js/scripts/route');
mix.js('resources/js/scripts/route/version.js', 'public/js/scripts/route');

mix.js('resources/js/scripts/tutoriel/category/index.js', 'public/js/scripts/tutoriel/category');
mix.js('resources/js/scripts/tutoriel/index.js', 'public/js/scripts/tutoriel');
mix.js('resources/js/scripts/tutoriel/create.js', 'public/js/scripts/tutoriel');
mix.js('resources/js/scripts/tutoriel/show.js', 'public/js/scripts/tutoriel');

mix.js('resources/js/scripts/settings/annonce/index.js', 'public/js/scripts/settings/annonce');
mix.js('resources/js/scripts/settings/slideshow/index.js', 'public/js/scripts/settings/slideshow');
mix.js('resources/js/scripts/settings/user/index.js', 'public/js/scripts/settings/user');
mix.js('resources/js/scripts/settings/user/show.js', 'public/js/scripts/settings/user');
mix.js('resources/js/scripts/settings/support/category/index.js', 'public/js/scripts/settings/support/category');
mix.js('resources/js/scripts/settings/support/sector/index.js', 'public/js/scripts/settings/support/sector');
mix.js('resources/js/scripts/settings/support/priority/index.js', 'public/js/scripts/settings/support/priority');
mix.js('resources/js/scripts/settings/support/source/index.js', 'public/js/scripts/settings/support/source');
mix.js('resources/js/scripts/settings/support/status/index.js', 'public/js/scripts/settings/support/status');

mix.js('resources/js/scripts/support/index.js', 'public/js/scripts/support');
mix.js('resources/js/scripts/support/show.js', 'public/js/scripts/support');

mix.js('resources/js/scripts/wiki/category/index.js', 'public/js/scripts/wiki/category');
mix.js('resources/js/scripts/wiki/sector/index.js', 'public/js/scripts/wiki/sector');
mix.js('resources/js/scripts/wiki/index.js', 'public/js/scripts/wiki');
mix.js('resources/js/scripts/wiki/create.js', 'public/js/scripts/wiki');
mix.js('resources/js/scripts/wiki/show.js', 'public/js/scripts/wiki');
mix.js('resources/js/scripts/wiki/edit.js', 'public/js/scripts/wiki');

mix.js('resources/js/scripts/login.js', 'public/js/scripts');
mix.js('resources/js/scripts/notificator.js', 'public/js/scripts');
mix.js('resources/js/scripts/youtube.js', 'public/js/scripts');
mix.js('resources/js/scripts/spotlight.js', 'public/js/scripts');

mix.sass('resources/sass/custom.scss', 'public/css')
mix.sass('resources/sass/spotlight.scss', 'public/css')
