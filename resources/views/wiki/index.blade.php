{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="card card-custom">
        <div class="card-header">
            <div class="card-title">
                <span class="card-icon"><i class="flaticon-list-2 text-primary"></i> </span>
                <h3 class="card-label">{{ $page_description }}</h3>
            </div>
            <div class="card-toolbar">
                <a href="{{ route('Wiki.create') }}" class="btn btn-primary btn-sm font-weight-bold mr-2"><i class="fas fa-plus-circle"></i> Nouvelle article</a>
                <button id="refreshTable" class="btn btn-default btn-sm btn-icon" data-toggle="tooltip" title="Rafraichir le tableau"><i class="fas fa-sync text-dark"></i></button>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" id="searchInList" name="q" class="form-control form-control-lg" placeholder="Rechercher dans le tableau...">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Catégorie</label>
                        <select id="selectCategory" class="form-control selectpicker" data-live-search="true" data-provide="selectpicker">
                            <option value=""></option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Secteur</label>
                        <select id="selectSector" class="form-control selectpicker" data-live-search="true" data-provide="selectpicker" data-placeholder="Secteur">
                            <option value=""></option>
                            @foreach($sectors as $sector)
                                <option value="{{ $sector->id }}">{{ $sector->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Publié</label>
                        <select id="selectPublished" class="form-control selectpicker" data-live-search="true" data-provide="selectpicker">
                            <option value=""></option>
                            <option value="0">Non Publier</option>
                            <option value="1">Publier</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="card card-custom">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead class="bg-dark text-white">
                            <tr class="text-center">
                                <th>#</th>
                                <th>Catégorie</th>
                                <th>Secteur</th>
                                <th>Nom de l'article</th>
                                <th>Publié</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody id="loadListArticle" data-target="/wiki/search">
                                @foreach($articles as $article)
                                <tr>
                                    <td class="w-5 text-center">{{ $article->id }}</td>
                                    <td>{{ $article->category->name }}</td>
                                    <td>{{ $article->sector->name }}</td>
                                    <td>{{ $article->title }}</td>
                                    <td class="text-center">
                                        @if($article->published == 1)
                                            <i class="fas fa-check-circle text-success fa-lg" data-toggle="tooltip" title="{{ $article->published_at->human }}"></i>
                                        @else
                                            <i class="fas fa-times-circle text-danger fa-lg"></i>
                                        @endif
                                    </td>
                                    <td class="w-20 text-center">
                                        <a href="/wiki/{{ $article->id }}" class="btn btn-sm btn-icon btn-default"><i class="fas fa-eye"></i> </a>
                                        <a href="/wiki/{{ $article->id }}/edit" class="btn btn-sm btn-icon btn-info"><i class="fas fa-edit"></i> </a>
                                        <a href="/wiki/{{ $article->id }}/delete" class="btn btn-sm btn-icon btn-danger"><i class="fas fa-trash"></i> </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/wiki/index.js') }}" type="text/javascript"></script>
@endsection
