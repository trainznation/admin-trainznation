{{-- Extends layout --}}
@extends('layout.default')

@section("page_toolbars")
    @if($article->published == 0)
        <a href="/wiki/{{ $article->id }}/publish" class="btn btn-success btn-sm mr-2"><i class="fas fa-check"></i> Publier</a>
    @else
        <a href="/wiki/{{ $article->id }}/unpublish" class="btn btn-danger btn-sm mr-2"><i class="fas fa-times"></i> Dépublier</a>
    @endif
    <a href="/wiki/{{ $article->id }}/edit" class="btn btn-primary btn-icon btn-sm mr-2" data-toggle="tooltip" title="Editer l'article"><i class="fas fa-edit"></i> </a>
    <a href="/wiki/{{ $article->id }}/delete" class="btn btn-danger btn-icon btn-sm mr-2" data-toggle="tooltip" title="Supprimer l'article"><i class="fas fa-trash"></i> </a>
@endsection

{{-- Content --}}
@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card card-custom">
                <div class="card-body">
                    <table class="table table-stripped">
                        <tbody>
                            <tr>
                                <td class="font-weight-bold">Titre</td>
                                <td class="font-italic text-right">{{ $article->title }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Catégorie</td>
                                <td class="font-italic text-right">{{ $article->category->name }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Secteur</td>
                                <td class="font-italic text-right">{{ $article->sector->name }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card card-custom">
                <div class="card-header">
                    <h3 class="card-title">Contenue de l'article</h3>
                </div>
                <div class="card-body">
                    <div id="markedDown" data-content="{{ $article->content }}"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/wiki/show.js') }}" type="text/javascript"></script>
@endsection
