{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="card card-custom">
        <div class="card-header">
            <div class="card-title">
                <span class="card-icon"><i class="flaticon-list-2 text-primary"></i> </span>
                <h3 class="card-label">{{ $page_description }}</h3>
            </div>
            <div class="card-toolbar">
                <button data-toggle="modal" data-target="#addCategory" class="btn btn-primary btn-sm font-weight-bold"><i class="fas fa-plus-circle"></i> Nouvelle catégorie</button>
            </div>
        </div>
        <div class="card-body">
            <div class="form-group">
                <input type="text" id="searchInList" name="q" class="form-control form-control-lg" placeholder="Rechercher dans le tableau...">
            </div>
            <div class="card card-custom">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead class="bg-dark text-white">
                            <tr class="text-center">
                                <th>#</th>
                                <th>Nom de la catégorie</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody id="loadListCategory" data-target="/wiki/category/search">
                                @foreach($categories as $category)
                                <tr>
                                    <td class="w-5 text-center">{{ $category->id }}</td>
                                    <td>{{ $category->name }}</td>
                                    <td class="w-10 text-center">
                                        <a href="/wiki/category/{{ $category->id }}" class="btn btn-sm btn-icon btn-danger"><i class="fas fa-trash"></i> </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nouvelle catégorie</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form id="formAddCategory" action="{{ route('Wiki.Category.store') }}" method="POST">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nom de la catégorie <span class="text-danger">*</span> </label>
                            <input type="text" class="form-control" name="name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="btnAddCategory" class="btn btn-primary font-weight-bold">Sauvegarder</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/wiki/category/index.js') }}" type="text/javascript"></script>
@endsection
