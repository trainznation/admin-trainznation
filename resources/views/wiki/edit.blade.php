{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="card card-custom">
        <div class="card-header">
            <div class="card-title">
                <span class="card-icon"><i class="flaticon-list-2 text-primary"></i> </span>
                <h3 class="card-label">{{ $page_description }}</h3>
            </div>
        </div>
        <div class="card-body">
            <form id="editArticle" action="{{ route('Wiki.update', $article->id) }}" method="POST">
                <div class="form-group">
                    <label>Titre de l'article <span class="text-danger">*</span> </label>
                    <input type="text" class="form-control form-control-lg" name="title" value="{{ $article->title }}" required>
                </div>
                <div class="form-group">
                    <label>Contenue de l'article <span class="text-danger">*</span> </label>
                    <textarea name="content" class="form-control" data-provide="markdown" rows="25">{{ $article->content }}</textarea>
                </div>
                <div class="text-right">
                    <button id="btnSubmit" class="btn btn-primary">Enregistrer l'article</button>
                </div>
            </form>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/wiki/edit.js') }}" type="text/javascript"></script>
@endsection
