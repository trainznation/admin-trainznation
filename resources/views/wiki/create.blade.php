{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="card card-custom">
        <div class="card-header">
            <div class="card-title">
                <span class="card-icon"><i class="flaticon-list-2 text-primary"></i> </span>
                <h3 class="card-label">{{ $page_description }}</h3>
            </div>
        </div>
        <div class="card-body">
            <form id="createArticle" action="{{ route('Wiki.store') }}" method="POST">
                <div class="form-group row">
                    <div class="col-md-6">
                        <label>Catégorie</label>
                        <select id="selectCategory" class="form-control selectpicker" name="wiki_category_id" data-provide="selectpicker" required>
                            <option value=""></option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <div id="divSector"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Titre de l'article <span class="text-danger">*</span> </label>
                    <input type="text" class="form-control form-control-lg" name="title" required>
                </div>
                <div class="form-group">
                    <label>Contenue de l'article <span class="text-danger">*</span> </label>
                    <textarea name="content" class="form-control" data-provide="markdown" rows="25"></textarea>
                </div>
                <div class="text-right">
                    <button id="btnSubmit" class="btn btn-primary">Enregistrer l'article</button>
                </div>
            </form>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/wiki/create.js') }}" type="text/javascript"></script>
@endsection
