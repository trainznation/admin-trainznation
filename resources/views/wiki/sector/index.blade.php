{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="card card-custom">
        <div class="card-header">
            <div class="card-title">
                <span class="card-icon"><i class="flaticon-list-2 text-primary"></i> </span>
                <h3 class="card-label">{{ $page_description }}</h3>
            </div>
            <div class="card-toolbar">
                <button data-toggle="modal" data-target="#addSector" class="btn btn-primary btn-sm font-weight-bold"><i class="fas fa-plus-circle"></i> Nouveau Secteur</button>
            </div>
        </div>
        <div class="card-body">
            <div class="form-group">
                <input type="text" id="searchInList" name="q" class="form-control form-control-lg" placeholder="Rechercher dans le tableau...">
            </div>
            <div class="card card-custom">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead class="bg-dark text-white">
                            <tr class="text-center">
                                <th>#</th>
                                <th>Catégorie</th>
                                <th>Nom du secteur</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody id="loadListSector" data-target="/wiki/sector/search">
                                @foreach($sectors as $sector)
                                <tr>
                                    <td class="w-5 text-center">{{ $sector->id }}</td>
                                    <td>{{ $sector->category->name }}</td>
                                    <td>{{ $sector->name }}</td>
                                    <td class="w-10 text-center">
                                        <a href="/wiki/sector/{{ $sector->id }}" class="btn btn-sm btn-icon btn-danger"><i class="fas fa-trash"></i> </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addSector" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nouveau Secteur</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form id="formAddSector" action="{{ route('Wiki.Sector.store') }}" method="POST">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Catégorie parente <span class="text-danger">*</span> </label>
                            <select name="wiki_category_id" class="form-control selectpicker" data-live-search="true" data-provide="selectpicker" required>
                                <option value=""></option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nom du secteur <span class="text-danger">*</span> </label>
                            <input type="text" class="form-control" name="name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="btnAddSector" class="btn btn-primary font-weight-bold">Sauvegarder</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/wiki/sector/index.js') }}" type="text/javascript"></script>
@endsection
