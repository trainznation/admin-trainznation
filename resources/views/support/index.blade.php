{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des tickets</h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Dropdown-->
                <!--end::Dropdown-->
                <!--begin::Button-->
                <a href="#addTicket" class="btn btn-primary" data-toggle="modal"><i class="fas fa-plus-circle"></i> Nouveau ticket</a>
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Search Form-->
            <!--begin::Search Form-->
            <div class="mb-7">
                <div class="row align-items-center">
                    <div class="col-lg-9 col-xl-8">
                        <div class="row align-items-center">
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="input-icon">
                                    <input type="text" class="form-control" placeholder="Recherche..." id="liste_search_tickets" />
                                    <span>
										<i class="flaticon2-search-1 text-muted"></i>
									</span>
                                </div>
                            </div>
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="d-flex align-items-center">
                                    <label class="mr-3 mb-0 d-none d-md-block">Status:</label>
                                    <select class="form-control" id="kt_datatable_search_status">
                                        <option value="">Tous</option>
                                        @foreach($statuses as $status)
                                        <option data-content="<i class='{{ $status->icon }} text-{{ $status->color }}'></i> {{ $status->designation }}" value="{{ $status->id }}"></option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="d-flex align-items-center">
                                    <label class="mr-3 mb-0 d-none d-md-block">Priorité:</label>
                                    <select class="form-control" id="kt_datatable_search_priority">
                                        <option value="">Tous</option>
                                        @foreach($priorities as $priority)
                                        <option data-content="<span class='label label-inline label-{{ $priority->color }}'>{{ $priority->designation }}</span>" value="{{ $priority->id }}"></option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="d-flex align-items-center">
                                    <label class="mr-3 mb-0 d-none d-md-block">Sources:</label>
                                    <select class="form-control" id="kt_datatable_search_source">
                                        <option value="">Tous</option>
                                        @foreach($sources as $source)
                                            <option value="{{ $source->id }}">{{ $source->designation }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Search Form-->
            <!--end: Search Form-->
            <!--begin: Datatable-->
            <table class="datatable datatable-bordered datatable-head-custom" id="listeTickets" data-href="{{ route('Support.list') }}"></table>
            <!--end: Datatable-->
        </div>
    </div>
    <div class="modal fade" id="addTicket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nouveau ticket de support</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form action="{{ route('Support.store') }}" id="formAddTicket" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Sujet <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="subject" required>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-md-3">Catégorie <span class="text-danger">*</span></label>
                            <div class="col-md-3">
                                <select name="support_category_id" id="support_category_id" class="form-control selectpicker" data-provide="selectpicker" required>
                                    <option value=""></option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <label class="col-form-label col-md-3">Secteur <span class="text-danger">*</span></label>
                            <div class="col-md-3">
                                <select name="support_sector_id" id="support_sector_id" class="form-control" required></select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-3">Source <span class="text-danger">*</span> </label>
                            <div class="col-md-3">
                                <select name="support_ticket_source_id" class="form-control selectpicker" data-provide="selectpicker" required>
                                    <option value=""></option>
                                    @foreach($sources as $source)
                                        <option value="{{ $source->id }}">{{ $source->designation }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <label class="col-form-label col-3">Status <span class="text-danger">*</span> </label>
                            <div class="col-md-3">
                                <select name="support_ticket_status_id" class="form-control selectpicker" data-provide="selectpicker" required>
                                    <option value=""></option>
                                    @foreach($statuses as $status)
                                        <option value="{{ $status->id }}">{{ $status->designation }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-3">Ticket emis par <span class="text-danger">*</span></label>
                            <div class="col-3">
                                <select name="requester_id" class="form-control selectpicker" data-provide="selectpicker" required>
                                    <option value=""></option>
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <label class="col-form-label col-3">Assignée à <span class="text-danger">*</span></label>
                            <div class="col-3">
                                <select name="assign_to" class="form-control selectpicker" data-provide="selectpicker" required>
                                    <option value=""></option>
                                    @foreach($admins as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Message ou description du problème <span class="text-danger">*</span></label>
                            <textarea name="message" rows="10" data-provide="markdown"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary font-weight-bold btn-submit">Valider</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/support/index.js') }}" type="text/javascript"></script>
@endsection
