{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="d-flex flex-row">
        <div class="flex-row-auto offcanvas-mobile w-350px w-x1-400px offcanvas-mobile-on" id="kt_chat_aside">
            <div class="card card-custom">
                <div class="card-body">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td class="font-weight-bold">Catégorie</td>
                                <td class="text-right">{{ $ticket->category->name }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Secteur</td>
                                <td class="text-right">{{ $ticket->sector->name }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Priorité</td>
                                <td class="text-right">
                                    <span class="label label-inline label-{{ $ticket->priority->color }}">{{ $ticket->priority->designation }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Status</td>
                                <td class="text-right">
                                    <span class="label label-inline label-{{ $ticket->status->color }}"><i class="{{ $ticket->status->icon }} text-white"></i> {{ $ticket->status->designation }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Source</td>
                                <td class="text-right">
                                    <span class="label label-inline label-default">{{ $ticket->source->designation }}</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="flex-row-fluid ml-lg-8" id="kt_chat_content">
            <div class="card card-custom mb-5">
                <div class="card-body">
                    <h2 class="text-center">{{ $ticket->subject }}</h2>
                </div>
            </div>
            <div class="card card-custom">
                <div class="card-body">
                    <div class="scroll scroll-pull" data-mobile-height="350">
                        <div class="message" data-href="{{ route('Support.conv', $ticket->id) }}"></div>
                    </div>
                </div>
                <div class="card-footer align-items-center">
                    <!--begin::Compose-->
                    <form id="formSubmitMessage" action="{{ route('Support.send', $ticket->id) }}" method="post">
                        <textarea class="form-control border-0 p-0" rows="2" placeholder="Tapez un message" name="messaging"></textarea>
                        <div class="d-flex align-items-center justify-content-between mt-5">
                            <div>
                                <button type="submit" class="btn btn-primary btn-md text-uppercase font-weight-bold chat-sending py-2 px-6">Envoyer</button>
                            </div>
                        </div>
                    </form>
                    <!--begin::Compose-->
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/support/show.js') }}" type="text/javascript"></script>
@endsection
