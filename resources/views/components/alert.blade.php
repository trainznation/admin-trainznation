@if(session()->has('success'))
    <div class="alert alert-custom alert-notice alert-success fade show" role="alert">
        <div class="alert-icon"><i class="fa fa-check-circle"></i></div>
        <div class="alert-text">{!! session('success') !!}</div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="ki ki-close"></i></span>
            </button>
        </div>
    </div>
@endif

@if(session()->has('warning'))
    <div class="alert alert-custom alert-notice alert-warning fade show" role="alert">
        <div class="alert-icon"><i class="fa fa-warning"></i></div>
        <div class="alert-text">{!! session('warning') !!}</div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="ki ki-close"></i></span>
            </button>
        </div>
    </div>
@endif

@if(session()->has('error'))
    <div class="alert alert-custom alert-notice alert-danger fade show" role="alert">
        <div class="alert-icon"><i class="fa fa-times-circle"></i></div>
        <div class="alert-text">{!! session('error') !!}</div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="ki ki-close"></i></span>
            </button>
        </div>
    </div>
@endif

@if(session()->has('info'))
    <div class="alert alert-custom alert-notice alert-info fade show" role="alert">
        <div class="alert-icon"><i class="fa fa-info-circle"></i></div>
        <div class="alert-text">{!! session('info') !!}</div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="ki ki-close"></i></span>
            </button>
        </div>
    </div>
@endif

@if($errors->any())
    <div class="alert alert-custom alert-notice alert-error fade show" role="alert">
        <div class="alert-icon"><i class="fa fa-times-circle"></i></div>
        <div class="alert-text">
            <h4 class="alert-heading">Erreur !</h4>
            <ul>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="ki ki-close"></i></span>
            </button>
        </div>
    </div>
@endif
