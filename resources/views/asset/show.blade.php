{{-- Extends layout --}}
@extends('layout.default')

@section('page_toolbars')
    <a href="{{ route('Asset.edit', $asset['id']) }}" class="btn btn-icon" data-toggle="tooltip" title="Editer l'objet"><i class="fa fa-edit text-primary"></i> </a>
    <a href="{{ route('Asset.delete', $asset['id']) }}" id="btnTrash" class="btn btn-icon" data-toggle="tooltip" title="Supprimer l'objet"><i class="fa fa-trash text-danger"></i> </a>
    @if($asset['meshes'] == 1)
        <button class="btn btn-icon" data-toggle="modal" data-target="#addSketchFab"><i class="fas fa-cubes text-warning" data-toggle="tooltip" title="Enregistrer le meshes 3D"></i> </button>
    @endif
    @if($asset['pricing'] == 1)
        <button class="btn btn-icon" data-toggle="modal" data-target="#addPrice"><i class="fas fa-euro-sign text-danger" data-toggle="tooltip" title="Enregistrer le tarif"></i> </button>
    @endif
    @if($asset['published'] == 1)
        <a href="{{ route('Asset.dispublish', $asset['id']) }}" class="btn btn-icon" data-toggle="tooltip" title="Dépublier l'objet"><i class="fa fa-times text-danger"></i> </a>
    @else
        <a href="{{ route('Asset.publish', $asset['id']) }}" class="btn btn-icon" data-toggle="tooltip" title="Publier l'objet"><i class="fa fa-check text-success"></i> </a>
    @endif
@endsection

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-4">
        <div class="card card-custom overlay mb-3">
            <div class="card-body p-0">
                <div class="overlay-wrapper">
                    <img src="{{ $asset['image'] }}" alt="" class="w-100 rounded"/>
                </div>
                <div class="overlay-layer align-items-end justify-content-center">
                    <div class="d-flex flex-grow-1 flex-center bg-white-o-5 py-5">
                        <a href="{{ route('Asset.edit', $asset['id']) }}" class="btn font-weight-bold btn-light-primary btn-shadow ml-2">Editer</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-custom gutter-b">
            <div class="card-body pt-4">
                <div class="pt-8 pb-6">
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">Social:</span>
                        <span>
                            @if($asset['social'] == 0)
                                <i class="socicon-facebook text-primary"></i>
                                <i class="socicon-twitter text-primary"></i>
                            @else
                                <i class="socicon-facebook"></i>
                                <i class="socicon-twitter"></i>
                            @endif
                        </span>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">Publier:</span>
                        <span>
                            @if($asset['published'] == 0)
                                <i class="fa fa-times-circle text-danger" data-toggle="tooltip" title="Non Publier"></i>
                            @else
                                <i class="fa fa-check-circle text-success" data-toggle="tooltip" title="Publier le {{ $asset['published_at']['normalize'] }}"></i>
                            @endif
                        </span>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">Mesh 3D:</span>
                        <span>
                            @if($asset['meshes'] == 0)
                                <i class="fa fa-times-circle text-danger"></i>
                            @else
                                <i class="fa fa-check-circle text-success"></i>
                            @endif
                        </span>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">Prix:</span>
                        <span>
                            @if($asset['pricing'] == 0)
                                <span class="text-success font-weight-bold">Gratuit</span>
                            @else
                                <span class="text-danger font-weight-bold">{{ format_currency($asset['price']) }}</span>
                            @endif
                        </span>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">Fichier CDP:</span>
                        <span>
                            @if(\Illuminate\Support\Facades\Storage::disk(env('FILESYSTEM_DRIVER'))->exists('/assets/'.$asset['id'].'/'.$asset['uuid'].'.zip') == false)
                                <i class="fa fa-times-circle text-danger" data-toggle="tooltip" title="Non disponible"></i>
                            @else
                                <i class="fa fa-check-circle text-success"></i>
                            @endif
                        </span>
                    </div>
                </div>
                <div class="pb-6">
                    {{ $asset['short_description'] }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card card-custom">
            <div class="card-header card-header-tabs-line">
                <div class="card-toolbar">
                    <ul class="nav nav-tabs nav-bold nav-tabs-line">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#description">
                                <span class="nav-icon"><i class="flaticon2-edit"></i></span>
                                <span class="nav-text">Description</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#kuid">
                                <span class="nav-icon"><i class="flaticon2-list"></i></span>
                                <span class="nav-text">Kuid</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tags">
                                <span class="nav-icon"><i class="flaticon2-tag"></i></span>
                                <span class="nav-text">Tags</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description">
                        <div id="markedDown" data-content="{{ $asset['description'] }}"></div>
                    </div>
                    <div class="tab-pane fade" id="kuid" role="tabpanel" aria-labelledby="kuid">
                        <div class="card card-custom">
                            <div class="card-header">
                                <div class="card-title">
									<span class="card-icon">
										<i class="flaticon2-chat-1 text-primary"></i>
									</span>
                                    <h3 class="card-label">Liste des kuids</h3>
                                </div>
                                <div class="card-toolbar">
                                    <a data-toggle="modal" href="#addKuid" class="btn btn-sm btn-icon btn-light-success mr-2">
                                        <i class="flaticon2-plus"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered" id="tableKuid">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>#</th>
                                            <th>Kuid</th>
                                            <th>Publier le</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($asset['kuids'] as $kuid)
                                        <tr>
                                            <td>{{ $kuid['id'] }}</td>
                                            <td>{{ '<'.$kuid['kuid'].'>' }}</td>
                                            <td>{{ \Illuminate\Support\Carbon::createFromTimestamp(strtotime($kuid['published_at']))->format('d/m/Y à H:i') }}</td>
                                            <td>
                                                <button class="btn btn-sm btn-icon btn-danger btn-delete-kuid" data-href="{{ route('Asset.Kuid.delete', [$asset['id'], $kuid['id']]) }}" data-toggle="tooltip" title="Supprimer"><i class="fa fa-trash"></i> </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tags" role="tabpanel" aria-labelledby="tags">
                        <div class="card card-custom">
                            <div class="card-header">
                                <div class="card-title">
									<span class="card-icon">
										<i class="flaticon2-chat-1 text-primary"></i>
									</span>
                                    <h3 class="card-label">Liste des Tags</h3>
                                </div>
                                <div class="card-toolbar">
                                    <a data-toggle="modal" href="#addTags" class="btn btn-sm btn-icon btn-light-success mr-2">
                                        <i class="flaticon2-plus"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered" id="tableTags">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th>#</th>
                                        <th>Tag</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($asset['tags'] as $tag)
                                        <tr>
                                            <td>{{ $tag['id'] }}</td>
                                            <td>{{ $tag['name']['fr'] }}</td>
                                            <td>
                                                <button class="btn btn-sm btn-icon btn-danger btn-delete-tag" data-href="{{ route('Asset.Tags.delete', [$asset['id'], $tag['id']]) }}" data-toggle="tooltip" title="Supprimer"><i class="fa fa-trash"></i> </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addKuid" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nouveau Kuid</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form id="formaddKuid" action="{{ route('Asset.Kuid.store', $asset['id']) }}" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="kuid">Kuid</label>
                        <input type="text" id="kuid_field" class="form-control" name="kuid">
                    </div>
                    <div class="form-group">
                        <div class="input-group date" id="published_at" data-target-input="nearest">
                            <label for="published_at" class="col-form-label mr-2">Publier le</label>
                            <input type="text" name="published_at" class="form-control datetimepicker-input" placeholder="Select date &amp; time" data-target="#published_at" value="{{ now()->format('Y-m-d H:i:s') }}"/>
                            <div class="input-group-append" data-target="#published_at" data-toggle="datetimepicker">
															<span class="input-group-text">
																<i class="ki ki-calendar"></i>
															</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btnFormAddKuid" class="btn btn-success font-weight-bold">Valider</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="addTags" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nouveau Tags</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form id="formAddTags" action="{{ route('Asset.Tags.store', $asset['id']) }}" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="kuid">Kuid</label>
                        <input type="text" id="tags_field" class="form-control tagify" name="tags" autofocus="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btnFormAddTags" class="btn btn-success font-weight-bold">Valider</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="addSketchFab" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Connect Sketchfab</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form id="formAddSketchfab" action="{{ route('Asset.addSketchfab', $asset['id']) }}" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="kuid">UID Sketchfab</label>
                        <input type="text" name="sketchfab_uid" class="form-control" value="{{ $asset['sketchfab_uid'] }}">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btnFormAddSketchfab" class="btn btn-success font-weight-bold">Valider</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="addPrice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tarif de l'objet</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form id="formAddPrice" action="{{ route('Asset.addPrice', $asset['id']) }}" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="kuid">Montant</label>
                        <input type="text" name="price" class="form-control" value="{{ $asset['price'] }}">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btnFormAddPrice" class="btn btn-success font-weight-bold">Valider</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/marked/marked.min.js"></script>
    <script src="{{ asset('js/scripts/asset/show.js') }}" type="text/javascript"></script>
@endsection
