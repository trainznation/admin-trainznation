{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">HTML Table
                    <div class="text-muted pt-2 font-size-sm">Datatable initialized from HTML table</div>
                </h3>
            </div>
            <div class="card-toolbar">
                <a href="#addcategory" class="btn btn-primary font-weight-bolder" data-toggle="modal">
                   <i class="fa fa-plus-circle"></i> Nouvelle catégorie
                </a>
            </div>
        </div>
        <div class="card-body">
            <!--begin: Search Form-->
            <!--begin::Search Form-->
            <div class="mb-7">
                <div class="row align-items-center">
                    <div class="col-lg-9 col-xl-8">
                        <div class="row align-items-center">
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="input-icon">
                                    <input type="text" class="form-control" placeholder="Search..." id="search_liste_categories"/>
                                    <span>
                                        <i class="flaticon2-search-1 text-muted"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                        <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Recherche</a>
                    </div>
                </div>
            </div>
            <!--end::Search Form-->
            <!--end: Search Form-->
            <!--begin: Datatable-->
            <table class="datatable datatable-bordered datatable-head-custom" id="listeCategories">
                <thead>
                <tr>
                    <th title="Field #1">#</th>
                    <th title="Field #1">Icon</th>
                    <th title="Field #1">Designation</th>
                    <th title="Field #1">Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($categories->data as $category)
                        <tr>
                            <td>{{ $category['id'] }}</td>
                            <td>
                                <div class="symbol symbol-circle mr-3">
                                    <img alt="Pic" src="{{ $category['image'] }}" style="width: 32px; height: 32px;"/>
                                </div>
                            </td>
                            <td>{{ $category['name'] }}</td>
                            <td>
                                <a href="{{ route('Asset.Category.destroy', $category['id']) }}" class="btn btn-icon btn-danger btn-sm mr-2 btn-delete"><i class="fa fa-trash"></i> </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <div class="modal fade" id="addcategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-plus-circle"></i> Nouvelle catégorie</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form id="formAddCategorie" action="" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nom de la catégorie <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="name">
                        </div>
                        <div class="form-group">
                            <label>Icone de la catégorie</label>
                            <div></div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="icon" id="customFile">
                                <label class="custom-file-label" for="customFile">Selectionnez un fichier</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Fermer</button>
                        <button type="submit" class="btn btn-primary font-weight-bold btn-submit">Sauvegarder</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/asset/category/index.js') }}" type="text/javascript"></script>
@endsection
