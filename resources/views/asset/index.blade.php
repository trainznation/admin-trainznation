{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des Objets</h3>
            </div>
            <div class="card-toolbar">
                <a href="{{ route('Asset.create') }}" class="btn btn-primary font-weight-bolder">
                   <i class="fa fa-plus-circle"></i> Nouvelle Objets
                </a>
            </div>
        </div>
        <div class="card-body">
            <!--begin: Search Form-->
            <!--begin::Search Form-->
            <div class="mb-7">
                <div class="row align-items-center">
                    <div class="col-lg-9 col-xl-8">
                        <div class="row align-items-center">
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="input-icon">
                                    <input type="text" class="form-control" placeholder="Search..." id="search_liste_objet"/>
                                    <span>
                                        <i class="flaticon2-search-1 text-muted"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="d-flex align-items-center">
                                    <label class="mr-3 mb-0 d-none d-md-block">Status:</label>
                                    <select class="form-control" id="kt_datatable_search_publier">
                                        <option value="">Tous</option>
                                        <option value="0">Non Publier</option>
                                        <option value="1">Publier</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                        <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Recherche</a>
                    </div>
                </div>
            </div>
            <!--end::Search Form-->
            <!--end: Search Form-->
            <!--begin: Datatable-->
            <table class="datatable datatable-bordered datatable-head-custom" id="listeObjets">
                <thead>
                <tr>
                    <th title="Field #1">#</th>
                    <th title="Field #1">Images</th>
                    <th title="Field #1">Designation</th>
                    <th title="Field #1">Publier</th>
                    <th title="Field #1">Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($assets->data as $asset)
                        <tr>
                            <td>{{ $asset['id'] }}</td>
                            <td>
                                <img alt="Pic" src="{{ $asset['image'] }}" class="img-fluid"/>
                            </td>
                            <td>
                                <strong>Catégorie:</strong> {{ $asset['asset_category']['name'] }} <br>
                                {{ $asset['designation'] }}
                            </td>
                            <td>{{ $asset['published'] }}</td>
                            <td>
                                <a href="{{ route('Asset.show', $asset['id']) }}" class="btn btn-primary btn-icon btn-sm"><i class="fa fa-eye"></i> </a>
                                <a href="{{ route('Asset.edit', $asset['id']) }}" class="btn btn-info btn-icon btn-sm"><i class="fa fa-edit"></i> </a>
                                <button class="btn btn-danger btn-icon btn-sm" data-id="{{ $asset['id'] }}"><i class="fa fa-trash"></i> </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/asset/index.js') }}" type="text/javascript"></script>
@endsection
