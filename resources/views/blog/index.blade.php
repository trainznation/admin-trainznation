{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">{{ $page_title }}
                </h3>
            </div>
            <div class="card-toolbar">
                <a href="{{ route('Blog.create') }}" class="btn btn-primary font-weight-bolder">
                   <i class="fa fa-plus-circle"></i> Nouvelle article
                </a>
            </div>
        </div>
        <div class="card-body">
            <!--begin: Search Form-->
            <!--begin::Search Form-->
            <div class="mb-7">
                <div class="row align-items-center">
                    <div class="col-lg-9 col-xl-8">
                        <div class="row align-items-center">
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="input-icon">
                                    <input type="text" class="form-control" placeholder="Rechercher..." id="search_liste_article"/>
                                    <span>
                                        <i class="flaticon2-search-1 text-muted"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="d-flex align-items-center">
                                    <label class="mr-3 mb-0 d-none d-md-block">Status:</label>
                                    <select class="form-control" id="kt_datatable_search_publier">
                                        <option value="">Tous</option>
                                        <option value="0">Non Publier</option>
                                        <option value="1">Publier</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                        <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Recherche</a>
                    </div>
                </div>
            </div>
            <!--end::Search Form-->
            <!--end: Search Form-->
            <!--begin: Datatable-->
            <table class="datatable datatable-bordered datatable-head-custom" id="listeArticle">
                <thead>
                <tr>
                    <th title="Field #1">#</th>
                    <th title="Field #1">Images</th>
                    <th title="Field #1">Designation</th>
                    <th title="Field #1">Publier</th>
                    <th title="Field #1">Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($articles->data as $article)
                        <tr>
                            <td>{{ $article['id'] }}</td>
                            <td>
                                <img alt="Pic" src="{{ $article['images'] }}" class="img-fluid"/>
                            </td>
                            <td>
                                <strong>Catégorie:</strong> {{ $article['category']['name'] }}<br><br>
                                {{ $article['title'] }}
                            </td>
                            <td>{{ $article['published'] }}</td>
                            <td>
                                <a href="{{ route('Blog.show', $article['id']) }}" class="btn btn-primary btn-icon btn-sm" data-toggle="tooltip" title="Voir l'article"><i class="fa fa-eye"></i> </a>
                                <a href="{{ route('Blog.edit', $article['id']) }}" class="btn btn-info btn-icon btn-sm" data-toggle="tooltip" title="Editer l'article"><i class="fa fa-edit"></i> </a>
                                <a class="btn btn-danger btn-icon btn-sm" href="{{ route('Blog.delete', $article['id']) }}" data-toggle="tooltip" title="Supprimer l'article"><i class="fa fa-trash"></i> </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/blog/index.js') }}" type="text/javascript"></script>
@endsection
