{{-- Extends layout --}}
@extends('layout.default')

@section('page_toolbars')
    <a href="{{ route('Blog.edit', $blog['id']) }}" class="btn btn-icon" data-toggle="tooltip" title="Editer l'article"><i class="fa fa-edit text-primary"></i> </a>
    <a href="{{ route('Blog.delete', $blog['id']) }}" class="btn btn-icon" data-toggle="tooltip" title="Supprimer l'article"><i class="fa fa-trash text-danger"></i> </a>
    @if($blog['published'] == 1)
        <a href="{{ route('Blog.dispublish', $blog['id']) }}" id="btn-dispublish" class="btn btn-icon" data-toggle="tooltip" title="Dépublier l'article"><i class="fa fa-times text-danger"></i> </a>
    @else
        <a href="{{ route('Blog.publish', $blog['id']) }}" id="btn-publish" class="btn btn-icon" data-toggle="tooltip" title="Publier l'article"><i class="fa fa-check text-success"></i> </a>
    @endif
@endsection

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-4">
        <div class="card card-custom overlay mb-3">
            <div class="card-body p-0">
                <div class="overlay-wrapper">
                    <img src="{{ $blog['images'] }}" alt="" class="w-100 rounded"/>
                </div>
                <div class="overlay-layer align-items-end justify-content-center">
                    <div class="d-flex flex-grow-1 flex-center bg-white-o-5 py-5">
                        <a href="{{ route('Blog.edit', $blog['id']) }}" class="btn font-weight-bold btn-light-primary btn-shadow ml-2">Editer</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-custom gutter-b">
            <div class="card-body pt-4">
                <div class="pt-8 pb-6">
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">Social:</span>
                        <span>
                            @if($blog['social'] == 0)
                                <i class="socicon-facebook text-primary"></i>
                                <i class="socicon-twitter text-primary"></i>
                            @else
                                <i class="socicon-facebook"></i>
                                <i class="socicon-twitter"></i>
                            @endif
                        </span>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">Publier:</span>
                        <span>
                            @if($blog['published'] == 0)
                                <i class="fa fa-times-circle text-danger" data-toggle="tooltip" title="Non Publier"></i>
                            @else
                                <i class="fa fa-check-circle text-success" data-toggle="tooltip" title="Publier le {{ $blog['published_at']['normalize'] }}"></i>
                            @endif
                        </span>
                    </div>
                </div>
                <div class="pb-6">
                    {{ $blog['short_content'] }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card card-custom">
            <div class="card-header card-header-tabs-line">
                <div class="card-toolbar">
                    <ul class="nav nav-tabs nav-bold nav-tabs-line">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#description">
                                <span class="nav-icon"><i class="flaticon2-edit"></i></span>
                                <span class="nav-text">Description</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#comments">
                                <span class="nav-icon"><i class="fa fa-comments"></i></span>
                                <span class="nav-text">Commentaires</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description">
                        <div id="markedDown" data-content="{{ $blog['content'] }}"></div>
                    </div>
                    <div class="tab-pane fade" id="comments" role="tabpanel" aria-labelledby="comments">
                        <div class="card card-custom">
                            <div class="card-header">
                                <div class="card-title">
									<span class="card-icon">
										<i class="flaticon2-chat-1 text-primary"></i>
									</span>
                                    <h3 class="card-label">Liste des commentaires</h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered" id="tableComments">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>Date</th>
                                            <th>Utilisateur</th>
                                            <th>Approuver</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($blog['comments']) == 0)
                                            <tr>
                                                <td colspan="5" class="font-weight-bold text-center">Aucun commentaire pour cette article</td>
                                            </tr>
                                        @else
                                            @foreach($blog['comments'] as $comment)
                                                <tr>
                                                    <td>{{ $blog['updated_at']['normalize'] }}</td>
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <div class="symbol symbol-35 symbol-circle mt-2 mr-2">
                                                                    <span class="symbol-label">
                                                                        <img src="{{ $comment['user']['avatar']['encoded'] }}" class="h-75 align-self-end" alt="">
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <strong>{{ $comment['user']['name'] }}</strong><br>
                                                                <i class="text-muted h6">{{ $comment['user']['email'] }}</i>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center">
                                                        @if($comment['approuved'] == true)
                                                            <div class="symbol symbol-20 mt-2 mr-2 text-center">
                                                                <span class="symbol-label ">
                                                                    <i class="fa fa-check-circle fa-2x text-success"></i>
                                                                </span>
                                                            </div>
                                                        @else
                                                            <div class="symbol symbol-20 mt-2 mr-2 text-center">
                                                                <span class="symbol-label ">
                                                                    <i class="fa fa-times-circle fa-2x text-danger"></i>
                                                                </span>
                                                            </div>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-sm btn-icon btn-primary btn-details" data-comment="{{ $comment['comment'] }}" data-title="{{ $blog['title'] }}" data-toggle="tooltip" title="Voir le commentaire"><i class="fa fa-eye"></i> </button>
                                                        @if($comment['approuved'] == true)
                                                            <button class="btn btn-sm btn-icon btn-danger btn-disapprove" data-commentid="{{ $comment['id'] }}" data-articleid="{{ $blog['id'] }}" data-toggle="tooltip" data-title="Désapprouve"><i class="fa fa-times"></i> </button>
                                                        @else
                                                            <button class="btn btn-sm btn-icon btn-success btn-approve" data-commentid="{{ $comment['id'] }}" data-articleid="{{ $blog['id'] }}" data-toggle="tooltip" data-title="Approuver"><i class="fa fa-check"></i> </button>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalDetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/marked/marked.min.js"></script>
    <script src="{{ asset('js/scripts/blog/show.js') }}" type="text/javascript"></script>
@endsection
