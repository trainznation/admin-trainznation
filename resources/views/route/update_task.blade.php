{{-- Extends layout --}}
@extends('layout.default')

@section('styles')
    <style type="text/css">
        .asset_img_class {
            width: 720px !important;
            height: 405px !important;
        }
    </style>
@endsection

{{-- Content --}}
@section('content')
    <div class="card card-custom">
        <form id="formAddTask" action="{{ route('Version.Task.update', [$version->route_id, $version->id, $task->id]) }}" method="post">
            @method("PUT")
            <div class="card-body">
                <div class="form-group">
                    <label for="name">Nom de la taches <span class="text-danger">*</span> </label>
                    <input type="text" id="name" class="form-control" name="name" required value="{{ $task->name }}">
                </div>
                <div class="form-group">
                    <label for="lieu">Lieu de la taches </label>
                    <input type="text" id="lieu" class="form-control" name="lieu" value="{{ $task->lieu }}">
                </div>
                <div class="form-group">
                    <label for="priority">Priorité de la tache <span class="text-danger">*</span></label>
                    <select name="priority" class="form-control selectpicker" data-live-search="true" required>
                        <option value="{{ $task->priority->value }}">{{ $task->priority->label }}</option>
                        <option value="0">Basse</option>
                        <option value="1">Moyenne</option>
                        <option value="2">Haute</option>
                        <option value="3">Critique</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="status">Etat actuel de la tache <span class="text-danger">*</span></label>
                    <select name="status" class="form-control selectpicker" data-live-search="true" required>
                        <option value="{{ $task->etat }}">{{ $task->etat }}</option>
                        <option value="0">En attente</option>
                        <option value="1">En Cours</option>
                        <option value="2">Terminer</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="description">Description <span class="text-danger">*</span></label>
                    <textarea name="description" id="description" cols="30" rows="10" data-provide="markdown" required>{{ $task->description }}</textarea>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <div class="input-group date" id="started_at" data-target-input="started_at">
                            <label for="started_at" class="col-form-label mr-2">Début de la tache <span class="text-danger">*</span></label>
                            <input type="text" name="started_at" class="form-control datetimepicker-input" placeholder="Select date &amp; time" data-target="#started_at" required value="{{ $task->started_at->format }}"/>
                            <div class="input-group-append" data-target="#started_at" data-toggle="datetimepicker">
										<span class="input-group-text">
											<i class="ki ki-calendar"></i>
										</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group date" id="finished_at" data-target-input="finished_at">
                            <label for="finished_at" class="col-form-label mr-2">Fin de la tache</label>
                            <input type="text" name="finished_at" class="form-control datetimepicker-input" placeholder="Select date &amp; time" data-target="#finished_at" value="{{ $task->finished_at->format }}"/>
                            <div class="input-group-append" data-target="#finished_at" data-toggle="datetimepicker">
										<span class="input-group-text">
											<i class="ki ki-calendar"></i>
										</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Valider</button>
            </div>
        </form>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/route/update_card.js') }}" type="text/javascript"></script>
@endsection
