<div class="card card-custom">
    <div class="card-header">
        <div class="card-title">Liste des versions de la route: <i>{{ $route->name }}</i></div>
        <div class="card-toolbar">
            <button class="btn btn-icon btn-hover-light-primary" data-toggle="modal" data-target="#addVersion"><i class="fa fa-plus"></i> </button>
        </div>
    </div>
    <div class="card-body">
        <div class="timeline timeline-2">
            <div class="timeline-bar"></div>
            @foreach($route->versions as $version)
            <div class="timeline-item">
                @if($version['published_at'] != null)
                    <div class="timeline-badge bg-success"></div>
                @else
                    <div class="timeline-badge bg-danger"></div>
                @endif
                    <div class="timeline-content d-flex align-items-center justify-content-between">
						<span class="mr-3">
							<strong>Version: </strong> {{ $version['version'] }} / <strong>Build: </strong> {{ $version['build'] }}
						</span>
                        <span class="text-muted text-right">
                            @if($version['published_at']['format'] == null)
                                <a href="{{ route('Route.Version.publish', [$version['route_id'], $version['id']]) }}" class="btn btn-icon btn-sm" data-toggle="tooltip" title="Publier cette version"><i class="fa fa-check text-success"></i> </a>
                            @else
                                @if($version['published_at']['format'] <= now()->addDay())
                                    Publié {{ $version['published_at']['human'] }}
                                @else
                                    Publié le {{ $version['published_at']['normalize'] }}
                                @endif
                            @endif
                        </span>
                    </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<div class="modal fade" id="addVersion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nouvelle Version</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form action="{{ route('Route.Version.store', $route->id) }}" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="version" class="col-form-label col-md-3">Version <span class="text-danger">*</span> </label>
                        <div class="col-md-3">
                            <input type="text" id="version" class="form-control" name="version" required>
                        </div>
                        <label for="build" class="col-form-label col-md-3">Build <span class="text-danger">*</span> </label>
                        <div class="col-md-3">
                            <input type="text" id="build" class="form-control" name="build" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Lien de la map</label>
                        <input type="text" class="form-control" name="map_link" />
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="">Gare de départ</label>
                            <input type="text" class="form-control" name="station_start" />
                        </div>
                        <div class="col-md-4">
                            <label for="">Gare d'arrivé</label>
                            <input type="text" class="form-control" name="station_end" />
                        </div>
                        <div class="col-md-4">
                            <label for="">Distance</label>
                            <input type="text" class="form-control" name="distance" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Image de la version</label>
                        <div class="custom-file">
                            <input type="file" name="img_version" class="custom-file-input" id="customFile" accept="image/png">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Video de la version</label>
                        <div class="custom-file">
                            <input type="file" name="video_version" class="custom-file-input" id="customFile" accept="video/mp4">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btnAddVersion" class="btn btn-primary font-weight-bold">Valider</button>
                </div>
            </form>
        </div>
    </div>
</div>
