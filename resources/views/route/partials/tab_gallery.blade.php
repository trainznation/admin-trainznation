<div class="card card-custom mb-5">
    <div class="card-body text-right">
        <button class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#addCategory"><i class="fa fa-plus"></i> Nouvelle catégorie</button>
        <button class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#addImages"><i class="fa fa-plus"></i> Nouvelle Images</button>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <div class="card card-custom">
            <div class="card-body">
                <ul class="navi navi-accent navi-hover navi-bold js-filter">
                    @foreach($route->gallery['categories'] as $category)
                        <li class="navi-item">
                            <a class="navi-link navi-gallery" data-href="#" data-categoryid="{{ $category['id'] }}" data-route="{{ $category['route_id'] }}">
                                <span class="navi-text font-size-lg">{{ $category['name'] }}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="card card-custom gutter-b">
            <div class="card-body">
                <div class="row" id="js-content">
                    <div style="width: 100%;height: 320px;position: relative;top: 120px;"><i class="fa fa-arrow-circle-left fa-5x"></i> <span style="position: relative;font-size: 35px;margin-left: 12px;margin-right: 12px;top: -10px;color: #adabab;">Cliquez sur une catégorie ou créer en une</span> </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nouvelle catégorie</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form action="{{ route('Route.Gallery.Category.store', $route->id) }}" method="POST">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Nom de la catégorie <span class="text-danger">*</span> </label>
                        <input type="text" name="name" class="form-control" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary font-weight-bold">Valider</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="addImages" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nouvelle images</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form action="" method="POST">
                <input type="hidden" id="route_id" name="route_id" value="{{ $route->id }}">
                <input type="hidden" id="category_id" name="category_id">
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="route_category_id" class="col-form-label col-md-3">Catégorie de la gallery</label>
                        <div class="col-md-9">
                            <select id="route_category_id" class="form-control form-control-solid form-control-lg select2">
                                <option label="Label"></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="dropzone dropzone-default dropzone-primary" id="dropImages">
                            <div class="dropzone-msg dz-message needsclick">
                                <h3 class="dropzone-msg-title">Drop files here or click to upload.</h3>
                                <span class="dropzone-msg-desc">Upload up to 10 files</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary font-weight-bold">Valider</button>
                </div>
            </form>
        </div>
    </div>
</div>
