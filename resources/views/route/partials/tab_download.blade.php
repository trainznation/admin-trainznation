<div id="route" data-id="{{ $route->id }}"></div>
<div class="card card-custom">
    <div class="card-header">
        <div class="card-title">Liste des Téléchargement disponible</div>
        <div class="card-toolbar">
            <button class="btn btn-hover-info" data-toggle="modal" data-target="#addDownload"><i class="fa fa-plus"></i> Nouveau téléchargement</button>
        </div>
    </div>
    <div class="card-body">
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-9 col-xl-8">
                    <div class="row align-items-center">
                        <div class="col-md-4 my-2 my-md-0">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Rechercher..." id="search_query_download" />
                                <span>
												<i class="flaticon2-search-1 text-muted"></i>
											</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="datatable datatable-bordered datatable-head-custom" id="liste_download"></div>
    </div>
</div>
<div class="modal fade" id="addDownload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nouveau téléchargement</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form action="{{ route('Route.Download.store', $route->id) }}" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group row">
                        <x-input
                            name="version"
                            type="text"
                            label="Version"
                            value="{!! $route->version !!}"
                            required="true"
                            classLabel="col-form-label col-md-2"
                            classInput="col-md-3">
                        </x-input>
                        <x-input
                            name="build"
                            type="text"
                            label="Build"
                            value="{!! $route->build !!}"
                            required="true"
                            classLabel="col-form-label col-md-2"
                            classInput="col-md-3">
                        </x-input>
                    </div>
                    <div class="form-group">
                        <textarea name="note" id="note" class="form-control" rows="10" data-provide="markdown" placeholder="Note de version"></textarea>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-8">
                            <label>Type</label>
                            <div class="radio-inline">
                                <label class="radio">
                                    <input type="radio" name="type" checked="checked" value="alpha"/>
                                    <span></span>
                                    <div class="symbol symbol-circle symbol-danger ml-2" data-toggle="tooltip" title="Alpha">
                                        <span class="symbol-label">A</span>
                                    </div>
                                </label>
                                <label class="radio">
                                    <input type="radio" name="type" value="beta"/>
                                    <span></span>
                                    <div class="symbol symbol-circle symbol-warning ml-2" data-toggle="tooltip" title="Beta">
                                        <span class="symbol-label">B</span>
                                    </div>
                                </label>
                                <label class="radio">
                                    <input type="radio" name="type" value="release"/>
                                    <span></span>
                                    <div class="symbol symbol-circle symbol-success ml-2" data-toggle="tooltip" title="Release">
                                        <span class="symbol-label">R</span>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="checkbox-inline">
                                <label class="checkbox">
                                    <input type="checkbox" name="published" checked="checked"/>
                                    <span></span>
                                    Ce téléchargement est t-il publier
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Fichier de la route <span class="text-danger">*</span> </label>
                        <div></div>
                        <div class="custom-file">
                            <input type="file" name="cdp" class="custom-file-input" id="customFile" accept=".zip" required>
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                        <span class="form-text text-muted">Zipper le fichier route et dependance ensemble</span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary font-weight-bold">Valider</button>
                </div>
            </form>
        </div>
    </div>
</div>
