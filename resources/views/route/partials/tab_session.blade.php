
<div id="route" data-id="{{ $route->id }}"></div>
<div class="card card-custom">
    <div class="card-header">
        <div class="card-title">Liste des Sessions disponible</div>
        <div class="card-toolbar">
            <button class="btn btn-hover-info" data-toggle="modal" data-target="#addSession"><i class="fa fa-plus"></i> Nouvelle Session</button>
        </div>
    </div>
    <div class="card-body">
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-9 col-xl-8">
                    <div class="row align-items-center">
                        <div class="col-md-4 my-2 my-md-0">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Rechercher..." id="search_query_session" />
                                <span>
												<i class="flaticon2-search-1 text-muted"></i>
											</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="datatable datatable-bordered datatable-head-custom" id="liste_session"></div>
    </div>
</div>

<div class="modal fade" id="addSession" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nouvelle session</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form action="{{ route('Route.Session.store', $route->id) }}" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <x-input
                            name="name"
                            label="Nom de la sessions"
                            type="text"
                            required="true"
                            autofocus="true"
                            ></x-input>
                    </div>
                    <div class="form-group">
                        <label>Court descriptif</label>
                        <textarea name="short_content" id="short_content" rows="6" class="form-control" data-provide="markdown" maxlength="255"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea name="content" id="content" class="form-control" cols="30" rows="10" data-provide="markdown"></textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="">Publier ?</label>
                                <input data-switch="true" id="published" type="checkbox" name="published" checked="checked" data-on-text="Publier" data-handle-width="50" data-off-text="Non publier" data-on-color="success" data-off-color="danger" onchange="appearPublishedAt()"/>
                            </div>
                        </div>
                        <div class="col-md-5" id="show_published_at">
                            <div class="form-group">
                                <label for="">Date de publication</label>
                                <input type="text" class="form-control" name="published_at" id="published_at" placeholder="Select date & time"  data-toggle="datetimepicker" data-target="#published_at"/>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <x-input
                                    name="kuid"
                                    label="KUID de la sessions"
                                    type="text"
                                    required="true"
                                    autofocus="true"
                                ></x-input>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="asset_image">Image de la session</label>
                        <div class="image-input image-input-empty image-input-outline" id="session_img" style="background-image: url({{ asset('media/users/blank.png') }})">
                            <div class="image-input-wrapper asset_img_class"></div>

                            <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                <i class="fa fa-pen icon-sm text-muted"></i>
                                <input type="file" name="session_img" accept=".png"/>
                                <input type="hidden" name="profile_avatar_remove"/>
                            </label>

                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                                <i class="ki ki-bold-close icon-xs text-muted"></i>
                                            </span>

                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="Remove avatar">
                                                <i class="ki ki-bold-close icon-xs text-muted"></i>
                                            </span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Fichier CDP ou ZIP</label>
                        <div class="col-md-9">
                            <div class="custom-file">
                                <input type="file" name="file_session" class="custom-file-input" id="customFile" accept=".zip,.cdp">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success font-weight-bold">Valider</button>
                </div>
            </form>
        </div>
    </div>
</div>
