<div class="row">
    <div class="col-md-4">
        <div class="card card-custom overlay">
            <div class="card-body p-0">
                <div class="overlay-wrapper">
                    <img src="{{ $route->image }}" alt="" class="w-100 rounded"/>
                </div>
                <div class="overlay-layer align-items-end justify-content-center">
                    <div class="d-flex flex-grow-1 flex-center bg-white-o-5 py-5">
                        <a href="{{ route('Route.edit', $route->id) }}" class="btn font-weight-bold btn-light-primary btn-shadow ml-2">Editer</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-custom gutter-b mt-3">
            <div class="card-body pt-4">
                <div class="pt-8 pb-6">
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">Publier:</span>
                        <span>
                            @if($route->published == 1)
                                <i class="fa fa-check-circle text-success"></i>
                            @else
                                <i class="fa fa-times-circle text-danger"></i>
                            @endif
                        </span>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">Version actuel:</span>
                        <span>
                            {{ 'V'.$route->version.':'.$route->build }}
                        </span>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">Kuid de la route:</span>
                        <span>
                            {{ '<'.$route->route_kuid.'>' }}
                        </span>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">Kuid des dépendances:</span>
                        <span>
                            {{ '<'.$route->dependance_kuid.'>' }}
                        </span>
                    </div>
                </div>
                <div class="pb-6">
                    {{ \Illuminate\Support\Str::limit($route->description, 150, '...') }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card card-custom gutter-b card-stretch" id="kt_page_stretched_card">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">Description</h3>
                </div>
            </div>
            <div class="card-body">
                <div class="card-scroll">
                    <div id="marked" data-content="{{ $route->description }}"></div>
                </div>
            </div>
        </div>
    </div>
</div>
