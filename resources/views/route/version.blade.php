{{-- Extends layout --}}
@extends('layout.default')

@section('styles')
    <style type="text/css">
        .asset_img_class {
            width: 720px !important;
            height: 405px !important;
        }
    </style>
@endsection

{{-- Content --}}
@section('content')
    <input type="hidden" id="version_uri" value="/route/{{ $version->route_id }}/version/{{ $version->id }}/info">
    <div class="row">
        <div class="col-md-5">
            <div class="card card-custom card-stretch gutter-b">
                <div class="card-header h-auto border-0">
                    <div class="card-title py-5">
                        <h3 class="card-label">
                            <span class="d-block text-dark font-weight-bolder">Average Système Tasks</span>
                            <span id="averageVersion" data-percent="{{ $version->percent_task }}" class="d-block text-muted mt-2 font-size-sm"></span>
                        </h3>
                    </div>
                </div>
                <div class="card-body card-body d-flex flex-column">
                    <div class="flex-grow-1">
                        <div id="chartAverage" style="height: 200px"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="card card-custom card-stretch gutter-b">
                <div class="card-body pt-0">
                    <div class="d-flex align-items-center mb-3 mt-15 bg-light rounded p-5">
                        <!--begin::Icon-->
                        <span class="svg-icon svg-icon-primary mr-5">
						<span class="svg-icon svg-icon-lg">
							<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Home/Library.svg-->
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"/>
                                    <circle fill="#000000" cx="12" cy="12" r="8"/>
                                </g>
							</svg>
                            <!--end::Svg Icon-->
						</span>
					</span>
                        <!--end::Icon-->
                        <!--begin::Title-->
                        <div class="d-flex flex-column flex-grow-1 mr-2">
                            <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-h2 mb-1">Taches Inscrites</a>
                        </div>
                        <!--end::Title-->
                        <!--begin::Lable-->
                        <span id="textTaskRegistered" class="font-weight-bolder text-primary py-1 font-size-h2">7</span>
                        <!--end::Lable-->
                    </div>
                    <div class="d-flex align-items-center mb-3 mt-3 bg-light rounded p-5">
                        <!--begin::Icon-->
                        <span class="svg-icon svg-icon-warning mr-5">
						<span class="svg-icon svg-icon-lg">
							<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Home/Library.svg-->
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"/>
                                    <circle fill="#000000" cx="12" cy="12" r="8"/>
                                </g>
							</svg>
                            <!--end::Svg Icon-->
						</span>
					</span>
                        <!--end::Icon-->
                        <!--begin::Title-->
                        <div class="d-flex flex-column flex-grow-1 mr-2">
                            <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-h2 mb-1">Tache en cours</a>
                        </div>
                        <!--end::Title-->
                        <!--begin::Lable-->
                        <span id="textTaskProgress" class="font-weight-bolder text-warning py-1 font-size-h2">7</span>
                        <!--end::Lable-->
                    </div>
                    <div class="d-flex align-items-center mb-3 mt-3 bg-light rounded p-5">
                        <!--begin::Icon-->
                        <span class="svg-icon svg-icon-success mr-5">
						<span class="svg-icon svg-icon-lg">
							<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Home/Library.svg-->
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"/>
                                    <circle fill="#000000" cx="12" cy="12" r="8"/>
                                </g>
							</svg>
                            <!--end::Svg Icon-->
						</span>
					</span>
                        <!--end::Icon-->
                        <!--begin::Title-->
                        <div class="d-flex flex-column flex-grow-1 mr-2">
                            <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-h2 mb-1">Tache terminer</a>
                        </div>
                        <!--end::Title-->
                        <!--begin::Lable-->
                        <span id="textTaskFinish" class="font-weight-bolder text-success py-1 font-size-h2">7</span>
                        <!--end::Lable-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-custom">
        <div class="card-header card-header-tabs-line">
            <div class="card-title">Gestion de la version</div>
            <div class="card-toolbar">
                <div class="dropdown dropdown-inline">
                    <button type="button" class="btn btn-hover-light-primary btn-icon btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="ki ki-bold-more-hor"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                        <a class="dropdown-item" href="#addTask" data-toggle="modal">Nouvelle tache</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="mb-7">
                <div class="row align-items-center">
                    <div class="col-lg-9 col-xl-8">
                        <div class="row align-items-center">
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="input-icon">
                                    <input type="text" class="form-control" placeholder="Rechercher..." id="search_query_task" />
                                    <span>
												<i class="flaticon2-search-1 text-muted"></i>
											</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="datatable datatable-bordered datatable-head-custom" id="liste_tasks"></div>
        </div>
    </div>


    <div class="modal fade" id="addTask" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ajout d'une tache</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form id="formAddTask" action="{{ route('Version.Task.store', [$version->route_id, $version->id]) }}" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name">Nom de la taches <span class="text-danger">*</span> </label>
                            <input type="text" id="name" class="form-control" name="name" required>
                        </div>
                        <div class="form-group">
                            <label for="lieu">Lieu de la taches </label>
                            <input type="text" id="lieu" class="form-control" name="lieu">
                        </div>
                        <div class="form-group">
                            <label for="priority">Priorité de la tache <span class="text-danger">*</span></label>
                            <select name="priority" class="form-control selectpicker" data-live-search="true" required>
                                <option value=""></option>
                                <option value="0">Basse</option>
                                <option value="1">Moyenne</option>
                                <option value="2">Haute</option>
                                <option value="3">Critique</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="status">Etat actuel de la tache <span class="text-danger">*</span></label>
                            <select name="status" class="form-control selectpicker" data-live-search="true" required>
                                <option value=""></option>
                                <option value="0">En attente</option>
                                <option value="1">En Cours</option>
                                <option value="2">Terminer</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="description">Description <span class="text-danger">*</span></label>
                            <textarea name="description" id="description" cols="30" rows="10" data-provide="markdown" required></textarea>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <div class="input-group date" id="started_at" data-target-input="started_at">
                                    <label for="started_at" class="col-form-label mr-2">Début de la tache <span class="text-danger">*</span></label>
                                    <input type="text" name="started_at" class="form-control datetimepicker-input" placeholder="Select date &amp; time" data-target="#started_at" required/>
                                    <div class="input-group-append" data-target="#started_at" data-toggle="datetimepicker">
										<span class="input-group-text">
											<i class="ki ki-calendar"></i>
										</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group date" id="finished_at" data-target-input="finished_at">
                                    <label for="finished_at" class="col-form-label mr-2">Fin de la tache</label>
                                    <input type="text" name="finished_at" class="form-control datetimepicker-input" placeholder="Select date &amp; time" data-target="#finished_at" />
                                    <div class="input-group-append" data-target="#finished_at" data-toggle="datetimepicker">
										<span class="input-group-text">
											<i class="ki ki-calendar"></i>
										</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="btnAddTask" class="btn btn-primary font-weight-bold">Valider</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="descTask" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Description de la tache</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/route/version.js') }}" type="text/javascript"></script>
@endsection
