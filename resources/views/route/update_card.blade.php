{{-- Extends layout --}}
@extends('layout.default')

@section('styles')
    <style type="text/css">
        .asset_img_class {
            width: 720px !important;
            height: 405px !important;
        }
    </style>
@endsection

{{-- Content --}}
@section('content')
    <div class="card card-custom">
        <form id="formAddCard" action="{{ route('Version.Card.update', [$route_id, $roadmap_id, $version_id, $card_id]) }}" method="post" enctype="multipart/form-data">
            @method("PUT")
            <div class="card-body">
                <div class="form-group">
                    <label for="status">Catégorie <span class="text-danger">*</span></label>
                    <select name="category_id" class="form-control selectpicker" data-live-search="true" required>
                        <option value="{{ $card->category['id'] }}">{{ $card->category['name'] }}</option>
                        @foreach($version->categories as $category)
                            <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Nom de la carte <span class="text-danger">*</span> </label>
                    <input type="text" id="name" class="form-control" name="name" required value="{{ $card->name }}">
                </div>
                <div class="form-group">
                    <label for="status">Etat actuel de la carte <span class="text-danger">*</span></label>
                    <select name="status" class="form-control selectpicker" data-live-search="true" required>
                        <option value="{{ $card->status }}">{{ $card->status }}</option>
                        <option value="Planifié">Planifié</option>
                        <option value="En développement">En développement</option>
                        <option value="Terminer">Terminer</option>
                    </select>
                </div>
                <div class="form group">
                    <label for="description">Description <span class="text-danger">*</span></label>
                    <textarea name="description" id="description" cols="30" rows="10" data-provide="markdown" required>{{ $card->description }}</textarea>
                </div>
                <div class="form-group mt-3">
                    <label for="">Image de la carte</label>
                    <div class="image-input image-input-outline" id="card_img">
                        <div class="image-input-wrapper asset_img_class" style="background-image: url({{ $card->image }})"></div>

                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                            <i class="fa fa-pen icon-sm text-muted"></i>
                            <input type="file" name="card_img" accept=".png"/>
                            <input type="hidden" name="profile_avatar_remove"/>
                        </label>

                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                  <i class="ki ki-bold-close icon-xs text-muted"></i>
                                 </span>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Valider</button>
            </div>
        </form>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/route/update_card.js') }}" type="text/javascript"></script>
@endsection
