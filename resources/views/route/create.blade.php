{{-- Extends layout --}}
@extends('layout.default')

@section("styles")
    <style>
        .asset_img_class {
            width: 720px !important;
            height: 405px !important;
        }
    </style>
@endsection

{{-- Content --}}
@section('content')
<form action="{{ route('Route.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">
                    {{ $page_title }}
                </h3>
            </div>
            <div class="card-toolbar">
                <div class="btn-group">
                    <button type="submit" class="btn btn-primary font-weight-bolder">
                        <i class="ki ki-check icon-xs"></i> Valider
                    </button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="form-group row">
                <label class="col-form-label col-md-3">Nom de la route <span class="text-danger">*</span></label>
                <div class="col-md-9">
                    <input type="text" name="name" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <label for="description">Description <span class="text-danger">*</span></label>
                <textarea name="description" id="description" class="form-control" rows="10" data-provide="markdown"></textarea>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group row">
                        <label for="published" class="col-form-label col-md-2">Publier</label>
                        <div class="col-md-10">
                            <input type="checkbox" id="published" name="published" data-switch="true" data-size="small" data-on-color="success" data-off-color="danger" data-on-text="Publier" data-off-text="Non publier">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group row">
                        <label for="version" class="col-form-label col-md-5">Version de la route</label>
                        <input type="text" name="version" id="version" class="form-control col-md-7">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group row">
                        <label for="build" class="col-form-label col-md-5">Build de la route</label>
                        <input type="text" name="build" id="build" class="form-control col-md-7">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="route_kuid" class="col-form-label col-md-5">Kuid de la route</label>
                        <input type="text" id="route_kuid" name="route_kuid" class="form-control col-md-7">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="dependance_kuid" class="col-form-label col-md-5">Kuid des dépendances</label>
                        <input type="text" id="dependance_kuid" name="dependance_kuid" class="form-control col-md-7">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="asset_image">Image de la route</label>
                <div class="image-input image-input-empty image-input-outline" id="route_img" style="background-image: url({{ asset('media/users/blank.png') }})">
                    <div class="image-input-wrapper asset_img_class"></div>

                    <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                        <i class="fa fa-pen icon-sm text-muted"></i>
                        <input type="file" name="route_img" accept=".png, .jpg, .jpeg"/>
                        <input type="hidden" name="profile_avatar_remove"/>
                    </label>

                    <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                                <i class="ki ki-bold-close icon-xs text-muted"></i>
                                            </span>

                    <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="Remove avatar">
                                                <i class="ki ki-bold-close icon-xs text-muted"></i>
                                            </span>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/route/create.js') }}" type="text/javascript"></script>
@endsection
