{{-- Extends layout --}}
@extends('layout.default')

@section("styles")
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
@endsection

{{-- Content --}}
@section('content')
    <form action="{{ route('Tutoriel.store') }}" method="post" enctype="multipart/form-data">
        <div class="card card-custom card-sticky" id="kt_page_sticky_card">
            <div class="card-header">
                <div class="card-title">
                <span class="card-icon">
                    <i class="fab fa-youtube"></i>
                </span>
                    <h3 class="card-label">
                        Création d'un nouveau tutoriel
                    </h3>
                </div>
                <div class="card-toolbar">
                    <a href="{{ route('Tutoriel.index') }}" class="btn btn-light-primary font-weight-bolder mr-2">
                        <i class="ki ki-long-arrow-back icon-sm"></i>
                        Retour
                    </a>
                    <div class="btn-group">
                        <button type="submit" class="btn btn-primary font-weight-bolder">
                            <i class="ki ki-check icon-sm"></i>
                            Valider
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <h3 class="text-dark font-weight-bold mb-10">Informations générales</h3>
                <div class="form-group">
                    <label for="title">Titre <span class="text-danger">*</span> </label>
                    <input type="text" id="title" name="title" class="form-control" required>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <label for="tutoriel_category_id">Catégorie <span class="text-danger">*</span> </label>
                        <select id="tutoriel_category_id" class="form-control" name="tutoriel_category_id"></select>
                    </div>
                    <div class="col-md-4">
                        <label for="uri">URL </label>
                        <input type="text" id="uri" class="form-control" disabled>
                    </div>
                    <div class="col-md-4">
                        <label for="author">Auteur </label>
                        <input type="text" id="author" class="form-control" value="Trainznation" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label for="date_published">Date de publication </label>
                    <div class="input-group" id="date_published" data-target-input="nearest">
                        <input type="text" title="date_published" name="published_at" class="form-control datetimepicker-input" placeholder="Selectionner la date et l'heure de publication" data-target="#date_published">
                        <div class="input-group-append" data-target="#date_published" data-toggle="datetimepicker">
                            <span class="input-group-text">
                                <i class="ki ki-calendar"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="mb-5">Actions</label>
                    <div class="row">
                        <div class="col-md-4">
                            <span class="switch">
                                <label>
                                    <input data-switch="true" type="checkbox" id="published" name="published" checked="checked" data-on-text="Publier" data-handle-width="50" data-off-text="Non Publier" data-on-color="success" data-off-color="danger"/>
                                    <span></span>
                                    Tutoriel en Ligne
                                </label>
                            </span>
                        </div>
                        <div class="col-md-4">
                            <span class="switch">
                                <label>
                                    <input data-switch="true" type="checkbox" id="sourceSwitch" name="source" checked="checked" data-on-text="Publier" data-handle-width="50" data-off-text="Non Publier" data-on-color="success" data-off-color="danger"/>
                                    <span></span>
                                    Ajouter Source
                                </label>
                            </span>
                        </div>
                        <div class="col-md-4">
                            <span class="switch">
                                <label>
                                    <input data-switch="true" type="checkbox" id="premium" name="premium" checked="checked" data-on-text="Publier" data-handle-width="50" data-off-text="Non Publier" data-on-color="success" data-off-color="danger"/>
                                    <span></span>
                                    Tutoriel Premium
                                </label>
                            </span>
                        </div>
                    </div>
                </div>
                <h3 class="text-dark font-weight-bold mt-10 mb-10">Contenus</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Image du tutoriel</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="img_file" name="img_file" accept=".png">
                                <label class="custom-file-label" for="customFile">Choisir l'image</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Vidéo du tutoriel</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="video_file" name="video_file" accept=".mp4">
                                <label class="custom-file-label" for="customFile">Choisir la vidéo</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="youtube_id">Youtube</label>
                            <input type="text" id="youtube_id" class="form-control" disabled>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6" id="div_source_field">
                                <label for="">Source</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="sourceUpload" name="source_file" accept=".zip">
                                    <label class="custom-file-label" for="customFile">Choisir la source</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="">Durée</label>
                                <input type="text" id="duree" class="form-control" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div  class="mb-10"><img id="img_view" src="https://via.placeholder.com/768x420" class="img-fluid" alt/> </div>
                    </div>
                </div>
                <h3 class="text-dark font-weight-bold mt-10 mb-10">Description</h3>
                <div class="form-group">
                    <label for="short_content">Courte description</label>
                    <textarea name="short_content" id="short_content" rows="10" class="form-control" data-provide="markdown"></textarea>
                </div>
                <div class="form-group">
                    <label for="content" class="mb-5">Contenus</label> <button id="copycontent" class="btn btn-primary btn-sm"><i class="far fa-copy"></i> Copier le contenue</button>
                    <textarea name="content" id="content" rows="10" class="form-control" data-provide="markdown"></textarea>
                </div>
            </div>
        </div>
    </form>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/tutoriel/create.js') }}" type="text/javascript"></script>
@endsection
