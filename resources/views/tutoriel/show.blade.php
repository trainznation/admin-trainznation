{{-- Extends layout --}}
@extends('layout.default')

@section("styles")
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
@endsection

@section('page_toolbars')
    <a href="{{ route('Tutoriel.edit', $tutoriel->id) }}" class="btn btn-icon" data-toggle="tooltip" title="Editer le tutoriel"><i class="fa fa-edit text-primary"></i> </a>
    <a href="{{ route('Tutoriel.delete', $tutoriel->id) }}" id="btnTrash" class="btn btn-icon" data-toggle="tooltip" title="Supprimer le tutoriel"><i class="fa fa-trash text-danger"></i> </a>
    @if($tutoriel->published == 1)
        <a href="{{ route('Tutoriel.dispublish', $tutoriel->id) }}" class="btn btn-icon" data-toggle="tooltip" title="Dépublier le tutoriel"><i class="fa fa-times text-danger"></i> </a>
    @else
        <a href="{{ route('Tutoriel.publish', $tutoriel->id) }}" class="btn btn-icon" data-toggle="tooltip" title="Publier le tutoriel"><i class="fa fa-check text-success"></i> </a>
    @endif
@endsection

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-4">
        <div class="card card-custom overlay mb-3">
            <div class="card-body p-0">
                <div class="overlay-wrapper">
                    <img src="{{ $tutoriel->image }}" alt="" class="w-100 rounded"/>
                </div>
                <div class="overlay-layer align-items-end justify-content-center">
                    <div class="d-flex flex-grow-1 flex-center bg-white-o-5 py-5">
                        <a href="{{ route('Tutoriel.edit', $tutoriel->id) }}" class="btn font-weight-bold btn-light-primary btn-shadow ml-2">Editer</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-custom gutter-b">
            <div class="card-body pt-4">
                <div class="pt-8 pb-6">
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">Social:</span>
                        <span>
                            @if($tutoriel->social == 0)
                                <i class="socicon-facebook text-primary"></i>
                                <i class="socicon-twitter text-primary"></i>
                            @else
                                <i class="socicon-facebook"></i>
                                <i class="socicon-twitter"></i>
                            @endif
                        </span>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">Premium:</span>
                        <span>
                            @if($tutoriel->premium == 0)
                                <i class="fas fa-certificate"></i>
                            @else
                                <i class="fas fa-certificate text-success"></i>
                            @endif
                        </span>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">Publier:</span>
                        <span>
                            @if($tutoriel->published == 0)
                                <i class="fa fa-times-circle text-danger" data-toggle="tooltip" title="Non Publier"></i>
                            @else
                                <i class="fa fa-check-circle text-success" data-toggle="tooltip" title="{{ ($tutoriel->published_at->format <= now()->subDay()) ? 'Publier le '.$tutoriel->published_at->normalize : 'Publier il y a '.$tutoriel->published_at->human }}"></i>
                            @endif
                        </span>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">Source:</span>
                        <span>
                            @if(\Illuminate\Support\Facades\Storage::disk(env('FILESYSTEM_DRIVER'))->exists('v3/tutoriel/'.$tutoriel->id.'/source.zip') == false)
                                <i class="fa fa-times-circle text-danger" data-toggle="tooltip" title="Non disponible"></i>
                            @else
                                <i class="fa fa-check-circle text-success"></i>
                            @endif
                        </span>
                    </div>
                </div>
                <div class="pb-6">
                    {{ $tutoriel->short_content }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card card-custom gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
						<i class="flaticon2-line-chart text-primary"></i>
					</span>
                    <h3 class="card-label">
                        Contenus
                    </h3>
                </div>
            </div>
            <div class="card-body">
                <div id="markedDown" data-content="{{ $tutoriel->content }}" class="mb-lg-8"></div>
                @if($tutoriel->video)
                    <video id="video" class="video-js"
                           controls
                           preload="auto"
                           width="640"
                           height="264"
                           poster="{{ $tutoriel->image }}"
                           data-setup=''>
                        <source src="{{ $tutoriel->video }}" type="video/mp4" />
                    </video>
                @else
                    <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                        <div class="alert-icon">
                            <i class="fas fa-video-slash"></i>
                        </div>
                        <div class="alert-text">Aucune vidéo de définie pour ce tutoriel <span class="text-right"><button class="btn btn-sm btn-primary text-right">Upload de video</button></span> </div>
                        <div class="alert-close">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">
									<i class="ki ki-close"></i>
								</span>
                            </button>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/youtube.js') }}"></script>
    <script src="{{ asset('js/scripts/tutoriel/show.js') }}" type="text/javascript"></script>
@endsection
