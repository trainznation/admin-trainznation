{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Gestion des priorité
                </h3>
            </div>
            <div class="card-toolbar">
                <a href="#addPriority" data-toggle="modal" class="btn btn-primary font-weight-bolder mr-3">
                    <i class="fa fa-plus-circle"></i> Nouvelle priorité
                </a>
                <button id="refreshTable" class="btn btn-icon btn-primary" data-toggle="tooltip" title="Acualisée le tableau"><i class="flaticon-refresh"></i> </button>
            </div>
        </div>
        <div class="card-body">
            <!--begin: Search Form-->
            <!--begin::Search Form-->
            <div class="mb-7">
                <div class="row align-items-center">
                    <div class="col-lg-9 col-xl-8">
                        <div class="row align-items-center">
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="input-icon">
                                    <input type="text" class="form-control" placeholder="Recherche..." id="search_liste_priority"/>
                                    <span>
                                        <i class="flaticon2-search-1 text-muted"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Search Form-->
            <!--end: Search Form-->
            <!--begin: Datatable-->
            <table class="datatable datatable-bordered datatable-head-custom" id="listePriority">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Designation</th>
                        <th>Couleur</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($priorities as $priority)
                        <tr>
                            <td>{{ $priority->id }}</td>
                            <td>{{ $priority->designation }}</td>
                            <td><i class="flaticon-squares text-{{ $priority->color }}"></i> {{ $priority->color }}</td>
                            <td>
                                <a href="{{ route('Support.Priorite.get', $priority->id) }}" class="btn btn-icon btn-primary btn-pill btn-edit" data-toggle="modal" data-target="editPriority"><i class="fas fa-edit" data-toggle="tooltip" title="Editer la priorité"></i> </a>
                                <a href="{{ route('Support.Priorite.delete', $priority->id) }}" class="btn btn-icon btn-danger btn-pill btn-trash"><i class="fas fa-trash" data-toggle="tooltip" title="Supprimer la priorité"></i> </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <div class="modal fade" id="addPriority" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nouvelle priorité</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form id="formAddPriority" action="{{ route('Support.Priorite.store') }}" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Couleur <span class="text-danger">*</span> </label>
                            <select class="form-control selectpicker" data-provider="selectpicker" name="color" data-live-search="true" required>
                                <option value=""></option>
                                <option data-content="<i class='flaticon-square text-primary'></i> primary" value="primary"></option>
                                <option data-content="<i class='flaticon-square text-secondary'></i> secondary" value="secondary"></option>
                                <option data-content="<i class='flaticon-square text-success'></i> success" value="success"></option>
                                <option data-content="<i class='flaticon-square text-danger'></i> danger" value="danger"></option>
                                <option data-content="<i class='flaticon-square text-warning'></i> warning" value="warning"></option>
                                <option data-content="<i class='flaticon-square text-info'></i> info" value="info"></option>
                                <option data-content="<i class='flaticon-square text-light'></i> light" value="light"></option>
                                <option data-content="<i class='flaticon-square text-dark'></i> dark" value="dark"></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Intitulé de la priorité <span class="text-danger">*</span> </label>
                            <input type="text" class="form-control" name="designation" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary font-weight-bold btn-submit">Valider</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editPriority" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Editer la priorité</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form id="formEditPriority" action="" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Couleur <span class="text-danger">*</span> </label>
                            <select class="form-control selectpicker" data-provider="selectpicker" name="color" data-live-search="true" required>
                                <option value=""></option>
                                <option data-content="<i class='flaticon-square text-primary'></i> primary" value="primary"></option>
                                <option data-content="<i class='flaticon-square text-secondary'></i> secondary" value="secondary"></option>
                                <option data-content="<i class='flaticon-square text-success'></i> success" value="success"></option>
                                <option data-content="<i class='flaticon-square text-danger'></i> danger" value="danger"></option>
                                <option data-content="<i class='flaticon-square text-warning'></i> warning" value="warning"></option>
                                <option data-content="<i class='flaticon-square text-info'></i> info" value="info"></option>
                                <option data-content="<i class='flaticon-square text-light'></i> light" value="light"></option>
                                <option data-content="<i class='flaticon-square text-dark'></i> dark" value="dark"></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Intitulé de la priorité <span class="text-danger">*</span> </label>
                            <input type="text" id="name_priority" class="form-control" name="designation" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary font-weight-bold btn-submit">Valider</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/settings/support/priority/index.js') }}" type="text/javascript"></script>
@endsection
