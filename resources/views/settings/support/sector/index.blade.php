{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Gestion des secteurs
                </h3>
            </div>
            <div class="card-toolbar">
                <a href="#addSector" data-toggle="modal" class="btn btn-primary font-weight-bolder mr-3">
                    <i class="fa fa-plus-circle"></i> Nouveau secteur
                </a>
                <button id="refreshTable" class="btn btn-icon btn-primary" data-toggle="tooltip" title="Acualisée le tableau"><i class="flaticon-refresh"></i> </button>
            </div>
        </div>
        <div class="card-body">
            <!--begin: Search Form-->
            <!--begin::Search Form-->
            <div class="mb-7">
                <div class="row align-items-center">
                    <div class="col-lg-9 col-xl-8">
                        <div class="row align-items-center">
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="input-icon">
                                    <input type="text" class="form-control" placeholder="Recherche..." id="search_liste_sector"/>
                                    <span>
                                        <i class="flaticon2-search-1 text-muted"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Search Form-->
            <!--end: Search Form-->
            <!--begin: Datatable-->
            <table class="datatable datatable-bordered datatable-head-custom" id="listeSector">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nom</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sectors as $sector)
                        <tr>
                            <td>{{ $sector->id }}</td>
                            <td>{{ $sector->name }}<br><i class="text-muted h6">Catégorie: {{ $sector->category->name }}</i></td>
                            <td>
                                <a href="{{ route('Support.Sector.get', $sector->id) }}" class="btn btn-icon btn-primary btn-pill btn-edit" data-toggle="modal" data-target="editSector"><i class="fas fa-edit" data-toggle="tooltip" title="Editer le secteur"></i> </a>
                                <a href="{{ route('Support.Sector.delete', $sector->id) }}" class="btn btn-icon btn-danger btn-pill btn-trash"><i class="fas fa-trash" data-toggle="tooltip" title="Supprimer le secteur"></i> </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <div class="modal fade" id="addSector" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nouveau secteur</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form id="formAddSector" action="{{ route('Support.Sector.store') }}" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Catégorie <span class="text-danger">*</span> </label>
                            <select class="form-control selectpicker" data-provider="selectpicker" name="support_category_id" data-live-search="true" required>
                                <option value=""></option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nom du secteur <span class="text-danger">*</span> </label>
                            <input type="text" class="form-control" name="name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary font-weight-bold btn-submit">Valider</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editSector" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Editer le secteur</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form id="formEditSector" action="" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Catégorie <span class="text-danger">*</span> </label>
                            <select class="form-control selectpicker" id="select_category" data-provider="selectpicker" name="support_category_id" data-live-search="true" required>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nom du secteur <span class="text-danger">*</span> </label>
                            <input type="text" id="name_sector" class="form-control" name="name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary font-weight-bold btn-submit">Valider</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/settings/support/sector/index.js') }}" type="text/javascript"></script>
@endsection
