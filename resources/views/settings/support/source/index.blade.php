{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Gestion des sources
                </h3>
            </div>
            <div class="card-toolbar">
                <a href="#addSource" data-toggle="modal" class="btn btn-primary font-weight-bolder mr-3">
                    <i class="fa fa-plus-circle"></i> Nouvelle source
                </a>
                <button id="refreshTable" class="btn btn-icon btn-primary" data-toggle="tooltip" title="Acualisée le tableau"><i class="flaticon-refresh"></i> </button>
            </div>
        </div>
        <div class="card-body">
            <!--begin: Search Form-->
            <!--begin::Search Form-->
            <div class="mb-7">
                <div class="row align-items-center">
                    <div class="col-lg-9 col-xl-8">
                        <div class="row align-items-center">
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="input-icon">
                                    <input type="text" class="form-control" placeholder="Recherche..." id="search_liste_source"/>
                                    <span>
                                        <i class="flaticon2-search-1 text-muted"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Search Form-->
            <!--end: Search Form-->
            <!--begin: Datatable-->
            <table class="datatable datatable-bordered datatable-head-custom" id="listeSource">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nom</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sources as $source)
                        <tr>
                            <td>{{ $source->id }}</td>
                            <td>{{ $source->designation }}</td>
                            <td>
                                <a href="{{ route('Support.Source.get', $source->id) }}" class="btn btn-icon btn-primary btn-pill btn-edit" data-toggle="modal" data-target="editSource"><i class="fas fa-edit" data-toggle="tooltip" title="Editer la source"></i> </a>
                                <a href="{{ route('Support.Source.delete', $source->id) }}" class="btn btn-icon btn-danger btn-pill btn-trash"><i class="fas fa-trash" data-toggle="tooltip" title="Supprimer la source"></i> </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <div class="modal fade" id="addSource" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nouvelle source</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form id="formAddSource" action="{{ route('Support.Source.store') }}" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nom de la source <span class="text-danger">*</span> </label>
                            <input type="text" class="form-control" name="designation" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary font-weight-bold btn-submit">Valider</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editSource" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Editer la source</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form id="formEditSource" action="" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nom du secteur <span class="text-danger">*</span> </label>
                            <input type="text" id="name_source" class="form-control" name="designation" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary font-weight-bold btn-submit">Valider</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/settings/support/source/index.js') }}" type="text/javascript"></script>
@endsection
