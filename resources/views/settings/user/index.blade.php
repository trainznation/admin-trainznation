{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">{{ $page_title }}
                </h3>
            </div>
        </div>
        <div class="card-body">
            <!--begin: Search Form-->
            <!--begin::Search Form-->
            <div class="mb-7">
                <div class="row align-items-center">
                    <div class="col-lg-9 col-xl-8">
                        <div class="row align-items-center">
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="input-icon">
                                    <input type="text" class="form-control" placeholder="Recherche..." id="search_liste_user"/>
                                    <span>
                                        <i class="flaticon2-search-1 text-muted"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                        <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Recherche</a>
                    </div>
                </div>
            </div>
            <!--end::Search Form-->
            <!--end: Search Form-->
            <!--begin: Datatable-->
            <table class="datatable datatable-bordered datatable-head-custom" id="listeUser">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Identité</th>
                        <th>Groupe</th>
                        <th>Etat</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>
                                <div class="d-flex align-items-center mb-10">
                                    <!--begin::Symbol-->
                                    <div class="symbol symbol-40 symbol-circle symbol-light-white mr-5">
                                        <div class="symbol-label">
                                            <img src="{{ $user->avatar->encoded }}" class="h-75 align-self-end" alt="">
                                        </div>
                                    </div>
                                    <!--end::Symbol-->
                                    <!--begin::Text-->
                                    <div class="d-flex flex-column font-weight-bold">
                                        <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">{{ $user->name }}</a>
                                        <span class="text-muted">{{ $user->email }}</span>
                                    </div>
                                    <!--end::Text-->
                                </div>
                            </td>
                            <td>
                                {{ $user->admin }}
                            </td>
                            <td>{{ $user->account->premium }}</td>
                            <td>
                                <a href="{{ route('Settings.User.show', $user->id) }}" class="btn btn-icon btn-info" data-toggle="tooltip" title="Voir la fiche"><i class="fas fa-eye"></i> </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/settings/user/index.js') }}" type="text/javascript"></script>
@endsection
