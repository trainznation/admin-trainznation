{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Gestion des Annonces
                </h3>
            </div>
            <div class="card-toolbar">
                <a href="#addAnnonce" data-toggle="modal" class="btn btn-primary font-weight-bolder">
                    <i class="fa fa-plus-circle"></i> Nouvelle Annonce
                </a>
            </div>
        </div>
        <div class="card-body">
            <!--begin: Search Form-->
            <!--begin::Search Form-->
            <div class="mb-7">
                <div class="row align-items-center">
                    <div class="col-lg-9 col-xl-8">
                        <div class="row align-items-center">
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="input-icon">
                                    <input type="text" class="form-control" placeholder="Recherche..." id="search_liste_annonce"/>
                                    <span>
                                        <i class="flaticon2-search-1 text-muted"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                        <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Recherche</a>
                    </div>
                </div>
            </div>
            <!--end::Search Form-->
            <!--end: Search Form-->
            <!--begin: Datatable-->
            <table class="datatable datatable-bordered datatable-head-custom" id="listeAnnonce"></table>
            <!--end: Datatable-->
        </div>
    </div>
    <div class="modal fade" id="addAnnonce" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nouvelle Annonce</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form id="formAddAnnonce" action="" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="message">Message <span class="text-danger">*</span> </label>
                            <textarea name="message" id="message" class="form-control" rows="5" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="message">Type <span class="text-danger">*</span> </label>
                            <select name="type" id="type" class="form-control selectpicker" required>
                                <option value=""></option>
                                <option value="info" data-content="<i class='fas fa-square text-info'></i> info"></option>
                                <option value="warning" data-content="<i class='fas fa-square text-warning'></i> warning"></option>
                                <option value="success" data-content="<i class='fas fa-square text-success'></i> success"></option>
                                <option value="danger" data-content="<i class='fas fa-square text-danger'></i> danger"></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="expiring_at">Date d'expiration <span class="text-danger">*</span> </label>
                            <input type="text" id="expiring_at" name="expiring_at" class="form-control datetimepicker-input" data-toggle="datetimepicker" data-target="#expiring_at" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary font-weight-bold btn-submit">Valider</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/scripts/settings/annonce/index.js') }}" type="text/javascript"></script>
@endsection
