$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function login() {
    $("#formLogin").on('submit', (e) => {
        e.preventDefault();
        let form = $("#formLogin")
        let btn = document.querySelector('#kt_login_singin_form_submit_button')
        let uri = form.attr('action')
        let data = form.serializeArray()

        KTUtil.btnWait(btn, "spinner spinner-right spinner-white pr-15", "Veuillez patienter...")

        $.ajax({
            url: uri,
            method: "POST",
            data: data,
            statusCode: {
                200: data => {
                    KTUtil.btnRelease(btn)
                    window.location.href='/'
                },
                401: data => {
                    KTUtil.btnRelease(btn)
                    toastr.error(data.responseJSON.error, "Erreur")
                },
                422: data => {
                    KTUtil.btnRelease(btn)
                    toastr.warning(data.responseJSON.error, "Attention")
                }
            }
        })
    });
}

login();
