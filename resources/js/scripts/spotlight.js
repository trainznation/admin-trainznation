class Spotlight extends HTMLElement {

    /**
     * @property {HTMLInputElement} input
     * @property {spotlightItem[]} items
     */
    constructor() {
        super();
        this.shortcutHandler = this.shortcutHandler.bind(this)
        this.hide = this.hide.bind(this)
        this.onInput = this.onInput.bind(this)
    }

    connectedCallback () {
        this.classList.add('spotlight')
        this.innerHTML = `
            <div class="spotlight-bar">
                <input type="text">
                <ul class="spotlight-suggestions">
                </ul>
            </div>
        `
        this.input = this.querySelector('input')
        this.input.addEventListener('blur', this.hide)
        const suggestions = this.querySelector('.spotlight-suggestions')

        this.items = Array.from(document.querySelectorAll(this.getAttribute('target'))).map(a => {
            const title = a.innerText.trim()

            if(title === '') return;

            const item = new SpotlightItem(title, a.getAttribute('href'))
            suggestions.appendChild(item.element)
            return item
        }).filter(i => i !== null)


        window.addEventListener('keydown', this.shortcutHandler)
        this.input.addEventListener('input', this.onInput)
    }

    disconnectedCallback() {
        window.removeEventListener('keydown', this.shortcutHandler)
    }

    shortcutHandler(e) {
        if (e.key === 'k' && e.ctrlKey === true) {
            e.preventDefault();
            this.classList.add('active')
            this.input.focus()
        }

        if(e.key === 'Escape' && document.activeElement === this.input) {
            this.input.blur()
        }
    }

    hide() {
        this.classList.remove('active')
    }

    /**
     *
     * @param {InputEvent} e
     */

    onInput(e) {
        const search = e.target.value
        let regexp = '^(.*)'
        for(const i in search) {
            regexp += `(${search[i]})(.*)`
        }
        regexp += '$'

        regexp = new RegExp(regexp, 'i')
        this.items.forEach(item => item.match(regexp))
    }
}

/**
 * @property {HTMLLIElement} element
 * @property {string} title
 */

class SpotlightItem {

    /**
     *
     * @param {string} title
     * @param {string} href
     */

    constructor(title, href) {
        const li = document.createElement('li')
        const a = document.createElement('a')
        a.setAttribute('href', href)
        a.innerText = title
        li.appendChild(a)

        li.setAttribute('hidden', 'hidden')
        this.element = li
        this.title = title
    }

    /**
     *
     * @param {RegExp} regexp
     * @return {boolean}
     */
    match(regexp) {
        const matches = this.title.match(regexp)
        if(matches === null) {
            this.element.setAttribute('hidden', 'hidden')
            return false
        }
        this.element.firstElementChild.innerHTML = matches.reduce((acc,match, index) => {
            if(index === 0) {
                return acc;
            }

            return acc + (index % 2 === 0 ? `<mark>${match}</mark>` : match)
        }, '')
        this.element.removeAttribute('hidden')
        return true
    }
}

customElements.define('spotlight-bar', Spotlight)
