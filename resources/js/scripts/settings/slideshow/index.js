let table;

new KTImageInput('kt_image_5');

function loadListSlide() {
    table = $("#listeSlide").KTDatatable({
        data: {
            saveState: {cookie: false},
        },
        search: {
            input: $('#search_liste_slide'),
            key: 'generalSearch'
        },
        columns: [
            {
                field: "ID"
            },
            {
                field: "Image"
            },
            {
                field: "Actions"
            },
        ]
    })
}

loadListSlide()
