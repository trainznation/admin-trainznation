let table;

function loadListUser() {
    table = $("#listeUser").KTDatatable({
        data: {
            saveState: {cookie: false},
        },
        search: {
            input: $('#search_liste_user'),
            key: 'generalSearch'
        },
        columns: [
            {
                field: "ID",
                width: 80
            },
            {
                field: "Identité",
            },
            {
                field: "Groupe",
                template: (row) => {
                    let group = {
                        0: {"class": "primary", "text": "Utilisateur"},
                        1: {"class": "danger", "text": "Administrateur"},
                    }

                    return `<span class="label label-inline label-${group[row.Groupe].class}">${group[row.Groupe].text}</span>`
                }
            },
            {
                field: "Etat",
                template: (row) => {
                    let premium = {
                        0: {"class": "primary", "text": "Compte Normal"},
                        1: {"class": "success", "text": "Compte Premium"},
                    }

                    return `<span class="label label-inline label-${premium[row.Etat].class}">${premium[row.Etat].text}</span>`
                }
            },
            {
                field: "Actions"
            }
        ]
    })
}

loadListUser()
