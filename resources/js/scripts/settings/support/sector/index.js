const spinning = 'spinner spinner-right spinner-white pr-15'
let dataTable;

dataTable = $("#listeSector").KTDatatable({
    data: {
        saveState: {cookie: false},
    },
    search: {
        input: $("#search_liste_sector"),
        key: 'generalSearch'
    },
    columns: [
        {
            field: 'Id'
        },
        {
            field: 'name',
            title: 'Nom',
        },
        {
            field: 'Actions',
            title: 'Actions'
        }
    ]
});

let btn = document.querySelector('#refreshTable').addEventListener('click', (e) => {
    e.preventDefault()
    KTUtil.btnWait(btn, spinning)
    dataTable.reload()
    $("#listeSector").on('datatable-on-reloaded', (e) => {
        KTUtil.btnRelease(btn)
    })
})

$("#formAddSector").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formAddSector")
    let btn = document.querySelector('.btn-submit')
    let url = form.attr('action')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning)

    $.ajax({
        url: url,
        method: 'POST',
        data: data,
        success: (data) => {
            KTUtil.btnRelease(btn)
            toastr.success("Le secteur à été ajouté")
            $("#addSector").on('hidden.bs.modal', (e) => {
                window.location.reload()
            })
        },
        error: (err) => {
            console.error(error)
            KTUtil.btnRelease(btn)
            toastr.error('Erreur de script, consulter les logs', 'Erreur Serveur')
        }
    })
})
$("#formEditSector").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formEditSector")
    let url = form.attr('action')
    let btn = document.querySelector('.btn-submit')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning)

    $.ajax({
        url: url,
        method: 'PUT',
        data: data,
        success: (data) => {
            KTUtil.btnRelease(btn)
            toastr.success("Le secteur à été modifié")
            $("#editSector").modal('hide')
        },
        error: (err) => {
            KTUtil.btnRelease(btn)
            toastr.error('Erreur Systeme, consulter les logs')
            toastr.error(err)
        }
    })
})

dataTable.on('click', '.btn-edit', (e) => {
    e.preventDefault()
    $.ajax({
        url: e.currentTarget.href,
        success: (data) => {

            document.querySelector('#select_category').innerHTML = `<option value="${data.category.id}">${data.category.name}</option>`
            $("#formEditSector").attr('action', e.currentTarget.href)
            $("#name_sector").val(data.name)
            $("#editSector").modal('show')
        },
        error: (err) => {
            console.log(err)
            toastr.error("Erreur de script, consulter les logs", "Erreur Système")
        }
    })
})
dataTable.on('click', '.btn-trash', (e) => {
    e.preventDefault()
    $.ajax({
        url: e.currentTarget.href,
        method: 'DELETE',
        success: () => {
            e.currentTarget.parentNode.parentNode.parentNode.style.display = 'none'
            toastr.success("Le secteur à été supprimer")
        },
        error: (err) => {
            console.log(err)
            toastr.error("Erreur de script, consulter les logs", "Erreur Système")
        }
    })
})
