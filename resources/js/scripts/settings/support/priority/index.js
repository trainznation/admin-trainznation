const spinning = 'spinner spinner-right spinner-white pr-15'
let dataTable;

dataTable = $("#listePriority").KTDatatable({
    data: {
        saveState: {cookie: false},
    },
    search: {
        input: $("#search_liste_priority"),
        key: 'generalSearch'
    },
    columns: [
        {
            field: 'Id',
            width: 25
        },
        {
            field: 'designation',
            title: 'Designation',
        },
        {
            field: 'couleur',
            title: 'Couleur',
        },
        {
            field: 'Actions',
            title: 'Actions'
        }
    ]
});

let btn = document.querySelector('#refreshTable').addEventListener('click', (e) => {
    e.preventDefault()
    KTUtil.btnWait(btn, spinning)
    dataTable.reload()
    $("#listePriority").on('datatable-on-reloaded', (e) => {
        KTUtil.btnRelease(btn)
    })
})

$("#formAddPriority").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formAddPriority")
    let btn = document.querySelector('.btn-submit')
    let url = form.attr('action')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning)

    $.ajax({
        url: url,
        method: 'POST',
        data: data,
        success: (data) => {
            KTUtil.btnRelease(btn)
            toastr.success("La priorité à été ajouté")
            $("#addSector").on('hidden.bs.modal', (e) => {
                window.location.reload()
            })
        },
        error: (err) => {
            console.error(error)
            KTUtil.btnRelease(btn)
            toastr.error('Erreur de script, consulter les logs', 'Erreur Serveur')
        }
    })
})
$("#formEditPriority").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formEditPriority")
    let url = form.attr('action')
    let btn = document.querySelector('.btn-submit')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning)

    $.ajax({
        url: url,
        method: 'PUT',
        data: data,
        success: (data) => {
            KTUtil.btnRelease(btn)
            toastr.success("La priorité à été modifié")
            $("#editSector").modal('hide')
        },
        error: (err) => {
            KTUtil.btnRelease(btn)
            toastr.error('Erreur Systeme, consulter les logs')
            toastr.error(err)
        }
    })
})

dataTable.on('click', '.btn-edit', (e) => {
    e.preventDefault()
    $.ajax({
        url: e.currentTarget.href,
        success: (data) => {
            $("#formEditPriority").attr('action', e.currentTarget.href)
            $("#name_priority").val(data.designation)
            $("#editPriority").modal('show')
        },
        error: (err) => {
            console.log(err)
            toastr.error("Erreur de script, consulter les logs", "Erreur Système")
        }
    })
})
dataTable.on('click', '.btn-trash', (e) => {
    e.preventDefault()
    $.ajax({
        url: e.currentTarget.href,
        method: 'DELETE',
        success: () => {
            e.currentTarget.parentNode.parentNode.parentNode.style.display = 'none'
            toastr.success("La priorité à été supprimer")
        },
        error: (err) => {
            console.log(err)
            toastr.error("Erreur de script, consulter les logs", "Erreur Système")
        }
    })
})
