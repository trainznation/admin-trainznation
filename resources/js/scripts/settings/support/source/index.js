const spinning = 'spinner spinner-right spinner-white pr-15'
let dataTable;

dataTable = $("#listeSource").KTDatatable({
    data: {
        saveState: {cookie: false},
    },
    search: {
        input: $("#search_liste_source"),
        key: 'generalSearch'
    },
    columns: [
        {
            field: 'Id'
        },
        {
            field: 'designation',
            title: 'Designation',
        },
        {
            field: 'Actions',
            title: 'Actions'
        }
    ]
});

let btn = document.querySelector('#refreshTable').addEventListener('click', (e) => {
    e.preventDefault()
    KTUtil.btnWait(btn, spinning)
    dataTable.reload()
    $("#listeSource").on('datatable-on-reloaded', (e) => {
        KTUtil.btnRelease(btn)
    })
})

$("#formAddSource").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formAddSource")
    let btn = document.querySelector('.btn-submit')
    let url = form.attr('action')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning)

    $.ajax({
        url: url,
        method: 'POST',
        data: data,
        success: (data) => {
            KTUtil.btnRelease(btn)
            toastr.success("La source à été ajouté")
            $("#addSource").on('hidden.bs.modal', (e) => {
                window.location.reload()
            })
        },
        error: (err) => {
            console.error(error)
            KTUtil.btnRelease(btn)
            toastr.error('Erreur de script, consulter les logs', 'Erreur Serveur')
        }
    })
})
$("#formEditSource").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formEditSource")
    let url = form.attr('action')
    let btn = document.querySelector('.btn-submit')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning)

    $.ajax({
        url: url,
        method: 'PUT',
        data: data,
        success: (data) => {
            KTUtil.btnRelease(btn)
            toastr.success("La source à été modifié")
            $("#editSource").modal('hide')
        },
        error: (err) => {
            KTUtil.btnRelease(btn)
            toastr.error('Erreur Systeme, consulter les logs')
            toastr.error(err)
        }
    })
})

dataTable.on('click', '.btn-edit', (e) => {
    e.preventDefault()
    $.ajax({
        url: e.currentTarget.href,
        success: (data) => {
            $("#formEditSource").attr('action', e.currentTarget.href)
            $("#name_source").val(data.designation)
            $("#editSource").modal('show')
        },
        error: (err) => {
            console.log(err)
            toastr.error("Erreur de script, consulter les logs", "Erreur Système")
        }
    })
})
dataTable.on('click', '.btn-trash', (e) => {
    e.preventDefault()
    $.ajax({
        url: e.currentTarget.href,
        method: 'DELETE',
        success: () => {
            e.currentTarget.parentNode.parentNode.parentNode.style.display = 'none'
            toastr.success("La source à été supprimer")
        },
        error: (err) => {
            console.log(err)
            toastr.error("Erreur de script, consulter les logs", "Erreur Système")
        }
    })
})
