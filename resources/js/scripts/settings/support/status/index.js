const spinning = 'spinner spinner-right spinner-white pr-15'
let dataTable;

dataTable = $("#listeStatus").KTDatatable({
    data: {
        saveState: {cookie: false},
    },
    search: {
        input: $("#search_liste_status"),
        key: 'generalSearch'
    },
    columns: [
        {
            field: 'Id',
            width:25
        },
        {
            field: 'designation',
            title: 'Designation',
        },
        {
            field: 'couleur',
            title: 'Couleur',
        },
        {
            field: 'icon',
            title: 'Icone',
        },
        {
            field: 'Actions',
            title: 'Actions'
        }
    ]
});

let btn = document.querySelector('#refreshTable').addEventListener('click', (e) => {
    e.preventDefault()
    KTUtil.btnWait(btn, spinning)
    dataTable.reload()
    $("#listeStatus").on('datatable-on-reloaded', (e) => {
        KTUtil.btnRelease(btn)
    })
})

$("#formAddStatus").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formAddStatus")
    let btn = document.querySelector('.btn-submit')
    let url = form.attr('action')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning)

    $.ajax({
        url: url,
        method: 'POST',
        data: data,
        success: (data) => {
            KTUtil.btnRelease(btn)
            toastr.success("Le status à été ajouté")
            $("#addStatus").on('hidden.bs.modal', (e) => {
                window.location.reload()
            })
        },
        error: (err) => {
            console.error(error)
            KTUtil.btnRelease(btn)
            toastr.error('Erreur de script, consulter les logs', 'Erreur Serveur')
        }
    })
})
$("#formEditStatus").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formEditStatus")
    let url = form.attr('action')
    let btn = document.querySelector('.btn-submit')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning)

    $.ajax({
        url: url,
        method: 'PUT',
        data: data,
        success: (data) => {
            KTUtil.btnRelease(btn)
            toastr.success("Le status à été modifié")
            $("#editSource").modal('hide')
        },
        error: (err) => {
            KTUtil.btnRelease(btn)
            toastr.error('Erreur Systeme, consulter les logs')
            toastr.error(err)
        }
    })
})

dataTable.on('click', '.btn-edit', (e) => {
    e.preventDefault()
    $.ajax({
        url: e.currentTarget.href,
        success: (data) => {
            $("#formEditStatus").attr('action', e.currentTarget.href)
            $("#name_status").val(data.designation)
            $("#icon_status").val(data.icon)
            $("#editStatus").modal('show')
        },
        error: (err) => {
            console.log(err)
            toastr.error("Erreur de script, consulter les logs", "Erreur Système")
        }
    })
})
dataTable.on('click', '.btn-trash', (e) => {
    e.preventDefault()
    $.ajax({
        url: e.currentTarget.href,
        method: 'DELETE',
        success: () => {
            e.currentTarget.parentNode.parentNode.parentNode.style.display = 'none'
            toastr.success("Le status à été supprimer")
        },
        error: (err) => {
            console.log(err)
            toastr.error("Erreur de script, consulter les logs", "Erreur Système")
        }
    })
})
