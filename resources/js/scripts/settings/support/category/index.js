const spinning = 'spinner spinner-right spinner-white pr-15'
let dataTable;

dataTable = $("#listeCategory").KTDatatable({
    data: {
        saveState: {cookie: false},
    },
    search: {
        input: $("#search_liste_category"),
        key: 'generalSearch'
    },
    columns: [
        {
            field: 'Id'
        },
        {
            field: 'name',
            title: 'Nom',
        },
        {
            field: 'Actions',
            title: 'Actions'
        }
    ]
});

$("#formAddCategory").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formAddCategory")
    let btn = document.querySelector('.btn-submit')
    let url = form.attr('action')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning)

    $.ajax({
        url: url,
        method: 'POST',
        data: data,
        success: (data) => {
            KTUtil.btnRelease(btn)
            toastr.success("La catégorie à été ajouté")
            $("#addCategory").on('hidden.bs.modal', (e) => {
                window.location.reload()
            })
        },
        error: (err) => {
            console.error(error)
            KTUtil.btnRelease(btn)
            toastr.error('Erreur de script, consulter les logs', 'Erreur Serveur')
        }
    })
})
$("#formEditCategory").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formEditCategory")
    let url = form.attr('action')
    let btn = document.querySelector('.btn-submit')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning)

    $.ajax({
        url: url,
        method: 'PUT',
        data: data,
        success: (data) => {
            KTUtil.btnRelease(btn)
            toastr.success("La catégorie à été modifié")
            $("#editCategory").modal('hide')
        },
        error: (err) => {
            KTUtil.btnRelease(btn)
            toastr.error('Erreur Systeme, consulter les logs')
            toastr.error(err)
        }
    })
})

dataTable.on('click', '.btn-edit', (e) => {
    e.preventDefault()
    console.log(e.currentTarget.href)
    $.ajax({
        url: e.currentTarget.href,
        success: (data) => {
            $("#formEditCategory").attr('action', e.currentTarget.href)
            $("#name_category").val(data.name)
            $("#editCategory").modal('show')
        },
        error: (err) => {
            console.log(err)
            toastr.error("Erreur de script, consulter les logs", "Erreur Système")
        }
    })
})
dataTable.on('click', '.btn-trash', (e) => {
    e.preventDefault()
    $.ajax({
        url: e.currentTarget.href,
        method: 'DELETE',
        success: () => {
            e.currentTarget.parentNode.parentNode.parentNode.style.display = 'none'
            toastr.success("La catégorie à été supprimer")
        },
        error: (err) => {
            console.log(err)
            toastr.error("Erreur de script, consulter les logs", "Erreur Système")
        }
    })
})
