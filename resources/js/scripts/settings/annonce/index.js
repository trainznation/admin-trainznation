const spinning = 'spinner spinner-right spinner-white pr-15'
let listeAnnonce;
let now = moment().format("YYYY-MM-DD hh:mm:ss");

$.ajax({
    url: '/settings/announcement/list',
    success: (data) => {
        loadListeAnnonce(data.data)
    }
})

function loadListeAnnonce(data) {
    listeAnnonce = $("#listeAnnonce").KTDatatable({
        data: {
            type: 'local',
            source: data,
            pageSize: 10
        },
        // layout definition
        layout: {
            scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
            // height: 450, // datatable's body's fixed height
            footer: false, // display/hide footer
        },

        // column sorting
        sortable: true,

        pagination: true,

        search: {
            input: $('#search_liste_annonce'),
            key: 'generalSearch'
        },

        columns: [
            {
                field: 'id',
                title: 'ID',
                width: 30
            },
            {
                field: 'message',
                title: 'Message',
            },
            {
                field: 'type',
                title: 'Type',
                width: 160,
                textAlign: "center",
                template: (row) => {
                    let type = {
                        'info': {'class': 'info', 'text': 'info'},
                        'success': {'class': 'success', 'text': 'success'},
                        'warning': {'class': 'warning', 'text': 'warning'},
                        'danger': {'class': 'danger', 'text': 'danger'},
                    }

                    return `<i class="far fa-dot-circle text-${type[row.type].class} fa-2x" data-toggle="tooltip" title="${type[row.type].text}"></i>`
                }
            },
            {
                field: 'expiring_at',
                title: 'Expiration',
                textAlign: "center",
                template: (row) => {
                    return `${(row.expiring_at.format > now) ? '<span class="label label-inline label-success">Expire le '+row.expiring_at.normalize+'</span>' : '<span class="label label-inline label-danger">Expirée</span>'}`
                }
            },
            {
                field: 'Actions',
                title: 'Actions',
                textAlign: "center",
                template: (row) => {
                    return `
                        <div class="btn-group">
                            <a href="/settings/announcement/${row.id}" class="btn btn-sm btn-icon btn-danger btn-delete"><i class="fa fa-trash"></i> </a>
                        </div>
                    `
                }
            }
        ],
    })
    $('[data-toggle="tooltip"]').tooltip()
}

$("#expiring_at").datetimepicker({
    locale: 'fr',
    useCurrent: true,
    format: "YYYY-MM-DD HH:mm:ss",
    icons: {
        time: "fas fa-clock",
        date: "far fa-calendar-alt",
        up: "fas fa-angle-up",
        down: "fas fa-angle-down"
    },

});

$("#formAddAnnonce").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formAddAnnonce");
    let uri = form.attr('action')
    let btn = document.querySelector('.btn-submit')
    let data = form.serializeArray();

    KTUtil.btnWait(btn, spinning, 'Veuillez patienter', true)

    $.ajax({
        url: uri,
        method: 'post',
        data: data,
        statusCode: {
            200: () => {
                KTUtil.btnRelease(btn)
                toastr.success("L'annonce à été ajouter")
                $("#addAnnonce").on('hide.bs.modal', () => {
                    window.location.reload();
                })
            },
            500: (error) => {
                KTUtil.btnRelease(btn)
                console.error("Err", error)
                toastr.error("Erreur serveur, consulter les logs", "Erreur 500");
            }
        },
    })
})

