let table;

function loadListObject() {
    table = $("#listeObjets").KTDatatable({
        data: {
            saveState: {cookie: false}
        },
        search: {
            input: $('#search_liste_objet'),
            key: 'generalSearch'
        },
        columns: [
            {
                field: "#",
                width: 40
            },
            {
                field: 'Image',
                width: 120,
            },
            {
                field: 'Designation',
                width: 250
            },
            {
                field: 'Publier',
                title: 'Publier',
                autoHide: false,
                template: (row) => {
                    let publier = {
                        0: {'title': 'Non Publier', 'class': 'label-danger'},
                        1: {'title': 'Publier', 'class': 'label-success'}
                    };
                    return `<span class="label font-weight-bold label-inline label-lg ${publier[row.Publier].class}">${publier[row.Publier].title}</span>`
                }
            }
        ]
    });

    $("#kt_datatable_search_publier").on('change', () => {
        table.search($('#kt_datatable_search_publier').val().toLowerCase(), 'Publier')
    })

    $("#kt_datatable_search_publier").selectpicker();

    table.on('click', '.btn-danger', (e) => {
        e.preventDefault()
        let btn = $(".btn-danger")

        KTUtil.btnWait(document.querySelector('.btn-danger'), 'spinner')

        fetch(`/asset/${btn.attr('data-id')}`, {
            method: 'delete'
        })
            .then(response => response.text())
            .then(result => {
                KTUtil.btnRelease(document.querySelector('.btn-danger'))
                let data = JSON.parse(result)
                toastr.success(data.data.message)
            })
            .catch(error => {
                KTUtil.btnRelease(document.querySelector('.btn-danger'))
                let data = JSON.parse(error)
                toastr.error(data.data.message)
            })
    })


}


loadListObject();
