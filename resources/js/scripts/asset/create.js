let wizard;

function WizardObj() {
    let wizardEl = document.querySelector('#kt_wizard')
    let formEl = document.querySelector('#kt_form')
    let validations = [];

    wizard = new KTWizard(wizardEl, {
        startStep: 1,
        clickableSteps: false
    });

    wizard.on('change', (wizard) => {
        if (wizard.getStep() > wizard.getNewStep()) {
            return;
        }

        let validator = validations[wizard.getStep() - 1];

        if (validator) {
            validator.validate().then((status) => {
                if (status === 'Valid') {
                    wizard.goTo(wizard.getNewStep())

                    KTUtil.scrollTop();
                } else {
                    Swal.fire({
                        text: "Désolé, il semble que des erreurs ont été détectées, veuillez réessayer.",
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, compris!",
                        customClass: {
                            confirmButton: "btn font-weight-bold btn-light"
                        }
                    }).then(function () {
                        KTUtil.scrollTop();
                    });
                }
            })
        }

        return false;
    });

    wizard.on('changed', () => {
        KTUtil.scrollTop()
    })

    wizard.on('submit', () => {
        Swal.fire({
            text: "Tout est bon! Veuillez confirmer la soumission du formulaire.",
            icon: "success",
            showCancelButton: true,
            buttonsStyling: false,
            confirmButtonText: "Oui, soumettre",
            cancelButtonText: "Non, annuler",
            customClass: {
                confirmButton: "btn font-weight-bold btn-primary",
                cancelButton: "btn font-weight-bold btn-default"
            }
        }).then(function (result) {
            if (result.value) {
                formEl.submit()
            } else if (result.dismiss === 'cancel') {
                Swal.fire({
                    text: "Le formulaire n'a pas été soumis",
                    icon: "error",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, compris !",
                    customClass: {
                        confirmButton: "btn font-weight-bold btn-primary",
                    }
                });
            }
        });
    })

    let initValidation = () => {
        validations.push(FormValidation.formValidation(formEl, {
            fields: {
                asset_category_id: {
                    validators: {
                        notEmpty: {
                            message: "La catégorie est requise"
                        }
                    }
                },
                designation: {
                    validators: {
                        notEmpty: {
                            message: "La désignation est requise"
                        }
                    }
                }
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                // Bootstrap Framework Integration
                bootstrap: new FormValidation.plugins.Bootstrap({
                    //eleInvalidClass: '',
                    eleValidClass: '',
                })
            }
        }));
        validations.push(FormValidation.formValidation(formEl, {
            fields: {
                short_description: {
                    validators: {
                        notEmpty: {
                            message: "La courte description est requise"
                        },
                        stringLength: {
                            max: 255,
                            message: 'La courte description ne doit pas être supérieur à 255 caractères'
                        }
                    }
                }
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                // Bootstrap Framework Integration
                bootstrap: new FormValidation.plugins.Bootstrap({
                    //eleInvalidClass: '',
                    eleValidClass: '',
                })
            }
        }));
        validations.push(FormValidation.formValidation(formEl, {
            fields: {

            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                // Bootstrap Framework Integration
                bootstrap: new FormValidation.plugins.Bootstrap({
                    //eleInvalidClass: '',
                    eleValidClass: '',
                })
            }
        }));
    }

    initValidation()
}

function formatSearch(data) {
    if(data.loading) return data.text

    let markup = `
        <div class="row">
            <div class="col-md-2"><img src="${data.image}" style="width: 40px; height: 40px;" alt/> </div>
            <div class="col-md-10">${data.name}</div>
        </div>
    `;

    return markup;
}

function formatSelect(data) {
    return data.name || data.text
}

function formatedField() {
    moment.locale('fr')

    $("#published_at").datetimepicker({
        locale: 'fr',
        format: "YYYY-MM-DD HH:mm:ss",
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        }
    })
    $('[data-switch=true]').bootstrapSwitch();
    $('#short_description').maxlength({
        alwaysShow: true
    })

    $("#copyContent").on('click', (e) => {
        e.preventDefault()
        let short = $("#short_description").val()
        let desc = $("#description")

        desc.html(`_${short}_`)
    })

    $("#price").inputmask('999 999 999.99 €', {
        numericInput: true
    })

    new KTImageInput('asset_img')

    function appearPublishedAtField() {
        let published = $("#published")
        let published_at = $("#div_published_at")

        published.on('change', () => {
            published_at.show()
        })
    }

    function appearPriceField() {
        let pricing = $("#pricing")
        let price = $("#div_price")

        pricing.on('change', () => {
            price.show()
        })
    }

    function appearMeshesField() {
        let meshes = $("#meshes")
        let file = $("#div_meshes")

        meshes.on('change', () => {
            file.show()
        })
    }
}


$("#asset_category_id").select2({
    placeholder: 'Rechercher...',
    allowClear: true,
    ajax: {
        url: '/asset/category/list',
        dataType: 'json',
        delay: 250,
        data: (params) => {
            return {
                q: params.term,
            };
        },
        processResults: (data, params) => {
            return {
                results: data.items,
            };
        },
        cache: true
    },
    escapeMarkup: (markup) => {
        return markup;
    },
    minimumInputLength: 1,
    templateResult: formatSearch,
    templateSelection: formatSelect,
    language: {
        inputTooShort: function () {
            return "Veuillez entrer au moins 1 caractère...";
        },
        searching: () => {
            return "Recherche en cours..."
        },
        errorLoading: function() {
            return "Les résultats ne peuvent pas être chargés."
        },
    }
});

WizardObj();
formatedField()

