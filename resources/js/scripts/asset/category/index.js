let table;

function loadListeCategories() {
    table = $("#listeCategories").KTDatatable({
        data: {
            saveState: {cookie: false}
        },
        search: {
            input: $('#search_liste_categories'),
            key: 'generalSearch'
        },
        columns: [
            {
                field: "#",
                width: 40
            },
            {
                field: "Icon",
                width: 50,
            },
            {
                field: "Designation",
                width: 250
            }
        ]
    });


}

function deleteCategory() {
    let btns = document.querySelectorAll('.btn-delete')
    Array.from(btns).forEach((btn) => {
        btn.addEventListener('click', (e) => {
            console.log(e)
        })
    })
}

loadListeCategories();
deleteCategory();
