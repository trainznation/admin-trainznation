const spinning = 'spinner spinner-right spinner-white pr-15'
let tableKuid = document.querySelector('#tableKuid')
let tableTags = document.querySelector('#tableTags')

document.querySelector('#markedDown').innerHTML = marked(document.querySelector('#markedDown').dataset.content)

$("#btnTrash").on('click', (e) => {
    e.preventDefault()
    let btn = document.querySelector('#btnTrash')
    let uri = $("#btnTrash").attr('href')

    KTUtil.btnWait(btn, 'spinner')

    $.ajax({
        url: uri,
        method: 'DELETE',
        success: (data) => {
            KTUtil.btnRelease(btn)
            toastr.success(data.data.message)
            setTimeout(() => {
                window.location.href='/asset'
            }, 1500)
        },
        error: (error) => {
            KTUtil.btnRelease(btn)
            console.log(error)
            toastr.error(error.message)
        }
    })
})
$("#formaddKuid").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formaddKuid")
    let btn = document.querySelector('#btnFormAddKuid')
    let uri = form.attr('action')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning)

    $.ajax({
        url: uri,
        method: "POST",
        data: data,
        success: (data) => {
            KTUtil.btnRelease(btn)
            form[0].reset()

            let tr = data;
            $('#tableKuid tbody').prepend(tr).fadeIn()

            $("#addKuid").modal('hide')

            toastr.success("Le kuid à été ajouté");
        },
        error: (error) => {
            KTUtil.btnRelease(btn)
            console.log(error)
            toastr.error("Erreur Système")
        }
    })
})
$("#formAddTags").on('submit', (e) => {
    e.preventDefault();
    let form = $("#formAddTags")
    let btn = document.querySelector('#btnFormAddTags')
    let uri = form.attr('action')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning)

    $.ajax({
        url: uri,
        method: 'POST',
        data: data,
        success: (data) => {
            KTUtil.btnRelease(btn)
            form[0].reset()
            toastr.success("Le ou les tags ont été ajoutés", "Succès")

        },
        error: (error) => {
            KTUtil.btnRelease(btn)
            console.log(error)
            toastr.error("Erreur Systeme")
        }
    })
})
$("#formAddSketchfab").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formAddSketchfab")
    let uri = form.attr('action')
    let btn = document.querySelector("#btnFormAddSketchfab")
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning, 'Veuillez patienter...', true)

    $.ajax({
        url: uri,
        method: 'POST',
        data: data,
        success: (data) => {
            KTUtil.btnRelease(btn)
            form[0].reset()

            toastr.success("L'UID Sketchfab pour la vue 3D à été paramétrer")
        },
        error: (error) => {
            KTUtil.btnRelease(btn)
            console.error(error)
            toastr.error("Erreur Système, consulter les logs", "Erreur Serveur")
        }
    })
})
$("#formAddPrice").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formAddPrice")
    let uri = form.attr('action')
    let btn = document.querySelector("#btnFormAddPrice")
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning, 'Veuillez patienter...', true)

    $.ajax({
        url: uri,
        method: 'POST',
        data: data,
        success: (data) => {
            KTUtil.btnRelease(btn)
            form[0].reset()

            toastr.success("Le prix de l'asset à été fixé")
        },
        error: (error) => {
            KTUtil.btnRelease(btn)
            console.error(error)
            toastr.error("Erreur Système, consulter les logs", "Erreur Serveur")
        }
    })
})


function formatField() {
    moment.locale('fr')

    $("#published_at").datetimepicker({
        locale: 'fr',
        format: "YYYY-MM-DD HH:mm:ss",
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        }
    })
    $("#kuid_field").inputmask('kuid:400722:9999999', {
        rightAlignNumerics: false,
        //numericInput: true
    })

    let tags_field = document.querySelector('#tags_field');
    let tagify = new Tagify(tags_field, {
        originalInputValueFormat: valuesArr => valuesArr.map(item => item.value).join(',')
    })
}
function deleteKuid() {
    let btns = document.querySelectorAll('.btn-delete-kuid')
    Array.from(btns).forEach((btn) => {
        btn.addEventListener('click', (e) => {
            e.preventDefault()
            let uri = btn.dataset.href
            let tr = btn.parentElement.parentElement

            KTUtil.btnWait(btn, spinning)

            $.ajax({
                url: uri,
                method: 'DELETE',
                success: (data) => {
                    KTUtil.btnRelease(btn)
                    tableKuid.deleteRow(tr.rowIndex)
                    toastr.success(data)

                },
                error: (error) => {
                    KTUtil.btnRelease(btn)
                    console.log(error)
                    toastr.error(error)
                }
            })
        })
    })
}
function deleteTag() {
    let btns = document.querySelectorAll('.btn-delete-tag')
    Array.from(btns).forEach((btn) => {
        btn.addEventListener('click', (e) => {
            e.preventDefault()
            let uri = btn.dataset.href
            let tr = btn.parentElement.parentElement

            KTUtil.btnWait(btn, spinning)

            $.ajax({
                url: uri,
                method: 'DELETE',
                success: (data) => {
                    KTUtil.btnRelease(btn)
                    tableTags.deleteRow(tr.rowIndex)
                    toastr.success(data.data.message)

                },
                error: (error) => {
                    KTUtil.btnRelease(btn)
                    console.log(error)
                    toastr.error(error.data.error)
                }
            })
        })
    })
}

formatField();
deleteKuid()
deleteTag()


