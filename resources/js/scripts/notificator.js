let notifElement = {
    pulsingNotif: KTUtil.getById('pulsingNotif'),
    counter: null,
    counterNotif: KTUtil.getById('counterNotif'),
    notifList: KTUtil.getById('notifList')
}

$.ajax({
    url: '/me',
    success: (data) => {
        let notifications = data.notifications;
        verifyCountUnreadNotification(notifications)
    }
})


function verifyCountUnreadNotification(notifications) {
    let counter = 0;
    Array.from(notifications).forEach((notif) => {
        if (notif.read_at === null) {
            counter++
        }
    });

    if (counter !== 0) {
        notifElement.pulsingNotif.classList.add('pulse-ring')
    } else {
        notifElement.pulsingNotif.classList.remove('pulse-ring')
    }

    if(counter === 0 || counter <= 1) {
        notifElement.counterNotif.innerHTML = `${counter} Nouvelle Notification`
    } else {
        notifElement.counterNotif.innerHTML = `${counter} Nouvelles Notifications`
    }

    appearListNotifications(notifications)
}

function appearListNotifications(notifications) {
    notifElement.notifList.innerHTML = null
    Array.from(notifications).forEach((notif) => {
        if(notif.read_at === null) {
            let data = notif
            let date = moment(notif.updated_at)
            let now = moment()
            let diff = now.diff(date)


            let dateTransform;
            if(diff < 60000) {
                dateTransform = `Il y a ${now.diff(date, 'seconds')} Secondes`
            } else if(diff < 3600000) {
                dateTransform = `Il y a ${now.diff(date, 'minutes')} Minutes`
            } else if(diff < 86400000) {
                dateTransform = `Il y a ${now.diff(date, 'hours')} Heures`
            } else if(diff < 2592000000) {
                dateTransform = `Il y a ${now.diff(date, 'days')} Jours`
            } else {
                dateTransform = `Il y a ${now.diff(date, 'months')} Mois`
            }
            notifElement.notifList.innerHTML += `
                <a data-href="/notif/${notif.user_id}/${notif.id}" class="navi-item readNotif">
                    <div class="navi-link rounded">
                        <div class="symbol symbol-50 symbol-circle mr-3">
                            <div class="symbol-label"><i class="${data.icon} text-${data.status} icon-lg"></i> </div>
                        </div>
                        <div class="navi-text">
                            <div class="font-weight-bold font-size-lg">${data.title}</div>
                            <div class="text-muted">${dateTransform}</div>
                        </div>
                    </div>
                </a>
            `
        }
    })
    initReadNotif()
}

function initReadNotif() {
    let readNotif = document.querySelectorAll('.readNotif')
    Array.from(readNotif).forEach((notif) => {
        notif.addEventListener('click', (e) => {
            let uri = notif.dataset.href

            KTApp.block(notif, {
                overlayColor: '#000000',
                state: 'warning', // a bootstrap color
                size: 'lg' //available custom sizes: sm|lg
            })

            $.ajax({
                url: uri,
                success: (data) => {
                    console.log(data)
                    KTApp.unblock(notif)
                    notif.style.display = 'none'
                    verifyCountUnreadNotification(readNotif)
                },
                error: (err) => {
                    console.log(err)
                    toastr.error("Erreur Système, consulter les logs", "Erreur 500")
                    KTApp.unblock(notif)
                }
            })
        })
    })
}

function readNotif(notif, event) {
    let uri = notif.attr('data-href')


    KTApp.block(notif, {
        overlayColor: '#000000',
        state: 'warning', // a bootstrap color
        size: 'lg' //available custom sizes: sm|lg
    })

    $.ajax({
        url: uri,
        success: (data) => {
            console.log(data)
            KTApp.unblock(notif)
            notif.fadeOut()
        },
        error: (err) => {
            console.log(err)
            toastr.error("Erreur Système, consulter les logs", "Erreur 500")
            KTApp.unblock(notif)
        }
    })
}
