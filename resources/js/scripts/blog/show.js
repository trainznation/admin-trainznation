const spinning = 'spinner spinner-right spinner-white pr-15'
const marked = require('markdown').markdown

function btnDetails() {
    let btns = document.querySelectorAll('.btn-details')
    let modal = $("#modalDetails")
    Array.from(btns).forEach((btn) => {
        btn.addEventListener('click', (e) => {
            e.preventDefault()
            let title = btn.dataset.title
            let comment = btn.dataset.comment

            modal.find('.modal-title').html(`Commentaire de l'article: ${title}`)
            modal.find('.modal-body').html(comment)

            modal.modal('show')
        })
    })
}
function btnApprove() {
    let btns = document.querySelectorAll('.btn-approve')
    Array.from(btns).forEach((btn) => {
        btn.addEventListener('click', (e) => {
            KTUtil.btnWait(btn, spinning)

            $.ajax({
                url: `/blog/${btn.dataset.articleid}/comment/${btn.dataset.commentid}/approve`,
                success: (data) => {
                    KTUtil.btnRelease(btn)
                    btn.classList.remove('btn-success')
                    btn.classList.remove('btn-approve')
                    btn.classList.add('btn-danger')
                    btn.classList.add('btn-disapprove')
                    btn.setAttribute('data-title', 'Désapprover')
                    btn.innerHtml = '<i class="fa fa-times"></i>'

                    toastr.success(data.data.message)
                },
                error: (error) => {
                    KTUtil.btnRelease(btn)
                    console.error(error)
                    toastr.error("Erreur Système")
                }
            })
        })
    })
}
function btnDisapprove() {
    let btns = document.querySelectorAll('.btn-disapprove')
    Array.from(btns).forEach((btn) => {
        btn.addEventListener('click', (e) => {
            KTUtil.btnWait(btn, spinning)

            $.ajax({
                url: `/blog/${btn.dataset.articleid}/comment/${btn.dataset.commentid}/disapprove`,
                success: (data) => {
                    KTUtil.btnRelease(btn)
                    btn.classList.remove('btn-danger')
                    btn.classList.remove('btn-disapprove')
                    btn.classList.add('btn-success')
                    btn.classList.add('btn-approve')
                    btn.setAttribute('data-title', 'Approuver')
                    btn.innerHtml = '<i class="fa fa-check"></i>'

                    toastr.success(data.data.message)
                },
                error: (error) => {
                    KTUtil.btnRelease(btn)
                    console.error(error)
                    toastr.error("Erreur Système")
                }
            })
        })
    })
}

btnDetails()
btnApprove()
btnDisapprove()
document.querySelector('#markedDown').innerHTML = marked.toHTML(document.querySelector('#markedDown').dataset.content)
