let table;
const spinning = 'spinner spinner-right spinner-white pr-15'

function loadListeCategories() {
    table = $("#listeCategories").KTDatatable({
        data: {
            saveState: {cookie: false}
        },
        search: {
            input: $('#search_liste_categories'),
            key: 'generalSearch'
        },
        columns: [
            {
                field: "#",
                width: 40
            },
            {
                field: "Designation",
                width: 400
            },
        ]
    });
}




$("#formAddCategorie").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formAddCategorie")
    let btn = document.querySelector('.btn-submit')
    let uri = form.attr('action')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning, 'Chargement...')

    $.ajax({
        url: uri,
        method: 'POST',
        data: data,
        success: (data) => {
            KTUtil.btnRelease(btn)
            toastr.success(`La catégorie ${data.data.data.name} à été ajouté`)
            let tr = data.content
            form[0].reset()
            $('table tbody').prepend(tr).fadeIn();
        },
        error: (error) => {
            KTUtil.btnRelease(btn)
            console.error(error)
            toastr.error(error.responseText)
        }
    })
})

$("#addcategory").on('hidden.bs.modal', (e) => {
    window.location.reload()
})

loadListeCategories();
