function authenticate() {
    return gapi.auth2.getAuthInstance()
        .signIn({scope: "https://www.googleapis.com/auth/youtube.readonly"})
        .then(() => {console.log('Sign-in Successful')}, (err) => {console.error("Error signing in", err); })
}

function loadClient() {
    gapi.client.setApiKey("AIzaSyALQZ8Eqjhck2EBqoo8myHkf0UYWFKMz18");
    return gapi.client.load("https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest")
        .then(function() { console.log("GAPI client loaded for API"); },
            function(err) { console.error("Error loading GAPI client for API", err); });
}

function execute() {
    return gapi.client.youtube.videos.list({
        "part": [
            "snippet"
        ],
        "id": [
            "Ks-_Mh1QhMc"
        ]
    })
        .then(function(response) {
                // Handle the results here (response.result has the parsed body).
                console.log("Response", response);
            },
            function(err) { console.error("Execute error", err); });
}

gapi.load("client:auth2", function() {
    gapi.auth2.init({client_id: "827701011917-p7ltoll1a32a56e2atvuli5pqm37hagf.apps.googleusercontent.com"});
});

$("#btnAuth").on('click', (e) => {
    authenticate().then(loadClient)
})

$("#btnExec").on('click', (e) => {
    execute()
})
