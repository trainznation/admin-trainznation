const spinning = 'spinner spinner-right spinner-white pr-15'

function loadMessage() {
    $.ajax({
        url: $(".message").attr('data-href'),
        success: (data) => {
            $(".message").html(data)
        }
    })
}

$("#formSubmitMessage").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formSubmitMessage")
    let url = form.attr('action')
    let btn = document.querySelector('.chat-sending')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning)

    $.ajax({
        url: url,
        method: 'POST',
        data: data,
        success: (data) => {
            console.log(data)
            KTUtil.btnRelease(btn)
        }
    })
})

loadMessage()
setInterval(() => {
    loadMessage()
}, 5000)
