const spinning = 'spinner spinner-right spinner-white pr-15'
let dataTable;

dataTable = $("#listeTickets").KTDatatable({
    data: {
        type: 'remote',
        source: {
            read:{
                url: $("#listeTickets").attr('data-href')
            }
        }
    },
    search: {
        input: $("#liste_search_tickets"),
        key: 'generalSearch'
    },
    columns: [
        {
            field: 'id',
            title: "#",
            width: 25
        },
        {
            field: 'subject',
            title: 'Sujet'
        },
        {
            field: 'priority.id',
            title: 'Priorite',
            template: (row) => {
                return `<span class="label label-inline label-${row.priority.color}">${row.priority.designation}</span>`
            }
        },
        {
            field: 'status.id',
            title: 'Status',
            template: (row) => {
                return `<span class="label label-inline label-${row.status.color}"><i class="${row.status.icon} text-white"></i> ${row.status.designation}</span>`
            }
        },
        {
            field: 'source.id',
            title: 'Source',
            template: (row) => {
                return `<span class="label label-inline label-default">${row.source.designation}</span>`
            },
            width: 100
        },
        {
            field: 'Actions',
            title: 'Actions',
            template: (row) => {
                return `
                <a href="/support/${row.id}" class="btn btn-primary btn-icon" data-toggle="tooltip" title="Voir le ticket"><i class="fas fa-eye"></i> </a>
                <a href="/support/${row.id}/edit" class="btn btn-info btn-icon" data-toggle="tooltip" title="Editer le ticket"><i class="fas fa-edit"></i> </a>
                <a href="/support/${row.id}/delete" class="btn btn-danger btn-icon" data-toggle="tooltip" title="Supprimer le ticket"><i class="fas fa-trash"></i> </a>
                `
            },
        },
        {
            field: 'category',
            title: 'Catégorie',
            template: (row) => {
                return `${row.category.name} - ${row.sector.name}`
            },
            autoHide: true
        },
        {
            field: 'requester',
            title: 'Utilisateur',
            template: (row) => {
                return `<span class="label label-inline label-default">${row.requester.name}</span>`
            },
            autoHide: true
        },
        {
            field: 'created_at',
            title: 'Horodatage',
            template: (row) => {
                return `<strong>Créer le:</strong> ${row.created_at.normalize}<br><strong>Mise à jour le:</strong> ${row.updated_at.normalize}`
            },
            autoHide: true
        }
    ]
});

$('#kt_datatable_search_status').on('change', function() {
    dataTable.search($(this).val().toLowerCase(), 'status.id');
});

$('#kt_datatable_search_priority').on('change', function() {
    dataTable.search($(this).val().toLowerCase(), 'priority.id');
});

$('#kt_datatable_search_source').on('change', function() {
    dataTable.search($(this).val().toLowerCase(), 'source.id');
});

$('#kt_datatable_search_status, #kt_datatable_search_priority, #kt_datatable_search_source').selectpicker();


$("#support_category_id").on('change', (e) => {
    $.ajax({
        url: `/support/sector/${$("#support_category_id").val()}/search`,
        success: (data) => {
            let content = ``;
            data.forEach(item => {
                console.log(item)
                content += `<option value="${item.id}">${item.name}</option>`
            })
            $("#support_sector_id").html(content)
        }
    })
})
$("#formAddTicket").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formAddTicket")
    let url = form.attr('action')
    let btn = document.querySelector('.btn-submit')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning)

    $.ajax({
        url: url,
        method: 'POST',
        data: data,
        success: (data) => {
            KTUtil.btnRelease(btn)
            console.log(data)
            /*toastr.success("Le ticket à été créer")
            $("#addTicket").modal('hide')
                .on('hidden.bs.modal', (e) => {
                    window.location.reload()
                })*/
        },
        error: (err) => {
            KTUtil.btnRelease(btn)
            console.error(err)
            toastr.error("Erreur Système, consulter les logs")
        }
    })
})
