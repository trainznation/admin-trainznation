let listeTutoriel;
const spinning = 'spinner spinner-right spinner-white pr-15'

$.ajax({
    url: '/tutoriel/list',
    success: (data) => {
        loadListeTutoriels(data.data)
    }
})

function loadListeTutoriels(data) {
    listeTutoriel = $("#listeTutoriel").KTDatatable({
        data: {
            type: 'local',
            source: data,
            pageSize: 10
        },
        // layout definition
        layout: {
            scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
            // height: 450, // datatable's body's fixed height
            footer: false, // display/hide footer
        },

        // column sorting
        sortable: true,

        pagination: true,

        search: {
            input: $('#search_liste_tutoriels'),
            key: 'generalSearch'
        },

        columns: [
            {
                field: 'id',
                title: 'ID',
                width: 30
            },
            {
                field: 'title',
                title: 'Titre',
                template: (row) => {
                    return `
                    <div class="d-flex flex-column">
                        <span class="font-size-lg font-weight-bold">${row.title}</span>
                        <span class="text-muted">${row.category.name}</span>
                    </div>
                `
                }
            },
            {
                field: 'image',
                title: 'Thumbnail',
                width: 160,
                template: (row) => {
                    return `<img src="${row.image}" class="img-fluid" style="width: 160px" alt>`
                }
            },
            {
                field: 'stat',
                title: "Etat",
                template: (row) => {
                    let status = {
                        0: {'class': 'text-dark'},
                        1: {'class': 'text-primary'},
                    }

                    return `
                        <i class="fab fa-facebook-square ${status[row.social].class}"></i>
                        <i class="far fa-file-archive ${status[row.source].class}"></i>
                        <i class="fas fa-certificate ${status[row.premium].class}"></i>
                    `
                }
            },
            {
                field: 'published',
                title: "Publier",
                textAlign: 'center',
                width: 50,
                template: (row) => {
                    let published = {
                        0: {'class': 'danger', 'icon': 'fas fa-times-circle'},
                        1: {'class': 'success', 'icon': 'far fa-check-circle'},
                    }

                    return `<i class="${published[row.published].icon} text-${published[row.published].class} fa-2x"></i>`
                },
            },
            {
                field: 'Actions',
                title: 'Actions',
                textAlign: "center",
                template: (row) => {
                    return `
                        <div class="btn-group">
                            <a href="/tutoriel/${row.id}" class="btn btn-sm btn-icon btn-primary btn-show"><i class="fa fa-eye"></i> </a>
                            <a href="/tutoriel/${row.id}/edit" class="btn btn-sm btn-icon btn-secondary btn-edit"><i class="fa fa-edit"></i> </a>
                            <a href="/tutoriel/${row.id}/delete" class="btn btn-sm btn-icon btn-danger btn-delete"><i class="fa fa-trash"></i> </a>
                        </div>
                    `
                }
            }
        ],
    })
}
