const spinning = 'spinner spinner-right spinner-white pr-15'
let slugify = require('slugify')

$.ajax({
    url: '/tutoriel/category/list',
    method: 'GET',
    success: (data) => {
        let categories = data.data
        let select = document.getElementById('tutoriel_category_id')
        select.innerHTML = '';
        Array.from(categories).forEach((category) => {
            select.innerHTML += `<option value="${category.id}">${category.name}</option>`
        })
    }
})

function appearSourceField() {
    let sourceSwitch = $("#sourceSwitch")
    let sourceUpload = $("#div_source_field")

    if(sourceSwitch.is(':checked')) {
        sourceUpload.show()
    } else {
        sourceUpload.hide()
    }
}

function formatFields() {
    moment.locale('fr')

    document.getElementById('title').addEventListener('keyup', (e) => {
        document.getElementById('uri').value = slugify(document.getElementById('title').value)
    })

    $('[data-switch=true]').bootstrapSwitch();

    $("#date_published").datetimepicker({
        locale: 'fr',
        format: "YYYY-MM-DD HH:mm:ss",
        icons: {
            time: "fas fa-clock",
            date: "far fa-calendar-alt",
            up: "fas fa-angle-up",
            down: "fas fa-angle-down"
        }
    })

    $("#copycontent").on('click', (e) => {
        e.preventDefault()
        let short = $("#short_content").val()
        let desc = $("#content")

        desc.html(`_${short}_`)
    })
}

$("#img_file").on('change', (e) => {
    showPreviewImg(e)
})

function showPreviewImg(event){
    if(event.target.files.length > 0){
        let src = URL.createObjectURL(event.target.files[0]);
        let preview = document.getElementById("img_view");
        preview.src = src;
        preview.style.display = "block";
    }
}

formatFields()
appearSourceField()

