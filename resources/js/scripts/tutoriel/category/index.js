let listeCategory;
const spinning = 'spinner spinner-right spinner-white pr-15'

$.ajax({
    url: '/tutoriel/category/list',
    success: (data) => {
        loadListeCategory(data.data)
    }
})

function loadListeCategory (data) {
    listeCategory = $("#listeCategories").KTDatatable({
        data: {
            type: 'local',
            source: data,
            pageSize: 10
        },
        // layout definition
        layout: {
            scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
            // height: 450, // datatable's body's fixed height
            footer: false, // display/hide footer
        },

        // column sorting
        sortable: true,

        pagination: true,

        search: {
            input: $('#search_query_category'),
            key: 'generalSearch'
        },

        columns: [
            {
                field: 'name',
                title: "Categorie",
                template: (row) => {
                    return `
                        <div class="d-flex align-items-center">
                            <div class="symbol symbol-30 mr-5">
                                <img src="${row.image}" alt="${row.name}">
                            </div>
                            <div class="d-flex flex-column font-weight-bold">
                                <span class="text-dark font-size-lg">${row.name}</span>
                            </div>
                        </div>
                    `
                }
            },
            {
                field: 'Actions',
                title: 'Actions',
                template: (row) => {
                    return `
                        <button class="btn btn-icon btn-sm btn-danger" data-href="/tutoriel/category/${row.id}" data-toggle="tooltip" title="Supprimer la catégorie" onclick="deleteCategory($(this))"><i class="fa fa-trash"></i> </button>
                    `
                }
            }
        ]
    })
}

function deleteCategory(btn) {
    let uri = btn.attr('data-href')

    btn.html(`<div class="${spinning}"></div>`)

    $.ajax({
        url: uri,
        method: 'DELETE',
        success: (data) => {
            btn.html('<i class="fa fa-trash"></i>')
            toastr.success(data.data.message)
            btn.parents('tr').fadeOut()
        },
        error: (error) => {
            btn.html('<i class="fa fa-trash"></i>')
            console.error(error)
            toastr.error('Erreur Système, consulter les logs', "Erreur 500")
        }
    })
}
