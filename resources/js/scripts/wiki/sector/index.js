const spinning = 'spinner spinner-right spinner-white pr-15'
const Elements = {
    searchInList: document.querySelector('#searchInList'),
    loadListSector: document.querySelector('#loadListSector'),
    btnDanger: document.querySelectorAll('.btn-danger'),
    form: document.querySelector('#formAddSector'),
    btnForm: document.querySelector('#btnAddSector')
}

function searchElement() {
    KTApp.block(Elements.loadListSector, {message: "Chargement..."})

    $.ajax({
        url: Elements.loadListSector.dataset.target,
        method: 'POST',
        data: {q: Elements.searchInList.value},
        success: (data) => {
            KTApp.unblock(Elements.loadListSector)
            Elements.loadListSector.innerHTML = '';
            if(Array.from(data).length === 0) {
                Elements.loadListSector.innerHTML += `<tr><td colspan="3" class="text-center font-weight-bold"><i class="fas fa-warning"></i> Aucun enregistrement</td>`
            } else {
                Array.from(data).forEach(item => {
                    Elements.loadListSector.innerHTML += `
                <tr>
                    <td class="w-5 text-center">${item.id}</td>
                    <td class="w-5 text-center">${item.category.name}</td>
                    <td>${item.name}</td>
                    <td class="w-10 text-center">
                        <a href="/wiki/sector/${item.id}" class="btn btn-sm btn-icon btn-danger"><i class="fas fa-trash"></i> </a>
                    </td>
                </tr>
                `
                })
            }
        },
        error: (err) => {
            KTApp.unblock(Elements.loadListSector)
            console.error(err)
            toastr.error("Erreur de rafraichissement de la liste", "Erreur Système");
        }
    })
}

function refreshTable() {
    searchElement()
}

Array.from(Elements.btnDanger).forEach(btn => {
    btn.addEventListener('click', (e) => {
        e.preventDefault()
        KTUtil.btnWait(btn, spinning)

        $.ajax({
            url: btn.getAttribute('href'),
            success: () => {
                KTUtil.btnRelease(btn)
                toastr.success("Le secteur à été supprimer")
                btn.parentNode.parentNode.style.display = 'none'
            },
            error: (error) => {
                KTUtil.btnRelease(btn)
                console.error(error)
                toastr.error("Erreur lors de la suppression", "Erreur Système")
            }
        })
    })
})

Elements.searchInList.addEventListener('keyup', e => {
    e.preventDefault()
    searchElement()
})

Elements.searchInList.addEventListener('blur', e => {
    e.preventDefault()
    if(Elements.searchInList.value !== '') {
        Elements.searchInList.value = ''
        searchElement()
    }
})

Elements.form.addEventListener('submit', async (e) => {
    e.preventDefault()
    KTUtil.btnWait(Elements.btnForm, spinning, 'Chargement', true)

    let data = new FormData(Elements.form)

    try {
        let response = await fetch(Elements.form.getAttribute('action'), {
            method: 'POST',
            body: data
        })

        let responseData = await response.json()

        if (response.ok === false) {
            KTUtil.btnRelease(Elements.btnForm)
            let errors = responseData
            let errorsKey = Object.keys(errors)

            for (let i = 0; i < errorsKey.length; i++) {
                console.log(errors[key])
            }
        } else {
            KTUtil.btnRelease(Elements.btnForm)
            let inputs = Elements.form.querySelectorAll('input, textarea')
            for (let i = 0; i < inputs.length; i++) {
                inputs[i].value = ''
            }
            toastr.success(`La catégorie <strong>${responseData.name}</strong> à été ajouté`)
            $("#addSector").on('hide.bs.modal', () => {
                refreshTable()
            })
        }
    }catch (e) {
        KTUtil.btnRelease(Elements.btnForm)
        toastr.error("Erreur de script", "Erreur Système")
        console.error(e)
    }
})





