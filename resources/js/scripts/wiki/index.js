const spinning = 'spinner spinner-right spinner-white pr-15'

const Elements = {
    table: document.querySelector('#loadListArticle'),
    search: document.querySelector('#searchInList'),
    btnDanger: document.querySelectorAll('.btn-danger'),
    selectCategory: document.querySelector('#selectCategory'),
    selectSector: document.querySelector('#selectSector'),
    selectPublished: document.querySelector('#selectPublished'),
    refreshTable: document.querySelector('#refreshTable'),
}

/**
 *
 * @param {string} method
 * @param {string} uri
 * @param {HTMLFormElement} form
 */
async function fetching(method, uri, form = null) {
    if(method === 'POST' || method === 'PUT') {
        let data = new FormData(form)
    } else {
        let data = null
    }

    try {
        let response = await fetch(uri, {
            method: method,
            body: data
        })

        let responseData = await response.json()

        if(response.ok === false) {
            let errors = responseData
            let errorsKey = Object.keys(errors)

            return errorsKey
        } else {
            return responseData
        }
    }catch (e) {
        return e
    }
}

function searchElement() {
    KTApp.block(Elements.table, {message: 'Chargement...'})

    $.ajax({
        url: Elements.table.dataset.target,
        method: 'POST',
        data: {q: Elements.search.value},
        success: (data) => {
            KTApp.unblock(Elements.table)
            Elements.table.innerHTML = '';
            if(Array.from(data).length === 0) {
                Elements.table.innerHTML += `<tr><td colspan="6" class="text-center font-weight-bold"><i class="fas fa-warning"></i> Aucun enregistrement</td>`
            } else {
                Array.from(data).forEach(item => {
                    let status = {
                     0:{'class': 'text-danger', 'icon': 'fa-times-circle'},
                     1:{'class': 'text-success', 'icon': 'fa-check-circle'},
                    }
                    Elements.table.innerHTML += `
                <tr>
                    <td class="w-5 text-center">${item.id}</td>
                    <td>${item.category.name}</td>
                    <td>${item.sector.name}</td>
                    <td>${item.title}</td>
                    <td class="text-center">
                        <i class="fas ${status[item.published].icon} ${status[item.published].class} fa-lg"></i>
                    </td>
                    <td class="w-20 text-center">
                        <a href="/wiki/${item.id}" class="btn btn-sm btn-icon btn-default"><i class="fas fa-eye"></i> </a>
                        <a href="/wiki/${item.id}/edit" class="btn btn-sm btn-icon btn-info"><i class="fas fa-edit"></i> </a>
                        <a href="/wiki/${item.id}/delete" class="btn btn-sm btn-icon btn-danger"><i class="fas fa-trash"></i> </a>
                    </td>
                </tr>
                `
                })
            }
        },
        error: (err) => {
            KTApp.unblock(Elements.table)
            console.error(err)
            toastr.error("Erreur de rafraichissement de la liste", "Erreur Système");
        }
    })
}

function refreshTable() {
    searchElement()
}

Array.from(Elements.btnDanger).forEach(btn => {
    btn.addEventListener('click', (e) => {
        e.preventDefault()
        KTUtil.btnWait(btn, spinning)

        $.ajax({
            url: btn.getAttribute('href'),
            success: () => {
                KTUtil.btnRelease(btn)
                toastr.success("L'article à été supprimer")
                btn.parentNode.parentNode.style.display = 'none'
            },
            error: (error) => {
                KTUtil.btnRelease(btn)
                console.error(error)
                toastr.error("Erreur lors de la suppression", "Erreur Système")
            }
        })
    })
})

Elements.search.addEventListener('keyup', e => {
    e.preventDefault()
    searchElement()
})

Elements.search.addEventListener('blur', e => {
    e.preventDefault()
    if(Elements.search.value !== '') {
        Elements.search.value = ''
        searchElement()
    }
})

Elements.selectCategory.addEventListener('change', e => {
    e.preventDefault()
    KTApp.block(Elements.table, {message: 'Chargement...'})

    $.ajax({
        url: '/wiki/search',
        method: 'POST',
        data: {category: Elements.selectCategory.value},
        success: (data) => {
            KTApp.unblock(Elements.table)
            Elements.table.innerHTML = '';
            if(Array.from(data).length === 0) {
                Elements.table.innerHTML += `<tr><td colspan="6" class="text-center font-weight-bold"><i class="fas fa-warning"></i> Aucun enregistrement</td>`
            } else {
                Array.from(data).forEach(item => {
                    let status = {
                        0:{'class': 'text-danger', 'icon': 'fa-times-circle'},
                        1:{'class': 'text-success', 'icon': 'fa-check-circle'},
                    }
                    Elements.table.innerHTML += `
                <tr>
                    <td class="w-5 text-center">${item.id}</td>
                    <td>${item.category.name}</td>
                    <td>${item.sector.name}</td>
                    <td>${item.title}</td>
                    <td class="text-center">
                        <i class="fas ${status[item.published].icon} ${status[item.published].class} fa-lg"></i>
                    </td>
                    <td class="w-20 text-center">
                        <a href="/wiki/${item.id}" class="btn btn-sm btn-icon btn-default"><i class="fas fa-eye"></i> </a>
                        <a href="/wiki/${item.id}/edit" class="btn btn-sm btn-icon btn-info"><i class="fas fa-edit"></i> </a>
                        <a href="/wiki/${item.id}/delete" class="btn btn-sm btn-icon btn-danger"><i class="fas fa-trash"></i> </a>
                    </td>
                </tr>
                `
                })
            }
        },
        error: (err) => {
            KTApp.unblock(Elements.table)
            console.error(err)
            toastr.error("Erreur de rafraichissement de la liste", "Erreur Système");
        }
    })
})

Elements.selectSector.addEventListener('change', e => {
    e.preventDefault()
    KTApp.block(Elements.table, {message: 'Chargement...'})

    $.ajax({
        url: '/wiki/search',
        method: 'POST',
        data: {sector: Elements.selectSector.value},
        success: (data) => {
            KTApp.unblock(Elements.table)
            Elements.table.innerHTML = '';
            if(Array.from(data).length === 0) {
                Elements.table.innerHTML += `<tr><td colspan="6" class="text-center font-weight-bold"><i class="fas fa-warning"></i> Aucun enregistrement</td>`
            } else {
                Array.from(data).forEach(item => {
                    let status = {
                        0:{'class': 'text-danger', 'icon': 'fa-times-circle'},
                        1:{'class': 'text-success', 'icon': 'fa-check-circle'},
                    }
                    Elements.table.innerHTML += `
                <tr>
                    <td class="w-5 text-center">${item.id}</td>
                    <td>${item.category.name}</td>
                    <td>${item.sector.name}</td>
                    <td>${item.title}</td>
                    <td class="text-center">
                        <i class="fas ${status[item.published].icon} ${status[item.published].class} fa-lg"></i>
                    </td>
                    <td class="w-20 text-center">
                        <a href="/wiki/${item.id}" class="btn btn-sm btn-icon btn-default"><i class="fas fa-eye"></i> </a>
                        <a href="/wiki/${item.id}/edit" class="btn btn-sm btn-icon btn-info"><i class="fas fa-edit"></i> </a>
                        <a href="/wiki/${item.id}/delete" class="btn btn-sm btn-icon btn-danger"><i class="fas fa-trash"></i> </a>
                    </td>
                </tr>
                `
                })
            }
        },
        error: (err) => {
            KTApp.unblock(Elements.table)
            console.error(err)
            toastr.error("Erreur de rafraichissement de la liste", "Erreur Système");
        }
    })
})

Elements.selectPublished.addEventListener('change', e => {
    e.preventDefault()
    KTApp.block(Elements.table, {message: 'Chargement...'})

    $.ajax({
        url: '/wiki/search',
        method: 'POST',
        data: {published: Elements.selectPublished.value},
        success: (data) => {
            KTApp.unblock(Elements.table)
            Elements.table.innerHTML = '';
            if(Array.from(data).length === 0) {
                Elements.table.innerHTML += `<tr><td colspan="6" class="text-center font-weight-bold"><i class="fas fa-warning"></i> Aucun enregistrement</td>`
            } else {
                Array.from(data).forEach(item => {
                    let status = {
                        0:{'class': 'text-danger', 'icon': 'fa-times-circle'},
                        1:{'class': 'text-success', 'icon': 'fa-check-circle'},
                    }
                    Elements.table.innerHTML += `
                <tr>
                    <td class="w-5 text-center">${item.id}</td>
                    <td>${item.category.name}</td>
                    <td>${item.sector.name}</td>
                    <td>${item.title}</td>
                    <td class="text-center">
                        <i class="fas ${status[item.published].icon} ${status[item.published].class} fa-lg"></i>
                    </td>
                    <td class="w-20 text-center">
                        <a href="/wiki/${item.id}" class="btn btn-sm btn-icon btn-default"><i class="fas fa-eye"></i> </a>
                        <a href="/wiki/${item.id}/edit" class="btn btn-sm btn-icon btn-info"><i class="fas fa-edit"></i> </a>
                        <a href="/wiki/${item.id}/delete" class="btn btn-sm btn-icon btn-danger"><i class="fas fa-trash"></i> </a>
                    </td>
                </tr>
                `
                })
            }
        },
        error: (err) => {
            KTApp.unblock(Elements.table)
            console.error(err)
            toastr.error("Erreur de rafraichissement de la liste", "Erreur Système");
        }
    })
})

Elements.refreshTable.addEventListener('click', e => {
    refreshTable();
})
