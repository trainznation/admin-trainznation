const spinning = 'spinner spinner-right spinner-white pr-15'
const Elements = {
    searchInList: document.querySelector('#searchInList'),
    loadListCategory: document.querySelector('#loadListCategory'),
    btnDanger: document.querySelectorAll('.btn-danger'),
    formAddCategory: document.querySelector('#formAddCategory'),
    btnAddCategory: document.querySelector('#btnAddCategory')
}

function searchElement() {
    KTApp.block(Elements.loadListCategory, {message: "Chargement..."})

    $.ajax({
        url: Elements.loadListCategory.dataset.target,
        method: 'POST',
        data: {q: Elements.searchInList.value},
        success: (data) => {
            KTApp.unblock(Elements.loadListCategory)
            Elements.loadListCategory.innerHTML = '';
            if(Array.from(data).length === 0) {
                Elements.loadListCategory.innerHTML += `<tr><td colspan="3" class="text-center font-weight-bold"><i class="fas fa-warning"></i> Aucun enregistrement</td>`
            } else {
                Array.from(data).forEach(item => {
                    Elements.loadListCategory.innerHTML += `
                <tr>
                    <td class="w-5 text-center">${item.id}</td>
                    <td>${item.name}</td>
                    <td class="w-10 text-center">
                        <a href="/wiki/category/${item.id}" class="btn btn-sm btn-icon btn-danger"><i class="fas fa-trash"></i> </a>
                    </td>
                </tr>
                `
                })
            }
        },
        error: (err) => {
            KTApp.unblock(Elements.loadListCategory)
            console.error(err)
            toastr.error("Erreur de rafraichissement de la liste", "Erreur Système");
        }
    })
}

function refreshTable() {
    searchElement()
}

Array.from(Elements.btnDanger).forEach(btn => {
    btn.addEventListener('click', (e) => {
        e.preventDefault()
        KTUtil.btnWait(btn, spinning)

        $.ajax({
            url: btn.getAttribute('href'),
            success: () => {
                KTUtil.btnRelease(btn)
                toastr.success("La catégorie à été supprimer")
                btn.parentNode.parentNode.style.display = 'none'
            },
            error: (error) => {
                KTUtil.btnRelease(btn)
                console.error(error)
                toastr.error("Erreur lors de la suppression", "Erreur Système")
            }
        })
    })
})

Elements.searchInList.addEventListener('keyup', e => {
    e.preventDefault()
    searchElement()
})

Elements.searchInList.addEventListener('blur', e => {
    e.preventDefault()
    if(Elements.searchInList.value !== '') {
        Elements.searchInList.value = ''
        searchElement()
    }
})

Elements.formAddCategory.addEventListener('submit', async (e) => {
    e.preventDefault()
    KTUtil.btnWait(Elements.btnAddCategory, spinning, 'Chargement', true)

    let data = new FormData(Elements.formAddCategory)

    try {
        let response = await fetch(Elements.formAddCategory.getAttribute('action'), {
            method: 'POST',
            body: data
        })

        let responseData = await response.json()

        if (response.ok === false) {
            KTUtil.btnRelease(Elements.btnAddCategory)
            let errors = responseData
            let errorsKey = Object.keys(errors)

            for (let i = 0; i < errorsKey.length; i++) {
                console.log(errors[key])
            }
        } else {
            KTUtil.btnRelease(Elements.btnAddCategory)
            let inputs = Elements.formAddCategory.querySelectorAll('input, textarea')
            for (let i = 0; i < inputs.length; i++) {
                inputs[i].value = ''
            }
            toastr.success(`La catégorie <strong>${responseData.name}</strong> à été ajouté`)
            $("#addCategory").on('hide.bs.modal', () => {
                refreshTable()
            })
        }
    }catch (e) {
        KTUtil.btnRelease(Elements.btnAddCategory)
        toastr.error("Erreur de script", "Erreur Système")
        console.error(e)
    }
})





