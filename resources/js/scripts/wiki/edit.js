const spinning = 'spinner spinner-right spinner-white pr-15'
const Elements = {
    form: document.querySelector('#editArticle'),
    btn: document.querySelector('#btnSubmit'),
}

Elements.form.addEventListener('submit', async e => {
    e.preventDefault()
    KTUtil.btnWait(Elements.btn, spinning, 'Chargement...', true)

    $.ajax({
        url: Elements.form.getAttribute('action'),
        method: 'PUT',
        data: $("#editArticle").serializeArray(),
        success: (data) => {
            KTUtil.btnRelease(Elements.btn)
            toastr.success("L'article <strong>"+data.title+"</strong> à été édité");
        },
        error: (error) => {
            KTUtil.btnRelease(Elements.btn)
            toastr.error("Erreur de script", "Erreur Serveur")
            console.error(error)
        }
    })
})
