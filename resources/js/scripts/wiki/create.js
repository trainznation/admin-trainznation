const spinning = 'spinner spinner-right spinner-white pr-15'
const Elements = {
    form: document.querySelector('#createArticle'),
    btn: document.querySelector('#btnSubmit'),
    selectCategory: document.querySelector('#selectCategory'),
    divSector: document.querySelector('#divSector'),
}

Elements.form.addEventListener('submit', async e => {
    e.preventDefault()
    KTUtil.btnWait(Elements.btn, spinning, 'Chargement...', true)

    let data = new FormData(Elements.form)

    try {
        let response = await fetch(Elements.form.getAttribute('action'), {
            method: 'POST',
            body: data
        })

        let responseData = await response.json();

        if (response.ok === false) {
            KTUtil.btnRelease(Elements.btn)
            let errors = responseData
            let errorsKey = Object.keys(errors)

            for (let i = 0; i < errorsKey.length; i++) {
                console.log(errors[key])
            }
        } else {
            KTUtil.btnRelease(Elements.btn)
            let inputs = Elements.form.querySelectorAll('input, textarea')
            for (let i = 0; i < inputs.length; i++) {
                inputs[i].value = ''
            }
            toastr.success(`L'article <strong>${responseData.title}</strong> à été ajouté`)
        }
    }catch (e) {
        KTUtil.btnRelease(Elements.btn)
        toastr.error("Erreur de script", "Erreur Système")
        console.error(e)
    }
})

Elements.selectCategory.addEventListener('change', e => {
    e.preventDefault()
    KTApp.block(Elements.divSector, {message: 'Chargement'})

    $.ajax({
        url: '/wiki/sector/search',
        method: 'POST',
        data: {category: Elements.selectCategory.value},
        success: (data) => {
            KTApp.unblock(Elements.divSector)
            let contents = '';
            Array.from(data).forEach(item => {
                contents += `<option value="${item.id}">${item.name}</option>`
            })
            Elements.divSector.innerHTML = '';
            Elements.divSector.innerHTML += `<label>Secteur <span class="text-danger">*</span></label>`;
            Elements.divSector.innerHTML += `<select class="form-control" name="wiki_sector_id" required>${contents}</select>`;

        },
        error: (err) => {
            KTApp.unblock(Elements.divSector)
            toastr.error("Erreur de script", "Erreur Système")
            console.error(err)
        }
    })
})
