//const spinning = 'spinner spinner-right spinner-white pr-15'
let listeDownload;
let route = document.querySelector('#route')

$.ajax({
    url: `/route/${route.dataset.id}/download/list`,
    success: (data) => {
        loadListDownload(data.data)
    }
})

function loadListDownload(data) {
    listeDownload = $("#liste_download").KTDatatable({
        data: {
            type: 'local',
            source: data,
            pageSize: 10,
        },

        // layout definition
        layout: {
            scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
            // height: 450, // datatable's body's fixed height
            footer: false, // display/hide footer
        },

        // column sorting
        sortable: true,

        pagination: true,

        search: {
            input: $('#search_query_download'),
            key: 'generalSearch'
        },

        columns: [
            {
                field: 'designation',
                title: 'Designation',
                sortable: false,
                width: 90,
                template: (row) => {
                    return `
                        V.${row.version}:${row.build}
                    `
                }
            },
            {
                field: 'published',
                title: "Publication",
                width: 120,
                textAlign: 'center',
                template: (row) => {
                    let statePublish = {
                        0: {'icon': 'fa fa-close', 'color': 'danger', 'text': 'Non Publier'},
                        1: {'icon': 'ki ki-check', 'color': 'success', 'text': 'Publier'},
                    };

                    return `<span class="label label-inline label-pill label-${statePublish[row.published].color}"><i class="${statePublish[row.published].icon} text-white"></i>&nbsp; ${statePublish[row.published].text}</span>`
                }
            },
            {
                field: 'alpha',
                title: 'Type',
                width: 120,
                textAlign: 'center',
                template: (row) => {
                    let type = {
                        0: {'color': 'light-dark'},
                        1: {'color': 'success'},
                    };

                    return `
                        <div class="symbol-group symbol-hover">
                            <div class="symbol symbol-circle symbol-${type[row.alpha].color}" data-toggle="tooltip" title="Alpha">
                                <span class="symbol-label">A</span>
                            </div>
                            <div class="symbol symbol-circle symbol-${type[row.beta].color}" data-toggle="tooltip" title="Beta">
                                <span class="symbol-label">B</span>
                            </div>
                            <div class="symbol symbol-circle symbol-${type[row.release].color}" data-toggle="tooltip" title="Release">
                                <span class="symbol-label">R</span>
                            </div>
                        </div>
                    `;
                }
            },
            {
                field: 'Action',
                title: 'Action',
                template: (row) => {
                    return `
                    <button data-href="/route/${row.route_id}/download/${row.id}" class="btn btn-icon btn-xs btn-danger" data-toggle="tooltip" title="Supprimer le téléchargement" onclick="deleteDownload($(this))"><i class="fa fa-trash"></i> </button>
                    `
                }
            }
        ]
    })
}

function deleteDownload(btn) {
    let uri = btn.attr('data-href')

    btn.html(`<div class="${spinning}"></div>`)

    $.ajax({
        url: uri,
        method: 'DELETE',
        success: (data) => {
            btn.html('<i class="fa fa-trash"></i>')
            toastr.success(data.data.message)
            btn.parents('tr').fadeOut()
        },
        error: (error) => {
            btn.html('<i class="fa fa-trash"></i>')
            console.error(error)
            toastr.error('Erreur Système, consulter les logs', "Erreur 500")
        }
    })
}
