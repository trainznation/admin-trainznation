const spinning = 'spinner spinner-right spinner-white pr-15'
const Elements = {
    averageVersion: KTUtil.getById('averageVersion'),
    chartAverage: KTUtil.getById('chartAverage'),
    textTaskRegistered: KTUtil.getById('textTaskRegistered'),
    textTaskProgress: KTUtil.getById('textTaskProgress'),
    textTaskFinish: KTUtil.getById('textTaskFinish'),
    dataVersion: '',
}

function formatInput() {
    moment.locale('fr')

    $("#started_at, #finished_at").datetimepicker({
        locale: 'fr',
        format: "YYYY-MM-DD HH:mm:ss",
        icons: {
            time: "ki ki-clock",
            date: "ki ki-calendar",
            up: "ki ki-arrow-up",
            down: "ki ki-arrow-down"
        }
    })
}


function changeAverageVersion(el, data) {
    el.innerText = `Version: ${data.version} | Build: ${data.build}`
}

function changeColorChart(value) {
    if(value <= 33) {
        return '#f44336'
    } else if(value > 33 && value <= 66) {
        return '#ffc107'
    } else {
        return '#4caf50'
    }
}

function changeChartAverage(el, data) {
    let element = el
    let height = parseInt(KTUtil.css(element, 'height'))
    el.innerHTML = null

    if(!element) {
        return;
    }

    var options = {
        series: [data],
        chart: {
            height: height,
            type: 'radialBar',
            offsetY: 0
        },
        plotOptions: {
            radialBar: {
                startAngle: -90,
                endAngle: 90,

                hollow: {
                    margin: 0,
                    size: "70%"
                },
                dataLabels: {
                    showOn: "always",
                    name: {
                        show: true,
                        fontSize: "13px",
                        fontWeight: "700",
                        offsetY: -5,
                        color: '#9e9e9e'
                    },
                    value: {
                        color: '#616161',
                        fontSize: "30px",
                        fontWeight: "700",
                        offsetY: -40,
                        show: true
                    }
                },
                track: {
                    background: '#c6c3c3',
                    strokeWidth: '100%'
                }
            }
        },
        colors: [changeColorChart(data)],
        stroke: {
            lineCap: "round",
        },
        labels: ["Progression"]
    };

    let chart = new ApexCharts(element, options);
    chart.render();
}

function changeTextTask(dataAverage) {
    Elements.textTaskRegistered.innerText = dataAverage.registered
    Elements.textTaskProgress.innerText = dataAverage.progress
    Elements.textTaskFinish.innerText = dataAverage.finish
}

function loadListTasksTable(tasks)
{
    $("#liste_tasks").KTDatatable({
        data: {
            type: 'local',
            source: tasks,
            pageSize: 10,
        },

        layout: {
            scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
            // height: 450, // datatable's body's fixed height
            footer: false, // display/hide footer
        },

        // column sorting
        sortable: true,

        pagination: true,

        search: {
            input: $('#search_query_task'),
            key: 'generalSearch'
        },
        columns: [
            {
                field: "id",
                title: "#",
                width: 20
            },
            {
                field: 'name',
                title: "Anomalie/Titre"
            },
            {
                field: 'description',
                title: "Correction/Description"
            },
            {
                field: 'etat',
                title: 'Etat',
                template: (row) => {
                    let status = {
                        0: {"class": "danger", "title": "Non Commencer", "icon": "fas fa-times text-white"},
                        1: {"class": "warning", "title": "En cours", "icon": "fas fa-reload text-white"},
                        2: {"class": "success", "title": "Terminer", "icon": "fas fa-check-circle text-white"},
                    }

                    return `<span class="label label-inline label-${status[row.etat].class}"><i class="${status[row.etat].icon}"></i> &nbsp; ${status[row.etat].title}</span>`
                }
            },
            {
                field: 'priority',
                title: 'Priorité',
                template: (row) => {
                    let priorite = {
                        0: {"class": "success", "title": "Basse"},
                        1: {"class": "warning", "title": "Moyenne"},
                        2: {"class": "info", "title": "Haute"},
                        3: {"class": "danger", "title": "Critique"},
                    }

                    return `<span class="label label-inline label-${priorite[row.priority.value].class}">${priorite[row.priority.value].title}</span>`
                }
            },
            {
                field: "actions",
                title: "Actions",
                template: (row) => {
                    return `
                    <a href="#descTask" data-toggle="modal" class="btn btn-sm btn-icon btn-hover-light-primary" data-content="${row.description}" onclick="appearDescTask($(this))"><i class="fa fa-eye"></i> </a>
                    <a href="/route/${Elements.dataVersion.route_id}/version/${Elements.dataVersion.id}/tasks/${row.id}" class="btn btn-sm btn-icon btn-hover-light-info"><i class="fa fa-edit"></i> </a>
                    <a href="/route/${Elements.dataVersion.route_id}/version/${Elements.dataVersion.id}/tasks/${row.id}/delete" class="btn btn-sm btn-icon btn-hover-light-danger"><i class="fa fa-trash"></i> </a>
                    `
                }
            }
        ]
    })
}

function callVersion() {
    $.ajax({
        url: document.querySelector('#version_uri').value,
        success: (data) => {
            Elements.dataVersion = data.data
            changeAverageVersion(Elements.averageVersion, Elements.dataVersion)
            changeChartAverage(Elements.chartAverage, Elements.dataVersion.percent_task)
            changeTextTask({
                registered: Elements.dataVersion.registered_task,
                progress: Elements.dataVersion.progress_task,
                finish: Elements.dataVersion.finish_task,
            })
            loadListTasksTable(Elements.dataVersion.tasks)
        }
    })
}

function appearDescTask(e) {
    $("#descTask").find('.modal-body').html(e.attr('data-content')).modal('show')
}

$("#formAddTask").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formAddTask")
    let uri = form.attr('action')
    let btn = KTUtil.getById('btnAddTask')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning, 'Chargement')

    $.ajax({
        url: uri,
        method: 'post',
        data: data,
        success: (data) => {
            KTUtil.btnRelease(btn)
            toastr.success(`La Tâche ${data.data.name} à été ajouté`, "Succès")
            setTimeout(() => {
                window.location.reload()
            }, 1500)
        },
        error: (err) => {
            KTUtil.btnRelease(btn)
            console.error("Erreur Système, consultez les logs", "Erreur 500")
            console.error(err)
        }
    })
})
new KTImageInput('card_img');

callVersion()
formatInput()

