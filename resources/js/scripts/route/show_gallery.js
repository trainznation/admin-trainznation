let navs = document.querySelectorAll('.navi-gallery')
let content = document.querySelector('#js-content')
let route_id = $("#route_id").val()
let dropImages = "#dropImages"
let category_id;

Array.from(navs).forEach((nav) => {
    nav.addEventListener('click', (e) => {
        call(nav.dataset.route, nav.dataset.categoryid)
    })
})

function call(route, categoryid) {
    KTApp.block(content, {
        overlayColor: '#000000',
        state: 'danger',
        message: 'Veuillez patienter...'
    })
    $.ajax({
        url: `/route/${route}/gallery/category/${categoryid}`,
        success: (data) => {
            KTApp.unblock(content)
            let galleries = data.data.galleries
            content.innerHTML = '';
            if(Array.from(galleries).length === 0) {
                content.innerHTML = `
                    <div class="col-md-12 bg-gray-200 text-center">
                        <h3 class="p-7"><i class="flaticon-warning-sign icon-2x"></i> Aucune image dans cette catégorie de gallerie</h3>
                    </div>
                `
            } else {
                Array.from(galleries).forEach((gallery) => {
                    content.innerHTML += `
                <div class="col-md-4">
                <div class="card card-custom overlay mb-5">
                    <div class="card-body p-0">
                        <div class="overlay-wrapper">
                            <img src="${gallery.image}" alt="" class="w-100 rounded">
                        </div>
                            <div class="overlay-layer align-items-start justify-content-end pt-5 pr-5">
                                <a href="${gallery.image}" data-lightbox="image-${gallery.category}" class="btn btn-clean btn-icon">
                                    <i class="flaticon-eye icon-lg text-primary"></i>
                                </a>
                                <a data-href="/route/${route}/gallery/${gallery.id}/delete" class="btn btn-clean btn-icon" onclick="deleteImg($(this))">
                                    <i class="flaticon2-trash icon-lg text-primary"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                `
                })
            }
        },
        error: (err) => {
            KTApp.unblock(content)
            toastr.error("Erreur d'affichage de la gallerie")
            console.error(err)
        }
    })
}

function deleteImg(btn) {
    KTApp.block(btn.parents('.overlay'))

    $.ajax({
        url: btn.attr('data-href'),
        success: (data) => {
            btn.parents('.overlay').fadeOut()
            toastr.success(data.message)
        },
        error: (err) => {
            KTApp.unblock(btn.parents('.overlay'))
            toastr.error(err.error)
        }
    })
}

function formatSearch(data) {
    if(data.loading) return data.text

    let markup = `
        <div class="row">
            <div class="col-md-12">${data.name}</div>
        </div>
    `;

    return markup;
}

function formatSelect(data) {
    category_id = data.id
    return data.name || data.text
}

$("#route_category_id").select2({
    placeholder: 'Rechercher...',
    allowClear: true,
    ajax: {
        url: `/route/${route_id}/gallery/category/list`,
        dataType: 'json',
        delay: 250,
        data: (params) => {
            return {
                q: params.term,
            };
        },
        processResults: (data, params) => {
            return {
                results: data.items,
            };
        },
        cache: true
    },
    escapeMarkup: (markup) => {
        return markup;
    },
    minimumInputLength: 1,
    templateResult: formatSearch,
    templateSelection: formatSelect,
    language: {
        inputTooShort: function () {
            return "Veuillez entrer au moins 1 caractère...";
        },
        searching: () => {
            return "Recherche en cours..."
        },
        errorLoading: function() {
            return "Les résultats ne peuvent pas être chargés."
        },
    }
});

function dropzoneImage() {
    let dropzoneImage = new Dropzone(dropImages, {
        url: `/route/${route_id}/gallery/addFile`,
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 10, // MB
        addRemoveLinks: true,
    })

    dropzoneImage.on('sending', (file, xhr, formData) => {
        formData.append("category_id", category_id)
    })
}

dropzoneImage();
