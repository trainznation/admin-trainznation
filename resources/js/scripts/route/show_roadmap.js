const spinning = 'spinner spinner-right spinner-white pr-15'
const Elements = {
    averageVersion: KTUtil.getById('averageVersion'),
    chartAverage: KTUtil.getById('chartAverage'),
    textTaskRegistered: KTUtil.getById('textTaskRegistered'),
    textTaskProgress: KTUtil.getById('textTaskProgress'),
    textTaskFinish: KTUtil.getById('textTaskFinish'),
    naviVersions: document.querySelectorAll('.navi-versions'),
    content: KTUtil.getById('versionContent'),
    dataVersion: '',
    listeCategory: null,
    listeCartes: null,
    listeTasks: null
}

Array.from(Elements.naviVersions).forEach((nav) => {
    nav.addEventListener('click', (e) => {
        callVersion(nav.dataset.route, nav.dataset.roadmap, nav.dataset.version)
    })
})

$("#formAddVersion").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formAddVersion")
    let uri = form.attr('action')
    let btn = document.querySelector('#btnAddVersion')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning, 'Chargement...')

    $.ajax({
        url: uri,
        method: 'get',
        data: data,
        success: (data) => {
            KTUtil.btnRelease(btn)
            toastr.success(`La Version ${data.data.id} à été ajouté avec succès`)
            setTimeout(() => {
                window.location.reload()
            }, 1200)
        },
        error: (err) => {
            KTUtil.btnRelease(btn)
            console.log(err)
            toastr.error("Erreur Technique, consulter les logs système", "Erreur 500")
        }
    })
})




function blockElement(...elements) {
    Array.from(elements).forEach((element) => {
        KTApp.block(element, {
            overlayColor: '#000000',
            state: 'danger',
            message: 'Veuillez patienter...'
        })
    })
}

function unblockElement(...elements) {
    Array.from(elements).forEach((element) => {
        KTApp.unblock(element)
    })
}


function callVersion(route, roadmap, version) {
    blockElement(Elements.chartAverage, Elements.averageVersion, Elements.textTaskRegistered, Elements.content, Elements.textTaskFinish, Elements.textTaskProgress)


    $.ajax({
        url: `/route/${route}/roadmap/${roadmap}/version/${version}`,
        success: (data) => {
            Elements.dataVersion = data.data
            unblockElement(Elements.chartAverage, Elements.averageVersion, Elements.textTaskRegistered, Elements.content, Elements.textTaskFinish, Elements.textTaskProgress)
            changeAverageVersion(Elements.averageVersion, {
                version: data.data.version,
                build: data.data.build
            })
            changeChartAverage(Elements.chartAverage, data.data.percent_task)
            changeTextTask({
                registered: data.data.registered_task,
                progress: data.data.progress_task,
                finish: data.data.finish_task,
            })
        }
    })
}


