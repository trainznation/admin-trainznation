let table;
const spinning = 'spinner spinner-right spinner-white pr-15'

function loadTable() {
    table = $("#listeRoute").KTDatatable({
        data: {
            saveState: {cookie: false}
        },
        search: {
            input: $('#search_liste_route'),
            key: 'generalSearch'
        },
        columns: [
            {
                field: "#",
                width: 40
            },
            {
                field: 'Images',
                width: 150
            },
            {
                field: "Designation",
                width: 300
            },
            {
                field: "Publier",
                width: 150,
                template: (row) => {
                    let publish = {
                        0: {"title": "Non Publier", "class": "label-danger"},
                        1: {"title": "Publier", "class": "label-success"},
                    }
                    return `<span class="label font-weight-bold label-inline label-lg ${publish[row.Publier].class}">${publish[row.Publier].title}</span>`
                }
            },
            {
                field: "Actions",
                width: 200,
                autoHide: false
            }
        ]
    })

    $("#kt_datatable_search_publier").on('change', () => {
        table.search($('#kt_datatable_search_publier').val().toLowerCase(), 'Publier')
    })

    $("#kt_datatable_search_publier").selectpicker();
}

loadTable()
