let listeSessions;
let route = document.querySelector('#route')

$.ajax({
    url: `/route/${route.dataset.id}/session/list`,
    success: (data) => {
        loadListeSession(data.data)
    }
})

function loadListeSession(data) {
    listeSessions = $("#liste_session").KTDatatable({
        data: {
            type: 'local',
            source: data,
            pageSize: 10
        },
        // layout definition
        layout: {
            scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
            // height: 450, // datatable's body's fixed height
            footer: false, // display/hide footer
        },

        // column sorting
        sortable: true,

        pagination: true,

        search: {
            input: $('#search_query_session'),
            key: 'generalSearch'
        },

        columns: [
            {
                field: 'name',
                title: "Session",
            },
            {
                field: 'images',
                title: 'Thumbnail',
                sortable: false,
                width: 120,
                template: (row) => {
                    return `<img src="${row.image}" alt="${row.name}" width="120" class="img-fluid">`
                }
            },
            {
                field: 'kuid',
                title: 'KUID'
            },
            {
                field: 'published',
                title: 'Publication',
                template: (row) => {
                    let publish = {
                        0: {'color': 'danger', 'icon': 'ki ki-close', 'text': 'Non Publier'},
                        1: {'color': 'success', 'icon': 'ki ki-check', 'text': 'Publier'},
                    };

                    return `<span class="label label-inline label-${publish[row.published].color}"><i class="${publish[row.published].icon}"></i>&nbsp; ${publish[row.published].text}</span>`
                }
            },
            {
                field: 'Actions',
                title: 'Actions',
                sortable: false,
                template: (row) => {
                    return `
                        <button data-link="/route/${row.route.id}/session/${row.id}" class="btn btn-icon btn-xs btn-danger" data-toggle="tooltip" title="Supprimer la session" onclick="deleteSession($(this))"><i class="fa fa-trash"></i> </button>
                    `
                }
            },
            {
                field: 'short_content',
                title: 'Courte description',
                autoHide: true
            },

            {
                field: 'content',
                title: 'Description',
                autoHide: true,
                template: (row) => {
                    return `${marked(row.content)}`
                }
            },
        ]
    })
}

function chargeElement() {
    $('#short_content').maxlength({
        threshold: 5,
        warningClass: "label label-danger label-rounded label-inline",
        limitReachedClass: "label label-primary label-rounded label-inline"
    });
    $('[data-switch=true]').bootstrapSwitch();
    $("#published_at").datetimepicker({
        useCurrent: true,
        locale: 'fr',
        icons: {
            time: 'fa fa-clock',
            date: 'fa fa-calendar',
            up: 'fa fa-arrow-up',
            down: 'fa fa-arrow-down',
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-calendar-check-o',
            clear: 'fa fa-trash',
            close: 'fa fa-times'
        },
        format: 'Y-MM-DD HH:mm'
    });


    new KTImageInput('session_img')
}
function appearPublishedAt() {
    let published = $("#published")
    let published_at = $("#show_published_at")

    if(published.is(':checked')) {
        published_at.show()
    } else {
        published_at.hide()
    }
}
function deleteSession(btn) {
    let uri = btn.attr('data-link')

    btn.html(`<div class="${spinning}"></div>`)

    $.ajax({
        url: uri,
        method: 'DELETE',
        success: (data) => {
            btn.html('<i class="fa fa-trash"></i>')
            toastr.success(data.data.message)
            btn.parents('tr').fadeOut()
        },
        error: (error) => {
            btn.html('<i class="fa fa-trash"></i>')
            console.error(error)
            toastr.error('Erreur Système, consulter les logs', "Erreur 500")
        }
    })
}

chargeElement()
appearPublishedAt()
